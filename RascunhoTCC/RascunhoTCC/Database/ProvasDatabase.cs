﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Database
{
    class ProvasDatabase
    {
        public void CadastrarProvas(Model.tb_prova ModelProva)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            db.tb_prova.Add(ModelProva);
            db.SaveChanges();
        }

        public List<Model.tb_prova> ConsultarProvas()
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            List<Model.tb_prova> ModelProvas = db.tb_prova.OrderBy(c => c.dt_prova)
                                                          .ToList();

            return ModelProvas;
        }

        public void AlterarProvas(Model.tb_prova ModelProva)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            Model.tb_prova alterar = db.tb_prova.FirstOrDefault(c => c.id_prova == ModelProva.id_prova);
            alterar.tm_inicial = ModelProva.tm_inicial;
            alterar.tm_final = ModelProva.tm_final;
            alterar.dt_prova = ModelProva.dt_prova;
            alterar.tp_prova = ModelProva.tp_prova;

            db.SaveChanges();
        }

        public void RemoverProvas(int id)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            Model.tb_prova remover = db.tb_prova.FirstOrDefault(c => c.id_prova == id);
            db.tb_prova.Remove(remover);
            db.SaveChanges();
        }

        public List<Model.tb_prova> ConsultarPorNomeAlunos(string aluno)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();

            var Provas = (
                                from p in db.tb_prova
                                join c in db.tb_cliente on p.id_cliente equals c.id_cliente
                                where c.nm_cliente.Contains(aluno)
                                select p
                            ).ToList();

            return Provas;
        }
    }
}
