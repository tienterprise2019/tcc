﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Database
{
    class AulasDatabase
    {
        public void CadastrarAulas(Model.tb_aula ModelAula)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            db.tb_aula.Add(ModelAula);
            db.SaveChanges();
        }

        public List<Model.tb_aula> ConsultarAulas()
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            List<Model.tb_aula> ModelAulas = db.tb_aula.OrderBy(c => c.id_aula)
                                                          .ToList();

            return ModelAulas;
        }

        public void AlterarAulas(Model.tb_aula ModelAula)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            Model.tb_aula alterar = db.tb_aula.FirstOrDefault(c => c.id_aula == ModelAula.id_aula);
            alterar.hr_chegada = ModelAula.hr_chegada;
            alterar.km_final = ModelAula.km_final;
            alterar.ds_obs = ModelAula.ds_obs;

            db.SaveChanges();
        }

        public void RemoverAulas(int id)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            Model.tb_aula remover = db.tb_aula.FirstOrDefault(c => c.id_aula == id);
            db.tb_aula.Remove(remover);
            db.SaveChanges();
        }

        public List<Model.tb_aula> ConsultarPorNomeClientes(string cliente)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            var Aulas = (
                                from a in db.tb_aula
                                join c in db.tb_cliente on a.id_cliente equals c.id_cliente
                                where c.nm_cliente.Contains(cliente)
                                select a
                            ).ToList();

            return Aulas;
        }
    }
}
