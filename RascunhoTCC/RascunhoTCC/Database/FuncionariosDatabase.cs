﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Database
{
    class FuncionariosDatabase
    {
        public void CadastrarFuncionarios(Model.tb_funcionario ModelFuncionario)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            db.tb_funcionario.Add(ModelFuncionario);
            db.SaveChanges();
        }

        public List<Model.tb_funcionario> ConsultarFuncionarios()
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            List<Model.tb_funcionario> ModelFuncionarios = db.tb_funcionario.OrderBy(c => c.nm_funcionario)
                                                          .ToList();

            return ModelFuncionarios;
        }

        public void AlterarFuncionarios(Model.tb_funcionario ModelFuncionario)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            Model.tb_funcionario alterar = db.tb_funcionario.FirstOrDefault(c => c.id_funcionario == ModelFuncionario.id_funcionario);
            alterar.ds_cpf = ModelFuncionario.ds_cpf;
            alterar.dt_nascimento = ModelFuncionario.dt_nascimento;
            alterar.ds_telefone = ModelFuncionario.ds_telefone;
            alterar.ds_celular = ModelFuncionario.ds_celular;
            alterar.ds_email = ModelFuncionario.ds_email;
            alterar.ds_cargo = ModelFuncionario.ds_cargo;
            alterar.hr_entrada = ModelFuncionario.hr_entrada;
            alterar.hr_saida = ModelFuncionario.hr_saida;
            alterar.dt_contratacao = ModelFuncionario.dt_contratacao;
            alterar.nm_rua = ModelFuncionario.nm_rua;
            alterar.nr_numero = ModelFuncionario.nr_numero;
            alterar.nm_bairro = ModelFuncionario.nm_bairro;
            alterar.nm_cidade = ModelFuncionario.nm_cidade;
            alterar.ds_cep = ModelFuncionario.ds_cep;

            db.SaveChanges();
        }

        public void RemoverFuncionarios(int id)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            Model.tb_funcionario remover = db.tb_funcionario.FirstOrDefault(c => c.id_funcionario == id);
            db.tb_funcionario.Remove(remover);
            db.SaveChanges();
        }

        public List<Model.tb_funcionario> ConsultarPorNomeFuncionarios(string funcionario)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            List<Model.tb_funcionario> ModelFuncionarios = db.tb_funcionario.Where(c => c.nm_funcionario
                                                          .Contains(funcionario))
                                                          .OrderBy(c => c.nm_funcionario)
                                                          .ToList();

            return ModelFuncionarios;
        }
    }
}
