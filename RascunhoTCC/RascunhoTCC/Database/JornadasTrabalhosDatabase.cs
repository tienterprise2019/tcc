﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Database
{
    class JornadasTrabalhosDatabase
    {
        public void CadastrarJornadasTrabalhos(Model.tb_jornada_trabalho ModelJornadasTrabalhos)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            db.tb_jornada_trabalho.Add(ModelJornadasTrabalhos);
            db.SaveChanges();
        }

        public List<Model.tb_jornada_trabalho> ConsultarJornadasTrabalhos()
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            List<Model.tb_jornada_trabalho> ModelJornadasTrabalhos = db.tb_jornada_trabalho.OrderBy(c => c.id_jornada_trabalho)
                                                          .ToList();

            return ModelJornadasTrabalhos;
        }

        public void AlterarJornadasTrabalhos(Model.tb_jornada_trabalho ModelJornadasTrabalho)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            Model.tb_jornada_trabalho alterar = db.tb_jornada_trabalho.FirstOrDefault(c => c.id_jornada_trabalho == ModelJornadasTrabalho.id_jornada_trabalho);
            alterar.hr_entrada = ModelJornadasTrabalho.hr_entrada;
            alterar.hr_saida_interv = ModelJornadasTrabalho.hr_saida_interv;
            alterar.hr_retorno_intrv = ModelJornadasTrabalho.hr_retorno_intrv;
            alterar.hr_saida = ModelJornadasTrabalho.hr_saida;

            db.SaveChanges();
        }

        public void RemoverJornadasTrabalhos(int id)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            Model.tb_jornada_trabalho remover = db.tb_jornada_trabalho.FirstOrDefault(c => c.id_jornada_trabalho == id);
            db.tb_jornada_trabalho.Remove(remover);
            db.SaveChanges();
        }

        public List<Model.tb_jornada_trabalho> ConsultarPorNomeFuncionario(string funcionario)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            var JornadasTrabalhos = (
                                from j in db.tb_jornada_trabalho
                                join f in db.tb_funcionario on j.id_funcionario equals f.id_funcionario
                                where f.nm_funcionario.Contains(funcionario)
                                select j
                            ).ToList();

            return JornadasTrabalhos;
        }
    }
}
