﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Database
{
    class CarrosDatabase
    {
        public void CadastrarCarros(Model.tb_carro ModelCarro)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            db.tb_carro.Add(ModelCarro);
            db.SaveChanges();
        }

        public List<Model.tb_carro> ConsultarCarros()
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            List<Model.tb_carro> ModelCarros = db.tb_carro.OrderBy(c => c.nm_modelo)
                                                          .ToList();

            return ModelCarros;
        }

        public void AlterarCarros(Model.tb_carro ModelCarro)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            Model.tb_carro alterar = db.tb_carro.FirstOrDefault(c => c.id_carro == ModelCarro.id_carro);
            alterar.nm_modelo = ModelCarro.nm_modelo;
            alterar.ds_placa = ModelCarro.ds_placa;
            alterar.ds_ano = ModelCarro.ds_ano;
            alterar.km_inicial = ModelCarro.km_inicial;
            alterar.km_final = ModelCarro.km_final;
            alterar.tp_disponivel = ModelCarro.tp_disponivel;

            db.SaveChanges();
        }

        public void RemoverCarros(int id)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            Model.tb_carro remover = db.tb_carro.FirstOrDefault(c => c.id_carro == id);
            db.tb_carro.Remove(remover);
            db.SaveChanges();
        }

        public List<Model.tb_carro> ConsultarPorNomeModelos(string modelo)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            List<Model.tb_carro> ModelCarros = db.tb_carro.Where(c => c.nm_modelo
                                                          .Contains(modelo))
                                                          .OrderBy(c => c.nm_modelo)
                                                          .ToList();

            return ModelCarros;
        }
    }
}
