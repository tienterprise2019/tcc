﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Database
{
    class AbastecimentosDatabase
    {
        public void CadastrarAbastecimentos(Model.tb_abastecimento ModelAbastecimento)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            db.tb_abastecimento.Add(ModelAbastecimento);
            db.SaveChanges();
        }

        public List<Model.tb_abastecimento> ConsultarAbastecimentos()
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            List<Model.tb_abastecimento> ModelAbastecimentos = db.tb_abastecimento.OrderBy(c => c.nm_local)
                                                          .ToList();

            return ModelAbastecimentos;
        }

        public void AlterarAbastecimentos(Model.tb_abastecimento ModelAbastecimento)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            Model.tb_abastecimento alterar = db.tb_abastecimento.FirstOrDefault(c => c.id_abastecimento == ModelAbastecimento.id_abastecimento);
            alterar.tp_combustivel = ModelAbastecimento.tp_combustivel;
            alterar.nm_local = ModelAbastecimento.nm_local;
            alterar.vl_preco = ModelAbastecimento.vl_preco;
            alterar.nr_litros = ModelAbastecimento.nr_litros;
            alterar.dt_dia = ModelAbastecimento.dt_dia;

            db.SaveChanges();
        }

        public void RemoverAbastecimentos(int id)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            Model.tb_abastecimento remover = db.tb_abastecimento.FirstOrDefault(c => c.id_abastecimento == id);
            db.tb_abastecimento.Remove(remover);
            db.SaveChanges();
        }

        public List<Model.tb_abastecimento> ConsultarPorModelos(string modelo)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();

            var abastecimento = (
                                from a in db.tb_abastecimento
                                join c in db.tb_carro on a.id_carro equals c.id_carro
                                where c.nm_modelo.Contains(modelo)
                                select a
                            ).ToList();

            return abastecimento;
        }
    }
}
