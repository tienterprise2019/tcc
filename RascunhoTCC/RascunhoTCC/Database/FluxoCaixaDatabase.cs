﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Database
{
    class FluxoCaixaDatabase
    {
        public List<Model.vw_fluxo_caixa> ConsultarFluxosCaixas()
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            List<Model.vw_fluxo_caixa> ModelFluxosCaixas = db.vw_fluxo_caixa.OrderBy(f => f.dt_dia)
                                                          .ToList();

            return ModelFluxosCaixas;
        }

        public List<Model.vw_fluxo_caixa> ConsultarPorData(DateTime data)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            List<Model.vw_fluxo_caixa> ModelFluxosCaixas = db.vw_fluxo_caixa.Where(f => f.dt_dia.Equals(data))
                                                          .OrderBy(f => f.dt_dia)
                                                          .ToList();

            return ModelFluxosCaixas;
        }
    }
}
