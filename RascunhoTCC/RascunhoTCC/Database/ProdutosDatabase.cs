﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Database
{
    class ProdutosDatabase
    {
        public void CadastrarProdutos(Model.tb_produto ModelProduto)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            db.tb_produto.Add(ModelProduto);
            db.SaveChanges();
        }

        public List<Model.tb_produto> ConsultarProdutos()
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            List<Model.tb_produto> ModelProdutos = db.tb_produto.OrderBy(c => c.nm_produto)
                                                          .ToList();

            return ModelProdutos;
        }

        public void AlterarProdutos(Model.tb_produto ModelProduto)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            Model.tb_produto alterar = db.tb_produto.FirstOrDefault(c => c.id_produto == ModelProduto.id_produto);
            alterar.vl_preco = ModelProduto.vl_preco;
            alterar.qt_antiga = ModelProduto.qt_antiga;
            alterar.qt_atual = ModelProduto.qt_atual;
            alterar.ds_tipo = ModelProduto.ds_tipo;
            alterar.dt_compra = ModelProduto.dt_compra;
            alterar.id_fornecedor = ModelProduto.id_fornecedor;

            db.SaveChanges();
        }

        public void RemoverProdutos(int id)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            Model.tb_produto remover = db.tb_produto.FirstOrDefault(c => c.id_produto == id);
            db.tb_produto.Remove(remover);
            db.SaveChanges();
        }

        public List<Model.tb_produto> ConsultarPorNomeProdutos(string produto)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            List<Model.tb_produto> ModelProdutos = db.tb_produto.Where(c => c.nm_produto
                                                          .Contains(produto))
                                                          .OrderBy(c => c.nm_produto)
                                                          .ToList();

            return ModelProdutos;
        }
    }
}
