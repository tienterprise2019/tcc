﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Database
{
    class LoginDatabase
    {
        public Model.tb_login Login(Model.tb_login LoginModel)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            Model.tb_login LoginMD = db.tb_login.FirstOrDefault(l => l.ds_email == LoginModel.ds_email
                                                                     && l.ds_senha == LoginModel.ds_senha);

            return LoginMD;
        }

        public void CadastroLogin(Model.tb_login LoginModel)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            db.tb_login.Add(LoginModel);
            db.SaveChanges();
        }
    }
}
