﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Database
{
    class HoleritesDatabase
    {
        public void CadastrarHolerites(Model.tb_holerite ModelHolerite)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            db.tb_holerite.Add(ModelHolerite);
            db.SaveChanges();
        }

        public List<Model.tb_holerite> ConsultarHolerites()
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            List<Model.tb_holerite> ModelHolerites = db.tb_holerite.OrderBy(c => c.id_holerite)
                                                          .ToList();

            return ModelHolerites;
        }

        public void AlterarHolerites(Model.tb_holerite ModelHolerite)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            Model.tb_holerite alterar = db.tb_holerite.FirstOrDefault(c => c.id_holerite == ModelHolerite.id_holerite);
            alterar.vl_bruto = ModelHolerite.vl_bruto;
            alterar.tp_beneficio = ModelHolerite.tp_beneficio;
            alterar.vl_fgts = ModelHolerite.vl_fgts;
            alterar.vl_irrf = ModelHolerite.vl_irrf;
            alterar.pl_convenio = ModelHolerite.pl_convenio;
            alterar.sl_liquido = ModelHolerite.sl_liquido;
            alterar.sl_periculosidade = ModelHolerite.sl_periculosidade;
            alterar.vl_decimo_terceiro = ModelHolerite.vl_decimo_terceiro;
            alterar.dt_pagamento = ModelHolerite.dt_pagamento;

            db.SaveChanges();
        }

        public void RemoverHolerites(int id)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            Model.tb_holerite remover = db.tb_holerite.FirstOrDefault(c => c.id_holerite == id);
            db.tb_holerite.Remove(remover);
            db.SaveChanges();
        }

        public List<Model.tb_holerite> ConsultarPorNomeHolerites(string funcionario)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();

            var holerites = (
                                from h in db.tb_holerite
                                join f in db.tb_funcionario on h.id_funcionario equals f.id_funcionario
                                where f.nm_funcionario.Contains(funcionario)
                                select h
                            ).ToList();

            return holerites;

            //List<Model.tb_funcionario> ModelFuncionarios = db.tb_holerite.Where(c => c.tb_funcionario.nm_funcionario.Contains(funcionario))
            //                                                             .OrderBy(c => c.nm_funcionario)
            //                                                             .ToList();

            //return ModelHolerites;
        }
    }
}
