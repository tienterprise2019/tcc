﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Database
{
    class FornecedoresDatabase
    {
        public void CadastrarFornecedores(Model.tb_fornecedor ModelFornecedor)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            db.tb_fornecedor.Add(ModelFornecedor);
            db.SaveChanges();
        }

        public List<Model.tb_fornecedor> ConsultarFornecedores()
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            List<Model.tb_fornecedor> ModelFornecedores = db.tb_fornecedor.OrderBy(c => c.nm_fornecedor)
                                                          .ToList();

            return ModelFornecedores;
        }

        public void AlterarFornecedores(Model.tb_fornecedor ModelFornecedor)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            Model.tb_fornecedor alterar = db.tb_fornecedor.FirstOrDefault(c => c.id_fornecedor == ModelFornecedor.id_fornecedor);
            alterar.nm_fornecedor = ModelFornecedor.nm_fornecedor;
            alterar.ds_telefone = ModelFornecedor.ds_telefone;
            alterar.ds_email = ModelFornecedor.ds_email;
            alterar.ds_cnpj = ModelFornecedor.ds_cnpj;
            alterar.nm_rua = ModelFornecedor.nm_rua;
            alterar.nr_numero = ModelFornecedor.nr_numero;
            alterar.nm_bairro = ModelFornecedor.nm_bairro;
            alterar.nm_cidade = ModelFornecedor.nm_cidade;
            alterar.ds_cep = ModelFornecedor.ds_cep;
            db.SaveChanges();
        }

        public void RemoverFornecedores(int id)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            Model.tb_fornecedor remover = db.tb_fornecedor.FirstOrDefault(c => c.id_fornecedor == id);
            db.tb_fornecedor.Remove(remover);
            db.SaveChanges();
        }

        public List<Model.tb_fornecedor> ConsultarPorNomeFornecedores(string fornecedor)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            List<Model.tb_fornecedor> ModelFornecedores = db.tb_fornecedor.Where(c => c.nm_fornecedor
                                                          .Contains(fornecedor))
                                                          .OrderBy(c => c.nm_fornecedor)
                                                          .ToList();

            return ModelFornecedores;
        }
    }
}
