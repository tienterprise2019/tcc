﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Database
{
    class PacotesDatabase
    {
        public void CadastrarPacotes(Model.tb_pacote ModelPacote)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            db.tb_pacote.Add(ModelPacote);
            db.SaveChanges();
        }

        public List<Model.tb_pacote> ConsultarPacotes()
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            List<Model.tb_pacote> ModelPacotes = db.tb_pacote.OrderBy(c => c.id_cliente)
                                                          .ToList();

            return ModelPacotes;
        }

        public void AlterarPacotes(Model.tb_pacote ModelPacote)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            Model.tb_pacote alterar = db.tb_pacote.FirstOrDefault(c => c.id_pacote == ModelPacote.id_pacote);
            alterar.qt_teorica = ModelPacote.qt_teorica;
            alterar.qt_pratica = ModelPacote.qt_pratica;
            alterar.tp_carteira = ModelPacote.tp_carteira;
            alterar.vl_preco = ModelPacote.vl_preco;
            alterar.dt_venda = ModelPacote.dt_venda;

            db.SaveChanges();
        }

        public void RemoverPacotes(int id)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            Model.tb_pacote remover = db.tb_pacote.FirstOrDefault(c => c.id_pacote == id);
            db.tb_pacote.Remove(remover);
            db.SaveChanges();
        }

        public List<Model.tb_pacote> ConsultarPorNomeAlunos(string aluno)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();

            var Pacotes = (
                                from p in db.tb_pacote
                                join a in db.tb_cliente on p.id_cliente equals a.id_cliente
                                where a.nm_cliente.Contains(aluno)
                                select p
                            ).ToList();

            return Pacotes;
        }
    }
}
