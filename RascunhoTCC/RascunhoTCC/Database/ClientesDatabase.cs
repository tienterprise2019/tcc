﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Database
{
    class ClientesDatabase
    {
        public void CadastrarClientes(Model.tb_cliente ModelCliente)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            db.tb_cliente.Add(ModelCliente);
            db.SaveChanges();
        }

        public List<Model.tb_cliente> ConsultarClientes()
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            List<Model.tb_cliente> ModelClientes = db.tb_cliente.OrderBy(c => c.nm_cliente)
                                                          .ToList();

            return ModelClientes;
        }

        public void AlterarClientes(Model.tb_cliente ModelCliente)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            Model.tb_cliente alterar = db.tb_cliente.FirstOrDefault(c => c.id_cliente == ModelCliente.id_cliente);
            alterar.ds_cpf = ModelCliente.ds_cpf;
            alterar.dt_nascimento = ModelCliente.dt_nascimento;
            alterar.ds_telefone = ModelCliente.ds_telefone;
            alterar.ds_celular = ModelCliente.ds_celular;
            alterar.ds_email = ModelCliente.ds_email;
            alterar.tp_deficiente = ModelCliente.tp_deficiente;
            alterar.ds_obs = ModelCliente.ds_obs;
            alterar.nm_rua = ModelCliente.nm_rua;
            alterar.nr_numero = ModelCliente.nr_numero;
            alterar.nm_bairro = ModelCliente.nm_bairro;
            alterar.nm_cidade = ModelCliente.nm_cidade;
            alterar.ds_cep = ModelCliente.ds_cep;

            db.SaveChanges();
        }

        public void RemoverClientes(int id)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            Model.tb_cliente remover = db.tb_cliente.FirstOrDefault(c => c.id_cliente == id);
            db.tb_cliente.Remove(remover);
            db.SaveChanges();
        }

        public List<Model.tb_cliente> ConsultarPorNomeClientes(string cliente)
        {
            Model.tccdbEntities db = new Model.tccdbEntities();
            List<Model.tb_cliente> ModelClientes = db.tb_cliente.Where(c => c.nm_cliente
                                                          .Contains(cliente))
                                                          .OrderBy(c => c.nm_cliente)
                                                          .ToList();

            return ModelClientes;
        }
    }
}
