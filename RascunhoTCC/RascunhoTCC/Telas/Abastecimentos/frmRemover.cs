﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Abastecimentos
{
    public partial class frmRemover : Form
    {
        public frmRemover()
        {
            InitializeComponent();
            this.CarregarAbastecimentos();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRemover_Click(object sender, EventArgs e)
        {
            this.Remover();
        }

        public void CarregarAbastecimentos()
        {
            try
            {
                Business.AbastecimentosBusiness BSAbastecimento = new Business.AbastecimentosBusiness();
                List<Model.tb_abastecimento> ModelAbastecimentos = BSAbastecimento.ConsultarAbastecimentos();

                dgvAbastecimentos.AutoGenerateColumns = false;
                dgvAbastecimentos.DataSource = ModelAbastecimentos;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Remover()
        {
            try
            {
                Model.tb_abastecimento ModelAbastecimento = dgvAbastecimentos.CurrentRow.DataBoundItem as Model.tb_abastecimento;

                Business.AbastecimentosBusiness BSAbastecimento = new Business.AbastecimentosBusiness();
                BSAbastecimento.RemoverAbastecimentos(ModelAbastecimento.id_abastecimento);

                MessageBox.Show("Abastecimento removido com sucesso!", "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.CarregarAbastecimentos();

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
