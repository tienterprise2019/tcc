﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Abastecimentos
{
    public partial class frmAlterar : Form
    {
        public frmAlterar()
        {
            InitializeComponent();
            this.CarregarAbastecimentos();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvAbastecimentos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            this.CarregarAbastecimento();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            this.Alterar();
        }

        public void CarregarAbastecimentos()
        {
            try
            {
                Business.AbastecimentosBusiness BSAbastecimentos = new Business.AbastecimentosBusiness();
                List<Model.tb_abastecimento> ModelAbastecimentos = BSAbastecimentos.ConsultarAbastecimentos();

                dgvAbastecimentos.AutoGenerateColumns = false;
                dgvAbastecimentos.DataSource = ModelAbastecimentos;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarAbastecimento()
        {
            try
            {
                Model.tb_abastecimento ModelAbastecimento = dgvAbastecimentos.CurrentRow.DataBoundItem as Model.tb_abastecimento;

                cboTpCombustivel.Text = ModelAbastecimento.tp_combustivel;
                txtLocal.Text = ModelAbastecimento.nm_local;
                nudPreco.Value = ModelAbastecimento.vl_preco;
                nudLitros.Value = ModelAbastecimento.nr_litros;
                dtpData.Value = ModelAbastecimento.dt_dia;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Alterar()
        {
            try
            {
                Model.tb_abastecimento ModelAbastecimento = dgvAbastecimentos.CurrentRow.DataBoundItem as Model.tb_abastecimento;

                ModelAbastecimento.tp_combustivel = cboTpCombustivel.Text;
                ModelAbastecimento.vl_preco = nudPreco.Value;
                ModelAbastecimento.nr_litros = nudLitros.Value;
                ModelAbastecimento.nm_local = txtLocal.Text;
                ModelAbastecimento.dt_dia = dtpData.Value;

                Business.AbastecimentosBusiness BSAbastecimento = new Business.AbastecimentosBusiness();
                BSAbastecimento.RemoverAbastecimentos(ModelAbastecimento.id_abastecimento);

                MessageBox.Show("Abastecimento lterado com sucesso!", "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.CarregarAbastecimentos();

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
