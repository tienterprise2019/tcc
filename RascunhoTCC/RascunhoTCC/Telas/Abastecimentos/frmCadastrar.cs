﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Abastecimentos
{
    public partial class frmCadastrar : Form
    {
        public frmCadastrar()
        {
            InitializeComponent();
            this.CarregarFuncionarios();
            this.CarregarModelos();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            this.Cadastrar();
        }

        public void CarregarFuncionarios()
        {
            try
            {
                Business.FuncionariosBusiness BSFuncionarios = new Business.FuncionariosBusiness();
                List<Model.tb_funcionario> ModelFuncionarios = BSFuncionarios.ConsultarFuncionarios();

                cboFuncionario.DisplayMember = nameof(Model.tb_funcionario.nm_funcionario);
                cboFuncionario.DataSource = ModelFuncionarios;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarModelos()
        {
            try
            {
                Business.CarrosBusiness BSCarros = new Business.CarrosBusiness();
                List<Model.tb_carro> ModelCarros = BSCarros.ConsultarCarros();

                cboCarro.DisplayMember = nameof(Model.tb_carro.nm_modelo);
                cboCarro.DataSource = ModelCarros;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Cadastrar()
        {
            try
            {
                Model.tb_funcionario ModelFuncionario = cboFuncionario.SelectedItem as Model.tb_funcionario;
                Model.tb_carro ModelCarro = cboCarro.SelectedItem as Model.tb_carro;
                Model.tb_abastecimento ModelAbastecimento = new Model.tb_abastecimento();

                ModelAbastecimento.id_funcionario = ModelFuncionario.id_funcionario;
                ModelAbastecimento.id_carro = ModelCarro.id_carro;
                ModelAbastecimento.tp_combustivel = cboTpCombustivel.Text;
                ModelAbastecimento.vl_preco = nudPreco.Value;
                ModelAbastecimento.nr_litros = nudLitros.Value;
                ModelAbastecimento.nm_local = txtLocal.Text;
                ModelAbastecimento.dt_dia = dtpData.Value;
                
                Business.AbastecimentosBusiness BSAbastecimento = new Business.AbastecimentosBusiness();
                BSAbastecimento.CadastrarAbastecimentos(ModelAbastecimento);

                MessageBox.Show("Abastecimento cadastrado com sucesso!", "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Abastecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}