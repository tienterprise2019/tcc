﻿namespace RascunhoTCC.Telas
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMenu));
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlProva = new System.Windows.Forms.Panel();
            this.panel107 = new System.Windows.Forms.Panel();
            this.panel108 = new System.Windows.Forms.Panel();
            this.panel109 = new System.Windows.Forms.Panel();
            this.panel110 = new System.Windows.Forms.Panel();
            this.btnCadastrar11 = new System.Windows.Forms.Button();
            this.btnRemover11 = new System.Windows.Forms.Button();
            this.btnConsultar11 = new System.Windows.Forms.Button();
            this.btnAlterar11 = new System.Windows.Forms.Button();
            this.pnlCarro = new System.Windows.Forms.Panel();
            this.panel49 = new System.Windows.Forms.Panel();
            this.panel76 = new System.Windows.Forms.Panel();
            this.panel77 = new System.Windows.Forms.Panel();
            this.panel78 = new System.Windows.Forms.Panel();
            this.btnCadastrar3 = new System.Windows.Forms.Button();
            this.btnRemover3 = new System.Windows.Forms.Button();
            this.btnConsultar3 = new System.Windows.Forms.Button();
            this.btnAlterar3 = new System.Windows.Forms.Button();
            this.pnlAula = new System.Windows.Forms.Panel();
            this.panel72 = new System.Windows.Forms.Panel();
            this.panel73 = new System.Windows.Forms.Panel();
            this.panel74 = new System.Windows.Forms.Panel();
            this.panel75 = new System.Windows.Forms.Panel();
            this.btnCadastrar2 = new System.Windows.Forms.Button();
            this.btnRemover2 = new System.Windows.Forms.Button();
            this.btnConsultar2 = new System.Windows.Forms.Button();
            this.btnAlterar2 = new System.Windows.Forms.Button();
            this.pnlProd = new System.Windows.Forms.Panel();
            this.panel103 = new System.Windows.Forms.Panel();
            this.panel104 = new System.Windows.Forms.Panel();
            this.panel105 = new System.Windows.Forms.Panel();
            this.panel106 = new System.Windows.Forms.Panel();
            this.btnCadastrar10 = new System.Windows.Forms.Button();
            this.btnRemover10 = new System.Windows.Forms.Button();
            this.btnConsultar10 = new System.Windows.Forms.Button();
            this.btnAlterar10 = new System.Windows.Forms.Button();
            this.pnlAbast = new System.Windows.Forms.Panel();
            this.panel71 = new System.Windows.Forms.Panel();
            this.panel70 = new System.Windows.Forms.Panel();
            this.panel69 = new System.Windows.Forms.Panel();
            this.panel48 = new System.Windows.Forms.Panel();
            this.btnCadastrar1 = new System.Windows.Forms.Button();
            this.btnRemover1 = new System.Windows.Forms.Button();
            this.btnConsultar1 = new System.Windows.Forms.Button();
            this.btnAlterar1 = new System.Windows.Forms.Button();
            this.panel34 = new System.Windows.Forms.Panel();
            this.pnlPact = new System.Windows.Forms.Panel();
            this.panel99 = new System.Windows.Forms.Panel();
            this.panel100 = new System.Windows.Forms.Panel();
            this.panel101 = new System.Windows.Forms.Panel();
            this.panel102 = new System.Windows.Forms.Panel();
            this.btnCadastrar9 = new System.Windows.Forms.Button();
            this.btnRemover9 = new System.Windows.Forms.Button();
            this.btnConsultar9 = new System.Windows.Forms.Button();
            this.btnAlterar9 = new System.Windows.Forms.Button();
            this.panel33 = new System.Windows.Forms.Panel();
            this.pnlJornada = new System.Windows.Forms.Panel();
            this.panel95 = new System.Windows.Forms.Panel();
            this.panel96 = new System.Windows.Forms.Panel();
            this.panel97 = new System.Windows.Forms.Panel();
            this.panel98 = new System.Windows.Forms.Panel();
            this.btnCadastrar8 = new System.Windows.Forms.Button();
            this.btnRemover8 = new System.Windows.Forms.Button();
            this.btnConsultar8 = new System.Windows.Forms.Button();
            this.btnAlterar8 = new System.Windows.Forms.Button();
            this.pnlCliente = new System.Windows.Forms.Panel();
            this.panel79 = new System.Windows.Forms.Panel();
            this.panel80 = new System.Windows.Forms.Panel();
            this.panel81 = new System.Windows.Forms.Panel();
            this.panel82 = new System.Windows.Forms.Panel();
            this.btnCadastrar4 = new System.Windows.Forms.Button();
            this.btnRemover4 = new System.Windows.Forms.Button();
            this.btnConsultar4 = new System.Windows.Forms.Button();
            this.btnAlterar4 = new System.Windows.Forms.Button();
            this.panel31 = new System.Windows.Forms.Panel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.button13 = new System.Windows.Forms.Button();
            this.pnlForn = new System.Windows.Forms.Panel();
            this.panel83 = new System.Windows.Forms.Panel();
            this.panel84 = new System.Windows.Forms.Panel();
            this.panel85 = new System.Windows.Forms.Panel();
            this.panel86 = new System.Windows.Forms.Panel();
            this.btnCadastrar5 = new System.Windows.Forms.Button();
            this.btnRemover5 = new System.Windows.Forms.Button();
            this.btnConsultar5 = new System.Windows.Forms.Button();
            this.btnAlterar5 = new System.Windows.Forms.Button();
            this.pnlHoler = new System.Windows.Forms.Panel();
            this.panel91 = new System.Windows.Forms.Panel();
            this.panel92 = new System.Windows.Forms.Panel();
            this.panel93 = new System.Windows.Forms.Panel();
            this.panel94 = new System.Windows.Forms.Panel();
            this.btnCadastrar7 = new System.Windows.Forms.Button();
            this.btnRemover7 = new System.Windows.Forms.Button();
            this.btnConsultar7 = new System.Windows.Forms.Button();
            this.btnAlterar7 = new System.Windows.Forms.Button();
            this.pnlFunc = new System.Windows.Forms.Panel();
            this.panel87 = new System.Windows.Forms.Panel();
            this.panel88 = new System.Windows.Forms.Panel();
            this.panel89 = new System.Windows.Forms.Panel();
            this.panel90 = new System.Windows.Forms.Panel();
            this.btnCadastrar6 = new System.Windows.Forms.Button();
            this.btnRemover6 = new System.Windows.Forms.Button();
            this.btnConsultar6 = new System.Windows.Forms.Button();
            this.btnAlterar6 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblRelogio1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel68 = new System.Windows.Forms.Panel();
            this.panel67 = new System.Windows.Forms.Panel();
            this.panel65 = new System.Windows.Forms.Panel();
            this.panel64 = new System.Windows.Forms.Panel();
            this.panel63 = new System.Windows.Forms.Panel();
            this.panel62 = new System.Windows.Forms.Panel();
            this.panel61 = new System.Windows.Forms.Panel();
            this.panel60 = new System.Windows.Forms.Panel();
            this.panel59 = new System.Windows.Forms.Panel();
            this.panel55 = new System.Windows.Forms.Panel();
            this.panel66 = new System.Windows.Forms.Panel();
            this.panel56 = new System.Windows.Forms.Panel();
            this.panel53 = new System.Windows.Forms.Panel();
            this.panel54 = new System.Windows.Forms.Panel();
            this.panel51 = new System.Windows.Forms.Panel();
            this.panel52 = new System.Windows.Forms.Panel();
            this.panel50 = new System.Windows.Forms.Panel();
            this.panel47 = new System.Windows.Forms.Panel();
            this.panel46 = new System.Windows.Forms.Panel();
            this.panel45 = new System.Windows.Forms.Panel();
            this.panel44 = new System.Windows.Forms.Panel();
            this.panel43 = new System.Windows.Forms.Panel();
            this.panel42 = new System.Windows.Forms.Panel();
            this.panel41 = new System.Windows.Forms.Panel();
            this.panel38 = new System.Windows.Forms.Panel();
            this.panel39 = new System.Windows.Forms.Panel();
            this.panel37 = new System.Windows.Forms.Panel();
            this.panel36 = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel35 = new System.Windows.Forms.Panel();
            this.panel40 = new System.Windows.Forms.Panel();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel28 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.panel26 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel24 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel57 = new System.Windows.Forms.Panel();
            this.panel58 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btnProv = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.btnProdt = new System.Windows.Forms.Button();
            this.btnPacotes = new System.Windows.Forms.Button();
            this.btnJornada = new System.Windows.Forms.Button();
            this.btnHolerite = new System.Windows.Forms.Button();
            this.btnFunc = new System.Windows.Forms.Button();
            this.btnForn = new System.Windows.Forms.Button();
            this.btnCliente = new System.Windows.Forms.Button();
            this.btnCarro = new System.Windows.Forms.Button();
            this.btnAula = new System.Windows.Forms.Button();
            this.btnAbast = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblFechar = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel2.SuspendLayout();
            this.pnlProva.SuspendLayout();
            this.pnlCarro.SuspendLayout();
            this.pnlAula.SuspendLayout();
            this.pnlProd.SuspendLayout();
            this.pnlAbast.SuspendLayout();
            this.pnlPact.SuspendLayout();
            this.pnlJornada.SuspendLayout();
            this.pnlCliente.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel32.SuspendLayout();
            this.pnlForn.SuspendLayout();
            this.pnlHoler.SuspendLayout();
            this.pnlFunc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel55.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel14.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.pnlProva);
            this.panel2.Controls.Add(this.pnlCarro);
            this.panel2.Controls.Add(this.pnlAula);
            this.panel2.Controls.Add(this.pnlProd);
            this.panel2.Controls.Add(this.pnlAbast);
            this.panel2.Controls.Add(this.panel34);
            this.panel2.Controls.Add(this.pnlPact);
            this.panel2.Controls.Add(this.panel33);
            this.panel2.Controls.Add(this.pnlJornada);
            this.panel2.Controls.Add(this.pnlCliente);
            this.panel2.Controls.Add(this.panel31);
            this.panel2.Controls.Add(this.panel30);
            this.panel2.Controls.Add(this.pnlForn);
            this.panel2.Controls.Add(this.pnlHoler);
            this.panel2.Controls.Add(this.pnlFunc);
            this.panel2.Location = new System.Drawing.Point(334, 46);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(946, 673);
            this.panel2.TabIndex = 7;
            this.panel2.Click += new System.EventHandler(this.panel2_Click);
            // 
            // pnlProva
            // 
            this.pnlProva.BackColor = System.Drawing.Color.Transparent;
            this.pnlProva.Controls.Add(this.panel107);
            this.pnlProva.Controls.Add(this.panel108);
            this.pnlProva.Controls.Add(this.panel109);
            this.pnlProva.Controls.Add(this.panel110);
            this.pnlProva.Controls.Add(this.btnCadastrar11);
            this.pnlProva.Controls.Add(this.btnRemover11);
            this.pnlProva.Controls.Add(this.btnConsultar11);
            this.pnlProva.Controls.Add(this.btnAlterar11);
            this.pnlProva.Location = new System.Drawing.Point(3, 601);
            this.pnlProva.Name = "pnlProva";
            this.pnlProva.Size = new System.Drawing.Size(943, 44);
            this.pnlProva.TabIndex = 20;
            this.pnlProva.Visible = false;
            // 
            // panel107
            // 
            this.panel107.BackColor = System.Drawing.Color.White;
            this.panel107.Location = new System.Drawing.Point(709, 40);
            this.panel107.Name = "panel107";
            this.panel107.Size = new System.Drawing.Size(200, 1);
            this.panel107.TabIndex = 43;
            // 
            // panel108
            // 
            this.panel108.BackColor = System.Drawing.Color.White;
            this.panel108.Location = new System.Drawing.Point(485, 40);
            this.panel108.Name = "panel108";
            this.panel108.Size = new System.Drawing.Size(200, 1);
            this.panel108.TabIndex = 42;
            // 
            // panel109
            // 
            this.panel109.BackColor = System.Drawing.Color.White;
            this.panel109.Location = new System.Drawing.Point(254, 41);
            this.panel109.Name = "panel109";
            this.panel109.Size = new System.Drawing.Size(200, 1);
            this.panel109.TabIndex = 41;
            // 
            // panel110
            // 
            this.panel110.BackColor = System.Drawing.Color.White;
            this.panel110.Location = new System.Drawing.Point(22, 41);
            this.panel110.Name = "panel110";
            this.panel110.Size = new System.Drawing.Size(200, 1);
            this.panel110.TabIndex = 40;
            // 
            // btnCadastrar11
            // 
            this.btnCadastrar11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnCadastrar11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastrar11.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrar11.ForeColor = System.Drawing.Color.Aqua;
            this.btnCadastrar11.Location = new System.Drawing.Point(48, 3);
            this.btnCadastrar11.Name = "btnCadastrar11";
            this.btnCadastrar11.Size = new System.Drawing.Size(150, 30);
            this.btnCadastrar11.TabIndex = 51;
            this.btnCadastrar11.Text = "Cadastrar";
            this.btnCadastrar11.UseVisualStyleBackColor = false;
            this.btnCadastrar11.Click += new System.EventHandler(this.btnCadastrar11_Click);
            // 
            // btnRemover11
            // 
            this.btnRemover11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnRemover11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemover11.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemover11.ForeColor = System.Drawing.Color.Aqua;
            this.btnRemover11.Location = new System.Drawing.Point(737, 3);
            this.btnRemover11.Name = "btnRemover11";
            this.btnRemover11.Size = new System.Drawing.Size(150, 30);
            this.btnRemover11.TabIndex = 54;
            this.btnRemover11.Text = "Remover";
            this.btnRemover11.UseVisualStyleBackColor = false;
            this.btnRemover11.Click += new System.EventHandler(this.btnRemover11_Click);
            // 
            // btnConsultar11
            // 
            this.btnConsultar11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnConsultar11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultar11.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultar11.ForeColor = System.Drawing.Color.Aqua;
            this.btnConsultar11.Location = new System.Drawing.Point(280, 3);
            this.btnConsultar11.Name = "btnConsultar11";
            this.btnConsultar11.Size = new System.Drawing.Size(150, 30);
            this.btnConsultar11.TabIndex = 52;
            this.btnConsultar11.Text = "Consultar";
            this.btnConsultar11.UseVisualStyleBackColor = false;
            this.btnConsultar11.Click += new System.EventHandler(this.btnConsultar11_Click);
            // 
            // btnAlterar11
            // 
            this.btnAlterar11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnAlterar11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar11.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlterar11.ForeColor = System.Drawing.Color.Aqua;
            this.btnAlterar11.Location = new System.Drawing.Point(508, 3);
            this.btnAlterar11.Name = "btnAlterar11";
            this.btnAlterar11.Size = new System.Drawing.Size(150, 30);
            this.btnAlterar11.TabIndex = 53;
            this.btnAlterar11.Text = "Alterar";
            this.btnAlterar11.UseVisualStyleBackColor = false;
            this.btnAlterar11.Click += new System.EventHandler(this.btnAlterar11_Click);
            // 
            // pnlCarro
            // 
            this.pnlCarro.BackColor = System.Drawing.Color.Transparent;
            this.pnlCarro.Controls.Add(this.panel49);
            this.pnlCarro.Controls.Add(this.panel76);
            this.pnlCarro.Controls.Add(this.panel77);
            this.pnlCarro.Controls.Add(this.panel78);
            this.pnlCarro.Controls.Add(this.btnCadastrar3);
            this.pnlCarro.Controls.Add(this.btnRemover3);
            this.pnlCarro.Controls.Add(this.btnConsultar3);
            this.pnlCarro.Controls.Add(this.btnAlterar3);
            this.pnlCarro.Location = new System.Drawing.Point(3, 210);
            this.pnlCarro.Name = "pnlCarro";
            this.pnlCarro.Size = new System.Drawing.Size(943, 44);
            this.pnlCarro.TabIndex = 12;
            this.pnlCarro.Visible = false;
            // 
            // panel49
            // 
            this.panel49.BackColor = System.Drawing.Color.White;
            this.panel49.Location = new System.Drawing.Point(709, 42);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(200, 1);
            this.panel49.TabIndex = 31;
            // 
            // panel76
            // 
            this.panel76.BackColor = System.Drawing.Color.White;
            this.panel76.Location = new System.Drawing.Point(485, 42);
            this.panel76.Name = "panel76";
            this.panel76.Size = new System.Drawing.Size(200, 1);
            this.panel76.TabIndex = 30;
            // 
            // panel77
            // 
            this.panel77.BackColor = System.Drawing.Color.White;
            this.panel77.Location = new System.Drawing.Point(254, 42);
            this.panel77.Name = "panel77";
            this.panel77.Size = new System.Drawing.Size(200, 1);
            this.panel77.TabIndex = 29;
            // 
            // panel78
            // 
            this.panel78.BackColor = System.Drawing.Color.White;
            this.panel78.Location = new System.Drawing.Point(22, 42);
            this.panel78.Name = "panel78";
            this.panel78.Size = new System.Drawing.Size(200, 1);
            this.panel78.TabIndex = 28;
            // 
            // btnCadastrar3
            // 
            this.btnCadastrar3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnCadastrar3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastrar3.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrar3.ForeColor = System.Drawing.Color.Aqua;
            this.btnCadastrar3.Location = new System.Drawing.Point(48, 6);
            this.btnCadastrar3.Name = "btnCadastrar3";
            this.btnCadastrar3.Size = new System.Drawing.Size(150, 30);
            this.btnCadastrar3.TabIndex = 11;
            this.btnCadastrar3.Text = "Cadastrar";
            this.btnCadastrar3.UseVisualStyleBackColor = false;
            this.btnCadastrar3.Click += new System.EventHandler(this.btnCadastrar3_Click);
            // 
            // btnRemover3
            // 
            this.btnRemover3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnRemover3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemover3.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemover3.ForeColor = System.Drawing.Color.Aqua;
            this.btnRemover3.Location = new System.Drawing.Point(737, 7);
            this.btnRemover3.Name = "btnRemover3";
            this.btnRemover3.Size = new System.Drawing.Size(150, 30);
            this.btnRemover3.TabIndex = 14;
            this.btnRemover3.Text = "Remover";
            this.btnRemover3.UseVisualStyleBackColor = false;
            this.btnRemover3.Click += new System.EventHandler(this.btnRemover3_Click);
            // 
            // btnConsultar3
            // 
            this.btnConsultar3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnConsultar3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultar3.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultar3.ForeColor = System.Drawing.Color.Aqua;
            this.btnConsultar3.Location = new System.Drawing.Point(280, 6);
            this.btnConsultar3.Name = "btnConsultar3";
            this.btnConsultar3.Size = new System.Drawing.Size(150, 30);
            this.btnConsultar3.TabIndex = 12;
            this.btnConsultar3.Text = "Consultar";
            this.btnConsultar3.UseVisualStyleBackColor = false;
            this.btnConsultar3.Click += new System.EventHandler(this.btnConsultar3_Click);
            // 
            // btnAlterar3
            // 
            this.btnAlterar3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnAlterar3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar3.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlterar3.ForeColor = System.Drawing.Color.Aqua;
            this.btnAlterar3.Location = new System.Drawing.Point(508, 6);
            this.btnAlterar3.Name = "btnAlterar3";
            this.btnAlterar3.Size = new System.Drawing.Size(150, 30);
            this.btnAlterar3.TabIndex = 13;
            this.btnAlterar3.Text = "Alterar";
            this.btnAlterar3.UseVisualStyleBackColor = false;
            this.btnAlterar3.Click += new System.EventHandler(this.btnAlterar3_Click);
            // 
            // pnlAula
            // 
            this.pnlAula.BackColor = System.Drawing.Color.Transparent;
            this.pnlAula.Controls.Add(this.panel72);
            this.pnlAula.Controls.Add(this.panel73);
            this.pnlAula.Controls.Add(this.panel74);
            this.pnlAula.Controls.Add(this.panel75);
            this.pnlAula.Controls.Add(this.btnCadastrar2);
            this.pnlAula.Controls.Add(this.btnRemover2);
            this.pnlAula.Controls.Add(this.btnConsultar2);
            this.pnlAula.Controls.Add(this.btnAlterar2);
            this.pnlAula.Location = new System.Drawing.Point(3, 162);
            this.pnlAula.Name = "pnlAula";
            this.pnlAula.Size = new System.Drawing.Size(943, 44);
            this.pnlAula.TabIndex = 11;
            this.pnlAula.Visible = false;
            // 
            // panel72
            // 
            this.panel72.BackColor = System.Drawing.Color.White;
            this.panel72.Location = new System.Drawing.Point(709, 41);
            this.panel72.Name = "panel72";
            this.panel72.Size = new System.Drawing.Size(200, 1);
            this.panel72.TabIndex = 27;
            // 
            // panel73
            // 
            this.panel73.BackColor = System.Drawing.Color.White;
            this.panel73.Location = new System.Drawing.Point(485, 41);
            this.panel73.Name = "panel73";
            this.panel73.Size = new System.Drawing.Size(200, 1);
            this.panel73.TabIndex = 26;
            // 
            // panel74
            // 
            this.panel74.BackColor = System.Drawing.Color.White;
            this.panel74.Location = new System.Drawing.Point(254, 41);
            this.panel74.Name = "panel74";
            this.panel74.Size = new System.Drawing.Size(200, 1);
            this.panel74.TabIndex = 25;
            // 
            // panel75
            // 
            this.panel75.BackColor = System.Drawing.Color.White;
            this.panel75.Location = new System.Drawing.Point(22, 41);
            this.panel75.Name = "panel75";
            this.panel75.Size = new System.Drawing.Size(200, 1);
            this.panel75.TabIndex = 24;
            // 
            // btnCadastrar2
            // 
            this.btnCadastrar2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnCadastrar2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastrar2.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrar2.ForeColor = System.Drawing.Color.Aqua;
            this.btnCadastrar2.Location = new System.Drawing.Point(48, 5);
            this.btnCadastrar2.Name = "btnCadastrar2";
            this.btnCadastrar2.Size = new System.Drawing.Size(150, 30);
            this.btnCadastrar2.TabIndex = 6;
            this.btnCadastrar2.Text = "Cadastrar";
            this.btnCadastrar2.UseVisualStyleBackColor = false;
            this.btnCadastrar2.Click += new System.EventHandler(this.btnCadastrar2_Click);
            // 
            // btnRemover2
            // 
            this.btnRemover2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnRemover2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemover2.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemover2.ForeColor = System.Drawing.Color.Aqua;
            this.btnRemover2.Location = new System.Drawing.Point(737, 5);
            this.btnRemover2.Name = "btnRemover2";
            this.btnRemover2.Size = new System.Drawing.Size(150, 30);
            this.btnRemover2.TabIndex = 9;
            this.btnRemover2.Text = "Remover";
            this.btnRemover2.UseVisualStyleBackColor = false;
            this.btnRemover2.Click += new System.EventHandler(this.btnRemover2_Click);
            // 
            // btnConsultar2
            // 
            this.btnConsultar2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnConsultar2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultar2.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultar2.ForeColor = System.Drawing.Color.Aqua;
            this.btnConsultar2.Location = new System.Drawing.Point(280, 5);
            this.btnConsultar2.Name = "btnConsultar2";
            this.btnConsultar2.Size = new System.Drawing.Size(150, 30);
            this.btnConsultar2.TabIndex = 7;
            this.btnConsultar2.Text = "Consultar";
            this.btnConsultar2.UseVisualStyleBackColor = false;
            this.btnConsultar2.Click += new System.EventHandler(this.btnConsultar2_Click);
            // 
            // btnAlterar2
            // 
            this.btnAlterar2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnAlterar2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar2.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlterar2.ForeColor = System.Drawing.Color.Aqua;
            this.btnAlterar2.Location = new System.Drawing.Point(508, 5);
            this.btnAlterar2.Name = "btnAlterar2";
            this.btnAlterar2.Size = new System.Drawing.Size(150, 30);
            this.btnAlterar2.TabIndex = 8;
            this.btnAlterar2.Text = "Alterar";
            this.btnAlterar2.UseVisualStyleBackColor = false;
            this.btnAlterar2.Click += new System.EventHandler(this.btnAlterar2_Click);
            // 
            // pnlProd
            // 
            this.pnlProd.BackColor = System.Drawing.Color.Transparent;
            this.pnlProd.Controls.Add(this.panel103);
            this.pnlProd.Controls.Add(this.panel104);
            this.pnlProd.Controls.Add(this.panel105);
            this.pnlProd.Controls.Add(this.panel106);
            this.pnlProd.Controls.Add(this.btnCadastrar10);
            this.pnlProd.Controls.Add(this.btnRemover10);
            this.pnlProd.Controls.Add(this.btnConsultar10);
            this.pnlProd.Controls.Add(this.btnAlterar10);
            this.pnlProd.Location = new System.Drawing.Point(3, 553);
            this.pnlProd.Name = "pnlProd";
            this.pnlProd.Size = new System.Drawing.Size(943, 44);
            this.pnlProd.TabIndex = 19;
            this.pnlProd.Visible = false;
            // 
            // panel103
            // 
            this.panel103.BackColor = System.Drawing.Color.White;
            this.panel103.Location = new System.Drawing.Point(709, 41);
            this.panel103.Name = "panel103";
            this.panel103.Size = new System.Drawing.Size(200, 1);
            this.panel103.TabIndex = 43;
            // 
            // panel104
            // 
            this.panel104.BackColor = System.Drawing.Color.White;
            this.panel104.Location = new System.Drawing.Point(485, 41);
            this.panel104.Name = "panel104";
            this.panel104.Size = new System.Drawing.Size(200, 1);
            this.panel104.TabIndex = 42;
            // 
            // panel105
            // 
            this.panel105.BackColor = System.Drawing.Color.White;
            this.panel105.Location = new System.Drawing.Point(254, 42);
            this.panel105.Name = "panel105";
            this.panel105.Size = new System.Drawing.Size(200, 1);
            this.panel105.TabIndex = 41;
            // 
            // panel106
            // 
            this.panel106.BackColor = System.Drawing.Color.White;
            this.panel106.Location = new System.Drawing.Point(22, 42);
            this.panel106.Name = "panel106";
            this.panel106.Size = new System.Drawing.Size(200, 1);
            this.panel106.TabIndex = 40;
            // 
            // btnCadastrar10
            // 
            this.btnCadastrar10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnCadastrar10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastrar10.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrar10.ForeColor = System.Drawing.Color.Aqua;
            this.btnCadastrar10.Location = new System.Drawing.Point(48, 3);
            this.btnCadastrar10.Name = "btnCadastrar10";
            this.btnCadastrar10.Size = new System.Drawing.Size(150, 30);
            this.btnCadastrar10.TabIndex = 46;
            this.btnCadastrar10.Text = "Cadastrar";
            this.btnCadastrar10.UseVisualStyleBackColor = false;
            this.btnCadastrar10.Click += new System.EventHandler(this.btnCadastrar10_Click);
            // 
            // btnRemover10
            // 
            this.btnRemover10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnRemover10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemover10.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemover10.ForeColor = System.Drawing.Color.Aqua;
            this.btnRemover10.Location = new System.Drawing.Point(737, 3);
            this.btnRemover10.Name = "btnRemover10";
            this.btnRemover10.Size = new System.Drawing.Size(150, 30);
            this.btnRemover10.TabIndex = 49;
            this.btnRemover10.Text = "Remover";
            this.btnRemover10.UseVisualStyleBackColor = false;
            this.btnRemover10.Click += new System.EventHandler(this.btnRemover10_Click);
            // 
            // btnConsultar10
            // 
            this.btnConsultar10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnConsultar10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultar10.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultar10.ForeColor = System.Drawing.Color.Aqua;
            this.btnConsultar10.Location = new System.Drawing.Point(280, 3);
            this.btnConsultar10.Name = "btnConsultar10";
            this.btnConsultar10.Size = new System.Drawing.Size(150, 30);
            this.btnConsultar10.TabIndex = 47;
            this.btnConsultar10.Text = "Consultar";
            this.btnConsultar10.UseVisualStyleBackColor = false;
            this.btnConsultar10.Click += new System.EventHandler(this.btnConsultar10_Click);
            // 
            // btnAlterar10
            // 
            this.btnAlterar10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnAlterar10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar10.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlterar10.ForeColor = System.Drawing.Color.Aqua;
            this.btnAlterar10.Location = new System.Drawing.Point(508, 3);
            this.btnAlterar10.Name = "btnAlterar10";
            this.btnAlterar10.Size = new System.Drawing.Size(150, 30);
            this.btnAlterar10.TabIndex = 48;
            this.btnAlterar10.Text = "Alterar";
            this.btnAlterar10.UseVisualStyleBackColor = false;
            this.btnAlterar10.Click += new System.EventHandler(this.btnAlterar10_Click);
            // 
            // pnlAbast
            // 
            this.pnlAbast.BackColor = System.Drawing.Color.Transparent;
            this.pnlAbast.Controls.Add(this.panel71);
            this.pnlAbast.Controls.Add(this.panel70);
            this.pnlAbast.Controls.Add(this.panel69);
            this.pnlAbast.Controls.Add(this.panel48);
            this.pnlAbast.Controls.Add(this.btnCadastrar1);
            this.pnlAbast.Controls.Add(this.btnRemover1);
            this.pnlAbast.Controls.Add(this.btnConsultar1);
            this.pnlAbast.Controls.Add(this.btnAlterar1);
            this.pnlAbast.Location = new System.Drawing.Point(3, 107);
            this.pnlAbast.Name = "pnlAbast";
            this.pnlAbast.Size = new System.Drawing.Size(943, 49);
            this.pnlAbast.TabIndex = 10;
            this.pnlAbast.Visible = false;
            // 
            // panel71
            // 
            this.panel71.BackColor = System.Drawing.Color.White;
            this.panel71.Location = new System.Drawing.Point(709, 46);
            this.panel71.Name = "panel71";
            this.panel71.Size = new System.Drawing.Size(200, 1);
            this.panel71.TabIndex = 23;
            // 
            // panel70
            // 
            this.panel70.BackColor = System.Drawing.Color.White;
            this.panel70.Location = new System.Drawing.Point(485, 46);
            this.panel70.Name = "panel70";
            this.panel70.Size = new System.Drawing.Size(200, 1);
            this.panel70.TabIndex = 22;
            // 
            // panel69
            // 
            this.panel69.BackColor = System.Drawing.Color.White;
            this.panel69.Location = new System.Drawing.Point(254, 46);
            this.panel69.Name = "panel69";
            this.panel69.Size = new System.Drawing.Size(200, 1);
            this.panel69.TabIndex = 21;
            // 
            // panel48
            // 
            this.panel48.BackColor = System.Drawing.Color.White;
            this.panel48.Location = new System.Drawing.Point(22, 46);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(200, 1);
            this.panel48.TabIndex = 5;
            // 
            // btnCadastrar1
            // 
            this.btnCadastrar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnCadastrar1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastrar1.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrar1.ForeColor = System.Drawing.Color.Aqua;
            this.btnCadastrar1.Location = new System.Drawing.Point(48, 10);
            this.btnCadastrar1.Name = "btnCadastrar1";
            this.btnCadastrar1.Size = new System.Drawing.Size(150, 30);
            this.btnCadastrar1.TabIndex = 1;
            this.btnCadastrar1.Text = "Cadastrar";
            this.btnCadastrar1.UseVisualStyleBackColor = false;
            this.btnCadastrar1.Click += new System.EventHandler(this.btnCadastrar1_Click);
            // 
            // btnRemover1
            // 
            this.btnRemover1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnRemover1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemover1.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemover1.ForeColor = System.Drawing.Color.Aqua;
            this.btnRemover1.Location = new System.Drawing.Point(737, 10);
            this.btnRemover1.Name = "btnRemover1";
            this.btnRemover1.Size = new System.Drawing.Size(150, 30);
            this.btnRemover1.TabIndex = 4;
            this.btnRemover1.Text = "Remover";
            this.btnRemover1.UseVisualStyleBackColor = false;
            this.btnRemover1.Click += new System.EventHandler(this.btnRemover1_Click);
            // 
            // btnConsultar1
            // 
            this.btnConsultar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnConsultar1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultar1.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultar1.ForeColor = System.Drawing.Color.Aqua;
            this.btnConsultar1.Location = new System.Drawing.Point(280, 10);
            this.btnConsultar1.Name = "btnConsultar1";
            this.btnConsultar1.Size = new System.Drawing.Size(150, 30);
            this.btnConsultar1.TabIndex = 2;
            this.btnConsultar1.Text = "Consultar";
            this.btnConsultar1.UseVisualStyleBackColor = false;
            this.btnConsultar1.Click += new System.EventHandler(this.btnConsultar1_Click);
            // 
            // btnAlterar1
            // 
            this.btnAlterar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnAlterar1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar1.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlterar1.ForeColor = System.Drawing.Color.Aqua;
            this.btnAlterar1.Location = new System.Drawing.Point(508, 10);
            this.btnAlterar1.Name = "btnAlterar1";
            this.btnAlterar1.Size = new System.Drawing.Size(150, 30);
            this.btnAlterar1.TabIndex = 3;
            this.btnAlterar1.Text = "Alterar";
            this.btnAlterar1.UseVisualStyleBackColor = false;
            this.btnAlterar1.Click += new System.EventHandler(this.btnAlterar1_Click);
            // 
            // panel34
            // 
            this.panel34.Location = new System.Drawing.Point(1, 649);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(946, 1);
            this.panel34.TabIndex = 9;
            // 
            // pnlPact
            // 
            this.pnlPact.BackColor = System.Drawing.Color.Transparent;
            this.pnlPact.Controls.Add(this.panel99);
            this.pnlPact.Controls.Add(this.panel100);
            this.pnlPact.Controls.Add(this.panel101);
            this.pnlPact.Controls.Add(this.panel102);
            this.pnlPact.Controls.Add(this.btnCadastrar9);
            this.pnlPact.Controls.Add(this.btnRemover9);
            this.pnlPact.Controls.Add(this.btnConsultar9);
            this.pnlPact.Controls.Add(this.btnAlterar9);
            this.pnlPact.Location = new System.Drawing.Point(3, 503);
            this.pnlPact.Name = "pnlPact";
            this.pnlPact.Size = new System.Drawing.Size(943, 44);
            this.pnlPact.TabIndex = 18;
            this.pnlPact.Visible = false;
            // 
            // panel99
            // 
            this.panel99.BackColor = System.Drawing.Color.White;
            this.panel99.Location = new System.Drawing.Point(709, 42);
            this.panel99.Name = "panel99";
            this.panel99.Size = new System.Drawing.Size(200, 1);
            this.panel99.TabIndex = 43;
            // 
            // panel100
            // 
            this.panel100.BackColor = System.Drawing.Color.White;
            this.panel100.Location = new System.Drawing.Point(485, 42);
            this.panel100.Name = "panel100";
            this.panel100.Size = new System.Drawing.Size(200, 1);
            this.panel100.TabIndex = 42;
            // 
            // panel101
            // 
            this.panel101.BackColor = System.Drawing.Color.White;
            this.panel101.Location = new System.Drawing.Point(254, 43);
            this.panel101.Name = "panel101";
            this.panel101.Size = new System.Drawing.Size(200, 1);
            this.panel101.TabIndex = 41;
            // 
            // panel102
            // 
            this.panel102.BackColor = System.Drawing.Color.White;
            this.panel102.Location = new System.Drawing.Point(22, 43);
            this.panel102.Name = "panel102";
            this.panel102.Size = new System.Drawing.Size(200, 1);
            this.panel102.TabIndex = 40;
            // 
            // btnCadastrar9
            // 
            this.btnCadastrar9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnCadastrar9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastrar9.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrar9.ForeColor = System.Drawing.Color.Aqua;
            this.btnCadastrar9.Location = new System.Drawing.Point(48, 5);
            this.btnCadastrar9.Name = "btnCadastrar9";
            this.btnCadastrar9.Size = new System.Drawing.Size(150, 30);
            this.btnCadastrar9.TabIndex = 41;
            this.btnCadastrar9.Text = "Cadastrar";
            this.btnCadastrar9.UseVisualStyleBackColor = false;
            this.btnCadastrar9.Click += new System.EventHandler(this.btnCadastrar9_Click);
            // 
            // btnRemover9
            // 
            this.btnRemover9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnRemover9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemover9.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemover9.ForeColor = System.Drawing.Color.Aqua;
            this.btnRemover9.Location = new System.Drawing.Point(737, 5);
            this.btnRemover9.Name = "btnRemover9";
            this.btnRemover9.Size = new System.Drawing.Size(150, 30);
            this.btnRemover9.TabIndex = 44;
            this.btnRemover9.Text = "Remover";
            this.btnRemover9.UseVisualStyleBackColor = false;
            this.btnRemover9.Click += new System.EventHandler(this.btnRemover9_Click);
            // 
            // btnConsultar9
            // 
            this.btnConsultar9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnConsultar9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultar9.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultar9.ForeColor = System.Drawing.Color.Aqua;
            this.btnConsultar9.Location = new System.Drawing.Point(280, 5);
            this.btnConsultar9.Name = "btnConsultar9";
            this.btnConsultar9.Size = new System.Drawing.Size(150, 30);
            this.btnConsultar9.TabIndex = 42;
            this.btnConsultar9.Text = "Consultar";
            this.btnConsultar9.UseVisualStyleBackColor = false;
            this.btnConsultar9.Click += new System.EventHandler(this.btnConsultar9_Click);
            // 
            // btnAlterar9
            // 
            this.btnAlterar9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnAlterar9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar9.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlterar9.ForeColor = System.Drawing.Color.Aqua;
            this.btnAlterar9.Location = new System.Drawing.Point(508, 7);
            this.btnAlterar9.Name = "btnAlterar9";
            this.btnAlterar9.Size = new System.Drawing.Size(150, 30);
            this.btnAlterar9.TabIndex = 43;
            this.btnAlterar9.Text = "Alterar";
            this.btnAlterar9.UseVisualStyleBackColor = false;
            this.btnAlterar9.Click += new System.EventHandler(this.btnAlterar9_Click);
            // 
            // panel33
            // 
            this.panel33.BackColor = System.Drawing.Color.White;
            this.panel33.Location = new System.Drawing.Point(0, 0);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(1, 674);
            this.panel33.TabIndex = 8;
            // 
            // pnlJornada
            // 
            this.pnlJornada.BackColor = System.Drawing.Color.Transparent;
            this.pnlJornada.Controls.Add(this.panel95);
            this.pnlJornada.Controls.Add(this.panel96);
            this.pnlJornada.Controls.Add(this.panel97);
            this.pnlJornada.Controls.Add(this.panel98);
            this.pnlJornada.Controls.Add(this.btnCadastrar8);
            this.pnlJornada.Controls.Add(this.btnRemover8);
            this.pnlJornada.Controls.Add(this.btnConsultar8);
            this.pnlJornada.Controls.Add(this.btnAlterar8);
            this.pnlJornada.Location = new System.Drawing.Point(3, 455);
            this.pnlJornada.Name = "pnlJornada";
            this.pnlJornada.Size = new System.Drawing.Size(943, 44);
            this.pnlJornada.TabIndex = 17;
            this.pnlJornada.Visible = false;
            // 
            // panel95
            // 
            this.panel95.BackColor = System.Drawing.Color.White;
            this.panel95.Location = new System.Drawing.Point(709, 41);
            this.panel95.Name = "panel95";
            this.panel95.Size = new System.Drawing.Size(200, 1);
            this.panel95.TabIndex = 39;
            // 
            // panel96
            // 
            this.panel96.BackColor = System.Drawing.Color.White;
            this.panel96.Location = new System.Drawing.Point(485, 41);
            this.panel96.Name = "panel96";
            this.panel96.Size = new System.Drawing.Size(200, 1);
            this.panel96.TabIndex = 38;
            // 
            // panel97
            // 
            this.panel97.BackColor = System.Drawing.Color.White;
            this.panel97.Location = new System.Drawing.Point(254, 42);
            this.panel97.Name = "panel97";
            this.panel97.Size = new System.Drawing.Size(200, 1);
            this.panel97.TabIndex = 37;
            // 
            // panel98
            // 
            this.panel98.BackColor = System.Drawing.Color.White;
            this.panel98.Location = new System.Drawing.Point(22, 42);
            this.panel98.Name = "panel98";
            this.panel98.Size = new System.Drawing.Size(200, 1);
            this.panel98.TabIndex = 36;
            // 
            // btnCadastrar8
            // 
            this.btnCadastrar8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnCadastrar8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastrar8.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrar8.ForeColor = System.Drawing.Color.Aqua;
            this.btnCadastrar8.Location = new System.Drawing.Point(48, 5);
            this.btnCadastrar8.Name = "btnCadastrar8";
            this.btnCadastrar8.Size = new System.Drawing.Size(150, 30);
            this.btnCadastrar8.TabIndex = 36;
            this.btnCadastrar8.Text = "Cadastrar";
            this.btnCadastrar8.UseVisualStyleBackColor = false;
            this.btnCadastrar8.Click += new System.EventHandler(this.btnCadastrar8_Click);
            // 
            // btnRemover8
            // 
            this.btnRemover8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnRemover8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemover8.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemover8.ForeColor = System.Drawing.Color.Aqua;
            this.btnRemover8.Location = new System.Drawing.Point(737, 6);
            this.btnRemover8.Name = "btnRemover8";
            this.btnRemover8.Size = new System.Drawing.Size(150, 30);
            this.btnRemover8.TabIndex = 39;
            this.btnRemover8.Text = "Remover";
            this.btnRemover8.UseVisualStyleBackColor = false;
            this.btnRemover8.Click += new System.EventHandler(this.btnRemover8_Click);
            // 
            // btnConsultar8
            // 
            this.btnConsultar8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnConsultar8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultar8.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultar8.ForeColor = System.Drawing.Color.Aqua;
            this.btnConsultar8.Location = new System.Drawing.Point(280, 6);
            this.btnConsultar8.Name = "btnConsultar8";
            this.btnConsultar8.Size = new System.Drawing.Size(150, 30);
            this.btnConsultar8.TabIndex = 37;
            this.btnConsultar8.Text = "Consultar";
            this.btnConsultar8.UseVisualStyleBackColor = false;
            this.btnConsultar8.Click += new System.EventHandler(this.btnConsultar8_Click);
            // 
            // btnAlterar8
            // 
            this.btnAlterar8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnAlterar8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar8.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlterar8.ForeColor = System.Drawing.Color.Aqua;
            this.btnAlterar8.Location = new System.Drawing.Point(508, 6);
            this.btnAlterar8.Name = "btnAlterar8";
            this.btnAlterar8.Size = new System.Drawing.Size(150, 30);
            this.btnAlterar8.TabIndex = 38;
            this.btnAlterar8.Text = "Alterar";
            this.btnAlterar8.UseVisualStyleBackColor = false;
            this.btnAlterar8.Click += new System.EventHandler(this.btnAlterar8_Click);
            // 
            // pnlCliente
            // 
            this.pnlCliente.BackColor = System.Drawing.Color.Transparent;
            this.pnlCliente.Controls.Add(this.panel79);
            this.pnlCliente.Controls.Add(this.panel80);
            this.pnlCliente.Controls.Add(this.panel81);
            this.pnlCliente.Controls.Add(this.panel82);
            this.pnlCliente.Controls.Add(this.btnCadastrar4);
            this.pnlCliente.Controls.Add(this.btnRemover4);
            this.pnlCliente.Controls.Add(this.btnConsultar4);
            this.pnlCliente.Controls.Add(this.btnAlterar4);
            this.pnlCliente.Location = new System.Drawing.Point(3, 259);
            this.pnlCliente.Name = "pnlCliente";
            this.pnlCliente.Size = new System.Drawing.Size(943, 44);
            this.pnlCliente.TabIndex = 13;
            this.pnlCliente.Visible = false;
            // 
            // panel79
            // 
            this.panel79.BackColor = System.Drawing.Color.White;
            this.panel79.Location = new System.Drawing.Point(709, 42);
            this.panel79.Name = "panel79";
            this.panel79.Size = new System.Drawing.Size(200, 1);
            this.panel79.TabIndex = 35;
            // 
            // panel80
            // 
            this.panel80.BackColor = System.Drawing.Color.White;
            this.panel80.Location = new System.Drawing.Point(485, 42);
            this.panel80.Name = "panel80";
            this.panel80.Size = new System.Drawing.Size(200, 1);
            this.panel80.TabIndex = 34;
            // 
            // panel81
            // 
            this.panel81.BackColor = System.Drawing.Color.White;
            this.panel81.Location = new System.Drawing.Point(254, 42);
            this.panel81.Name = "panel81";
            this.panel81.Size = new System.Drawing.Size(200, 1);
            this.panel81.TabIndex = 33;
            // 
            // panel82
            // 
            this.panel82.BackColor = System.Drawing.Color.White;
            this.panel82.Location = new System.Drawing.Point(22, 43);
            this.panel82.Name = "panel82";
            this.panel82.Size = new System.Drawing.Size(200, 1);
            this.panel82.TabIndex = 32;
            // 
            // btnCadastrar4
            // 
            this.btnCadastrar4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnCadastrar4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastrar4.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrar4.ForeColor = System.Drawing.Color.Aqua;
            this.btnCadastrar4.Location = new System.Drawing.Point(48, 7);
            this.btnCadastrar4.Name = "btnCadastrar4";
            this.btnCadastrar4.Size = new System.Drawing.Size(150, 30);
            this.btnCadastrar4.TabIndex = 16;
            this.btnCadastrar4.Text = "Cadastrar";
            this.btnCadastrar4.UseVisualStyleBackColor = false;
            this.btnCadastrar4.Click += new System.EventHandler(this.btnCadastrar4_Click);
            // 
            // btnRemover4
            // 
            this.btnRemover4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnRemover4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemover4.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemover4.ForeColor = System.Drawing.Color.Aqua;
            this.btnRemover4.Location = new System.Drawing.Point(737, 6);
            this.btnRemover4.Name = "btnRemover4";
            this.btnRemover4.Size = new System.Drawing.Size(150, 30);
            this.btnRemover4.TabIndex = 19;
            this.btnRemover4.Text = "Remover";
            this.btnRemover4.UseVisualStyleBackColor = false;
            this.btnRemover4.Click += new System.EventHandler(this.btnRemover4_Click);
            // 
            // btnConsultar4
            // 
            this.btnConsultar4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnConsultar4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultar4.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultar4.ForeColor = System.Drawing.Color.Aqua;
            this.btnConsultar4.Location = new System.Drawing.Point(280, 7);
            this.btnConsultar4.Name = "btnConsultar4";
            this.btnConsultar4.Size = new System.Drawing.Size(150, 30);
            this.btnConsultar4.TabIndex = 17;
            this.btnConsultar4.Text = "Consultar";
            this.btnConsultar4.UseVisualStyleBackColor = false;
            this.btnConsultar4.Click += new System.EventHandler(this.btnConsultar4_Click);
            // 
            // btnAlterar4
            // 
            this.btnAlterar4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnAlterar4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar4.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlterar4.ForeColor = System.Drawing.Color.Aqua;
            this.btnAlterar4.Location = new System.Drawing.Point(508, 7);
            this.btnAlterar4.Name = "btnAlterar4";
            this.btnAlterar4.Size = new System.Drawing.Size(150, 30);
            this.btnAlterar4.TabIndex = 18;
            this.btnAlterar4.Text = "Alterar";
            this.btnAlterar4.UseVisualStyleBackColor = false;
            this.btnAlterar4.Click += new System.EventHandler(this.btnAlterar4_Click);
            // 
            // panel31
            // 
            this.panel31.BackColor = System.Drawing.Color.White;
            this.panel31.Location = new System.Drawing.Point(-1, 0);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(1, 674);
            this.panel31.TabIndex = 7;
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel30.Controls.Add(this.panel32);
            this.panel30.Location = new System.Drawing.Point(-1, 650);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(947, 22);
            this.panel30.TabIndex = 6;
            // 
            // panel32
            // 
            this.panel32.BackColor = System.Drawing.Color.Red;
            this.panel32.Controls.Add(this.button13);
            this.panel32.Location = new System.Drawing.Point(1243, 12);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(25, 25);
            this.panel32.TabIndex = 0;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.Red;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Segoe UI Symbol", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.ForeColor = System.Drawing.Color.White;
            this.button13.Location = new System.Drawing.Point(-6, -7);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(38, 34);
            this.button13.TabIndex = 0;
            this.button13.Text = "X";
            this.button13.UseVisualStyleBackColor = false;
            // 
            // pnlForn
            // 
            this.pnlForn.BackColor = System.Drawing.Color.Transparent;
            this.pnlForn.Controls.Add(this.panel83);
            this.pnlForn.Controls.Add(this.panel84);
            this.pnlForn.Controls.Add(this.panel85);
            this.pnlForn.Controls.Add(this.panel86);
            this.pnlForn.Controls.Add(this.btnCadastrar5);
            this.pnlForn.Controls.Add(this.btnRemover5);
            this.pnlForn.Controls.Add(this.btnConsultar5);
            this.pnlForn.Controls.Add(this.btnAlterar5);
            this.pnlForn.Location = new System.Drawing.Point(3, 307);
            this.pnlForn.Name = "pnlForn";
            this.pnlForn.Size = new System.Drawing.Size(943, 44);
            this.pnlForn.TabIndex = 14;
            this.pnlForn.Visible = false;
            // 
            // panel83
            // 
            this.panel83.BackColor = System.Drawing.Color.White;
            this.panel83.Location = new System.Drawing.Point(709, 42);
            this.panel83.Name = "panel83";
            this.panel83.Size = new System.Drawing.Size(200, 1);
            this.panel83.TabIndex = 35;
            // 
            // panel84
            // 
            this.panel84.BackColor = System.Drawing.Color.White;
            this.panel84.Location = new System.Drawing.Point(485, 42);
            this.panel84.Name = "panel84";
            this.panel84.Size = new System.Drawing.Size(200, 1);
            this.panel84.TabIndex = 34;
            // 
            // panel85
            // 
            this.panel85.BackColor = System.Drawing.Color.White;
            this.panel85.Location = new System.Drawing.Point(254, 42);
            this.panel85.Name = "panel85";
            this.panel85.Size = new System.Drawing.Size(200, 1);
            this.panel85.TabIndex = 33;
            // 
            // panel86
            // 
            this.panel86.BackColor = System.Drawing.Color.White;
            this.panel86.Location = new System.Drawing.Point(22, 42);
            this.panel86.Name = "panel86";
            this.panel86.Size = new System.Drawing.Size(200, 1);
            this.panel86.TabIndex = 32;
            // 
            // btnCadastrar5
            // 
            this.btnCadastrar5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnCadastrar5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastrar5.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrar5.ForeColor = System.Drawing.Color.Aqua;
            this.btnCadastrar5.Location = new System.Drawing.Point(48, 6);
            this.btnCadastrar5.Name = "btnCadastrar5";
            this.btnCadastrar5.Size = new System.Drawing.Size(150, 30);
            this.btnCadastrar5.TabIndex = 21;
            this.btnCadastrar5.Text = "Cadastrar";
            this.btnCadastrar5.UseVisualStyleBackColor = false;
            this.btnCadastrar5.Click += new System.EventHandler(this.btnCadastrar5_Click);
            // 
            // btnRemover5
            // 
            this.btnRemover5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnRemover5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemover5.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemover5.ForeColor = System.Drawing.Color.Aqua;
            this.btnRemover5.Location = new System.Drawing.Point(737, 6);
            this.btnRemover5.Name = "btnRemover5";
            this.btnRemover5.Size = new System.Drawing.Size(150, 30);
            this.btnRemover5.TabIndex = 24;
            this.btnRemover5.Text = "Remover";
            this.btnRemover5.UseVisualStyleBackColor = false;
            this.btnRemover5.Click += new System.EventHandler(this.btnRemover5_Click);
            // 
            // btnConsultar5
            // 
            this.btnConsultar5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnConsultar5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultar5.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultar5.ForeColor = System.Drawing.Color.Aqua;
            this.btnConsultar5.Location = new System.Drawing.Point(280, 6);
            this.btnConsultar5.Name = "btnConsultar5";
            this.btnConsultar5.Size = new System.Drawing.Size(150, 30);
            this.btnConsultar5.TabIndex = 22;
            this.btnConsultar5.Text = "Consultar";
            this.btnConsultar5.UseVisualStyleBackColor = false;
            this.btnConsultar5.Click += new System.EventHandler(this.btnConsultar5_Click);
            // 
            // btnAlterar5
            // 
            this.btnAlterar5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnAlterar5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar5.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlterar5.ForeColor = System.Drawing.Color.Aqua;
            this.btnAlterar5.Location = new System.Drawing.Point(508, 6);
            this.btnAlterar5.Name = "btnAlterar5";
            this.btnAlterar5.Size = new System.Drawing.Size(150, 30);
            this.btnAlterar5.TabIndex = 23;
            this.btnAlterar5.Text = "Alterar";
            this.btnAlterar5.UseVisualStyleBackColor = false;
            this.btnAlterar5.Click += new System.EventHandler(this.btnAlterar5_Click);
            // 
            // pnlHoler
            // 
            this.pnlHoler.BackColor = System.Drawing.Color.Transparent;
            this.pnlHoler.Controls.Add(this.panel91);
            this.pnlHoler.Controls.Add(this.panel92);
            this.pnlHoler.Controls.Add(this.panel93);
            this.pnlHoler.Controls.Add(this.panel94);
            this.pnlHoler.Controls.Add(this.btnCadastrar7);
            this.pnlHoler.Controls.Add(this.btnRemover7);
            this.pnlHoler.Controls.Add(this.btnConsultar7);
            this.pnlHoler.Controls.Add(this.btnAlterar7);
            this.pnlHoler.Location = new System.Drawing.Point(1, 405);
            this.pnlHoler.Name = "pnlHoler";
            this.pnlHoler.Size = new System.Drawing.Size(943, 44);
            this.pnlHoler.TabIndex = 16;
            this.pnlHoler.Visible = false;
            // 
            // panel91
            // 
            this.panel91.BackColor = System.Drawing.Color.White;
            this.panel91.Location = new System.Drawing.Point(711, 41);
            this.panel91.Name = "panel91";
            this.panel91.Size = new System.Drawing.Size(200, 1);
            this.panel91.TabIndex = 35;
            // 
            // panel92
            // 
            this.panel92.BackColor = System.Drawing.Color.White;
            this.panel92.Location = new System.Drawing.Point(487, 41);
            this.panel92.Name = "panel92";
            this.panel92.Size = new System.Drawing.Size(200, 1);
            this.panel92.TabIndex = 34;
            // 
            // panel93
            // 
            this.panel93.BackColor = System.Drawing.Color.White;
            this.panel93.Location = new System.Drawing.Point(256, 42);
            this.panel93.Name = "panel93";
            this.panel93.Size = new System.Drawing.Size(200, 1);
            this.panel93.TabIndex = 33;
            // 
            // panel94
            // 
            this.panel94.BackColor = System.Drawing.Color.White;
            this.panel94.Location = new System.Drawing.Point(24, 42);
            this.panel94.Name = "panel94";
            this.panel94.Size = new System.Drawing.Size(200, 1);
            this.panel94.TabIndex = 32;
            // 
            // btnCadastrar7
            // 
            this.btnCadastrar7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnCadastrar7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastrar7.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrar7.ForeColor = System.Drawing.Color.Aqua;
            this.btnCadastrar7.Location = new System.Drawing.Point(50, 5);
            this.btnCadastrar7.Name = "btnCadastrar7";
            this.btnCadastrar7.Size = new System.Drawing.Size(150, 30);
            this.btnCadastrar7.TabIndex = 31;
            this.btnCadastrar7.Text = "Cadastrar";
            this.btnCadastrar7.UseVisualStyleBackColor = false;
            this.btnCadastrar7.Click += new System.EventHandler(this.btnCadastrar7_Click);
            // 
            // btnRemover7
            // 
            this.btnRemover7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnRemover7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemover7.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemover7.ForeColor = System.Drawing.Color.Aqua;
            this.btnRemover7.Location = new System.Drawing.Point(739, 5);
            this.btnRemover7.Name = "btnRemover7";
            this.btnRemover7.Size = new System.Drawing.Size(150, 30);
            this.btnRemover7.TabIndex = 34;
            this.btnRemover7.Text = "Remover";
            this.btnRemover7.UseVisualStyleBackColor = false;
            this.btnRemover7.Click += new System.EventHandler(this.btnRemover7_Click);
            // 
            // btnConsultar7
            // 
            this.btnConsultar7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnConsultar7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultar7.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultar7.ForeColor = System.Drawing.Color.Aqua;
            this.btnConsultar7.Location = new System.Drawing.Point(282, 5);
            this.btnConsultar7.Name = "btnConsultar7";
            this.btnConsultar7.Size = new System.Drawing.Size(150, 30);
            this.btnConsultar7.TabIndex = 32;
            this.btnConsultar7.Text = "Consultar";
            this.btnConsultar7.UseVisualStyleBackColor = false;
            this.btnConsultar7.Click += new System.EventHandler(this.btnConsultar7_Click);
            // 
            // btnAlterar7
            // 
            this.btnAlterar7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnAlterar7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar7.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlterar7.ForeColor = System.Drawing.Color.Aqua;
            this.btnAlterar7.Location = new System.Drawing.Point(510, 5);
            this.btnAlterar7.Name = "btnAlterar7";
            this.btnAlterar7.Size = new System.Drawing.Size(150, 30);
            this.btnAlterar7.TabIndex = 33;
            this.btnAlterar7.Text = "Alterar";
            this.btnAlterar7.UseVisualStyleBackColor = false;
            this.btnAlterar7.Click += new System.EventHandler(this.btnAlterar7_Click);
            // 
            // pnlFunc
            // 
            this.pnlFunc.BackColor = System.Drawing.Color.Transparent;
            this.pnlFunc.Controls.Add(this.panel87);
            this.pnlFunc.Controls.Add(this.panel88);
            this.pnlFunc.Controls.Add(this.panel89);
            this.pnlFunc.Controls.Add(this.panel90);
            this.pnlFunc.Controls.Add(this.btnCadastrar6);
            this.pnlFunc.Controls.Add(this.btnRemover6);
            this.pnlFunc.Controls.Add(this.btnConsultar6);
            this.pnlFunc.Controls.Add(this.btnAlterar6);
            this.pnlFunc.Location = new System.Drawing.Point(3, 357);
            this.pnlFunc.Name = "pnlFunc";
            this.pnlFunc.Size = new System.Drawing.Size(943, 44);
            this.pnlFunc.TabIndex = 15;
            this.pnlFunc.Visible = false;
            // 
            // panel87
            // 
            this.panel87.BackColor = System.Drawing.Color.White;
            this.panel87.Location = new System.Drawing.Point(709, 41);
            this.panel87.Name = "panel87";
            this.panel87.Size = new System.Drawing.Size(200, 1);
            this.panel87.TabIndex = 31;
            // 
            // panel88
            // 
            this.panel88.BackColor = System.Drawing.Color.White;
            this.panel88.Location = new System.Drawing.Point(485, 41);
            this.panel88.Name = "panel88";
            this.panel88.Size = new System.Drawing.Size(200, 1);
            this.panel88.TabIndex = 30;
            // 
            // panel89
            // 
            this.panel89.BackColor = System.Drawing.Color.White;
            this.panel89.Location = new System.Drawing.Point(254, 42);
            this.panel89.Name = "panel89";
            this.panel89.Size = new System.Drawing.Size(200, 1);
            this.panel89.TabIndex = 29;
            // 
            // panel90
            // 
            this.panel90.BackColor = System.Drawing.Color.White;
            this.panel90.Location = new System.Drawing.Point(22, 42);
            this.panel90.Name = "panel90";
            this.panel90.Size = new System.Drawing.Size(200, 1);
            this.panel90.TabIndex = 28;
            // 
            // btnCadastrar6
            // 
            this.btnCadastrar6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnCadastrar6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastrar6.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrar6.ForeColor = System.Drawing.Color.Aqua;
            this.btnCadastrar6.Location = new System.Drawing.Point(48, 4);
            this.btnCadastrar6.Name = "btnCadastrar6";
            this.btnCadastrar6.Size = new System.Drawing.Size(150, 30);
            this.btnCadastrar6.TabIndex = 26;
            this.btnCadastrar6.Text = "Cadastrar";
            this.btnCadastrar6.UseVisualStyleBackColor = false;
            this.btnCadastrar6.Click += new System.EventHandler(this.btnCadastrar6_Click);
            // 
            // btnRemover6
            // 
            this.btnRemover6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnRemover6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemover6.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemover6.ForeColor = System.Drawing.Color.Aqua;
            this.btnRemover6.Location = new System.Drawing.Point(737, 3);
            this.btnRemover6.Name = "btnRemover6";
            this.btnRemover6.Size = new System.Drawing.Size(150, 30);
            this.btnRemover6.TabIndex = 29;
            this.btnRemover6.Text = "Remover";
            this.btnRemover6.UseVisualStyleBackColor = false;
            this.btnRemover6.Click += new System.EventHandler(this.btnRemover6_Click);
            // 
            // btnConsultar6
            // 
            this.btnConsultar6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnConsultar6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultar6.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultar6.ForeColor = System.Drawing.Color.Aqua;
            this.btnConsultar6.Location = new System.Drawing.Point(280, 4);
            this.btnConsultar6.Name = "btnConsultar6";
            this.btnConsultar6.Size = new System.Drawing.Size(150, 30);
            this.btnConsultar6.TabIndex = 27;
            this.btnConsultar6.Text = "Consultar";
            this.btnConsultar6.UseVisualStyleBackColor = false;
            this.btnConsultar6.Click += new System.EventHandler(this.btnConsultar6_Click);
            // 
            // btnAlterar6
            // 
            this.btnAlterar6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnAlterar6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar6.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlterar6.ForeColor = System.Drawing.Color.Aqua;
            this.btnAlterar6.Location = new System.Drawing.Point(508, 4);
            this.btnAlterar6.Name = "btnAlterar6";
            this.btnAlterar6.Size = new System.Drawing.Size(150, 30);
            this.btnAlterar6.TabIndex = 28;
            this.btnAlterar6.Text = "Alterar";
            this.btnAlterar6.UseVisualStyleBackColor = false;
            this.btnAlterar6.Click += new System.EventHandler(this.btnAlterar6_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(12, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(27, 25);
            this.pictureBox1.TabIndex = 31;
            this.pictureBox1.TabStop = false;
            // 
            // lblRelogio1
            // 
            this.lblRelogio1.AutoSize = true;
            this.lblRelogio1.BackColor = System.Drawing.Color.Transparent;
            this.lblRelogio1.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRelogio1.ForeColor = System.Drawing.Color.White;
            this.lblRelogio1.Location = new System.Drawing.Point(47, 9);
            this.lblRelogio1.Name = "lblRelogio1";
            this.lblRelogio1.Size = new System.Drawing.Size(88, 25);
            this.lblRelogio1.TabIndex = 2;
            this.lblRelogio1.Text = "00:00:00";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel1.Controls.Add(this.panel68);
            this.panel1.Controls.Add(this.panel67);
            this.panel1.Controls.Add(this.panel65);
            this.panel1.Controls.Add(this.panel64);
            this.panel1.Controls.Add(this.panel63);
            this.panel1.Controls.Add(this.panel62);
            this.panel1.Controls.Add(this.panel61);
            this.panel1.Controls.Add(this.panel60);
            this.panel1.Controls.Add(this.panel59);
            this.panel1.Controls.Add(this.panel55);
            this.panel1.Controls.Add(this.panel56);
            this.panel1.Controls.Add(this.panel53);
            this.panel1.Controls.Add(this.panel54);
            this.panel1.Controls.Add(this.panel51);
            this.panel1.Controls.Add(this.panel52);
            this.panel1.Controls.Add(this.panel50);
            this.panel1.Controls.Add(this.panel47);
            this.panel1.Controls.Add(this.panel46);
            this.panel1.Controls.Add(this.panel45);
            this.panel1.Controls.Add(this.panel44);
            this.panel1.Controls.Add(this.panel43);
            this.panel1.Controls.Add(this.panel42);
            this.panel1.Controls.Add(this.panel41);
            this.panel1.Controls.Add(this.panel38);
            this.panel1.Controls.Add(this.panel39);
            this.panel1.Controls.Add(this.panel37);
            this.panel1.Controls.Add(this.panel36);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.pictureBox12);
            this.panel1.Controls.Add(this.pictureBox11);
            this.panel1.Controls.Add(this.pictureBox10);
            this.panel1.Controls.Add(this.pictureBox9);
            this.panel1.Controls.Add(this.pictureBox8);
            this.panel1.Controls.Add(this.pictureBox7);
            this.panel1.Controls.Add(this.pictureBox4);
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.pictureBox6);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.pictureBox5);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel28);
            this.panel1.Controls.Add(this.panel27);
            this.panel1.Controls.Add(this.panel26);
            this.panel1.Controls.Add(this.panel22);
            this.panel1.Controls.Add(this.panel21);
            this.panel1.Controls.Add(this.panel19);
            this.panel1.Controls.Add(this.panel17);
            this.panel1.Controls.Add(this.panel16);
            this.panel1.Controls.Add(this.panel15);
            this.panel1.Controls.Add(this.panel14);
            this.panel1.Controls.Add(this.panel13);
            this.panel1.Controls.Add(this.panel12);
            this.panel1.Controls.Add(this.panel11);
            this.panel1.Controls.Add(this.panel10);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.panel9);
            this.panel1.Controls.Add(this.btnProv);
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.btnProdt);
            this.panel1.Controls.Add(this.btnPacotes);
            this.panel1.Controls.Add(this.btnJornada);
            this.panel1.Controls.Add(this.btnHolerite);
            this.panel1.Controls.Add(this.btnFunc);
            this.panel1.Controls.Add(this.btnForn);
            this.panel1.Controls.Add(this.btnCliente);
            this.panel1.Controls.Add(this.btnCarro);
            this.panel1.Controls.Add(this.btnAula);
            this.panel1.Controls.Add(this.btnAbast);
            this.panel1.Location = new System.Drawing.Point(0, 45);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(335, 688);
            this.panel1.TabIndex = 9;
            // 
            // panel68
            // 
            this.panel68.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.panel68.Location = new System.Drawing.Point(13, 300);
            this.panel68.Name = "panel68";
            this.panel68.Size = new System.Drawing.Size(321, 2);
            this.panel68.TabIndex = 23;
            // 
            // panel67
            // 
            this.panel67.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.panel67.Location = new System.Drawing.Point(0, 301);
            this.panel67.Name = "panel67";
            this.panel67.Size = new System.Drawing.Size(336, 2);
            this.panel67.TabIndex = 23;
            // 
            // panel65
            // 
            this.panel65.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.panel65.Location = new System.Drawing.Point(0, 643);
            this.panel65.Name = "panel65";
            this.panel65.Size = new System.Drawing.Size(336, 2);
            this.panel65.TabIndex = 52;
            // 
            // panel64
            // 
            this.panel64.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.panel64.Location = new System.Drawing.Point(0, 595);
            this.panel64.Name = "panel64";
            this.panel64.Size = new System.Drawing.Size(336, 2);
            this.panel64.TabIndex = 51;
            // 
            // panel63
            // 
            this.panel63.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.panel63.Location = new System.Drawing.Point(-1, 546);
            this.panel63.Name = "panel63";
            this.panel63.Size = new System.Drawing.Size(336, 2);
            this.panel63.TabIndex = 50;
            // 
            // panel62
            // 
            this.panel62.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.panel62.Location = new System.Drawing.Point(-1, 497);
            this.panel62.Name = "panel62";
            this.panel62.Size = new System.Drawing.Size(336, 2);
            this.panel62.TabIndex = 49;
            // 
            // panel61
            // 
            this.panel61.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.panel61.Location = new System.Drawing.Point(0, 448);
            this.panel61.Name = "panel61";
            this.panel61.Size = new System.Drawing.Size(336, 2);
            this.panel61.TabIndex = 48;
            // 
            // panel60
            // 
            this.panel60.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.panel60.Location = new System.Drawing.Point(0, 399);
            this.panel60.Name = "panel60";
            this.panel60.Size = new System.Drawing.Size(336, 2);
            this.panel60.TabIndex = 47;
            // 
            // panel59
            // 
            this.panel59.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.panel59.Location = new System.Drawing.Point(0, 350);
            this.panel59.Name = "panel59";
            this.panel59.Size = new System.Drawing.Size(336, 2);
            this.panel59.TabIndex = 46;
            // 
            // panel55
            // 
            this.panel55.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.panel55.Controls.Add(this.panel66);
            this.panel55.Location = new System.Drawing.Point(0, 302);
            this.panel55.Name = "panel55";
            this.panel55.Size = new System.Drawing.Size(336, 1);
            this.panel55.TabIndex = 23;
            // 
            // panel66
            // 
            this.panel66.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.panel66.Location = new System.Drawing.Point(2, -1);
            this.panel66.Name = "panel66";
            this.panel66.Size = new System.Drawing.Size(336, 2);
            this.panel66.TabIndex = 23;
            // 
            // panel56
            // 
            this.panel56.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.panel56.Location = new System.Drawing.Point(0, 302);
            this.panel56.Name = "panel56";
            this.panel56.Size = new System.Drawing.Size(336, 1);
            this.panel56.TabIndex = 24;
            // 
            // panel53
            // 
            this.panel53.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.panel53.Location = new System.Drawing.Point(0, 252);
            this.panel53.Name = "panel53";
            this.panel53.Size = new System.Drawing.Size(336, 1);
            this.panel53.TabIndex = 23;
            // 
            // panel54
            // 
            this.panel54.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.panel54.Location = new System.Drawing.Point(1, 252);
            this.panel54.Name = "panel54";
            this.panel54.Size = new System.Drawing.Size(336, 1);
            this.panel54.TabIndex = 24;
            // 
            // panel51
            // 
            this.panel51.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.panel51.Location = new System.Drawing.Point(2, 203);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(336, 1);
            this.panel51.TabIndex = 23;
            // 
            // panel52
            // 
            this.panel52.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.panel52.Location = new System.Drawing.Point(2, 202);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(336, 1);
            this.panel52.TabIndex = 24;
            // 
            // panel50
            // 
            this.panel50.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.panel50.Location = new System.Drawing.Point(1, 154);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(336, 1);
            this.panel50.TabIndex = 23;
            // 
            // panel47
            // 
            this.panel47.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel47.Location = new System.Drawing.Point(135, 253);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(200, 1);
            this.panel47.TabIndex = 23;
            // 
            // panel46
            // 
            this.panel46.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel46.Location = new System.Drawing.Point(-1, 253);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(200, 1);
            this.panel46.TabIndex = 23;
            // 
            // panel45
            // 
            this.panel45.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel45.Location = new System.Drawing.Point(135, 204);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(200, 1);
            this.panel45.TabIndex = 23;
            // 
            // panel44
            // 
            this.panel44.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel44.Location = new System.Drawing.Point(0, 204);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(200, 1);
            this.panel44.TabIndex = 23;
            // 
            // panel43
            // 
            this.panel43.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel43.Location = new System.Drawing.Point(0, 155);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(200, 1);
            this.panel43.TabIndex = 23;
            // 
            // panel42
            // 
            this.panel42.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel42.Location = new System.Drawing.Point(135, 155);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(200, 1);
            this.panel42.TabIndex = 23;
            // 
            // panel41
            // 
            this.panel41.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel41.Location = new System.Drawing.Point(135, 106);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(200, 1);
            this.panel41.TabIndex = 23;
            // 
            // panel38
            // 
            this.panel38.BackColor = System.Drawing.Color.Aqua;
            this.panel38.Location = new System.Drawing.Point(3, 107);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(13, 50);
            this.panel38.TabIndex = 45;
            this.panel38.Visible = false;
            // 
            // panel39
            // 
            this.panel39.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel39.Location = new System.Drawing.Point(0, 106);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(200, 1);
            this.panel39.TabIndex = 21;
            // 
            // panel37
            // 
            this.panel37.BackColor = System.Drawing.Color.White;
            this.panel37.Location = new System.Drawing.Point(1, 0);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(334, 1);
            this.panel37.TabIndex = 24;
            // 
            // panel36
            // 
            this.panel36.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel36.Controls.Add(this.panel25);
            this.panel36.Location = new System.Drawing.Point(0, 499);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(336, 1);
            this.panel36.TabIndex = 23;
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.White;
            this.panel25.Location = new System.Drawing.Point(41, 0);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(250, 1);
            this.panel25.TabIndex = 22;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel4.Controls.Add(this.panel35);
            this.panel4.Controls.Add(this.panel40);
            this.panel4.Location = new System.Drawing.Point(0, 107);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(336, 1);
            this.panel4.TabIndex = 23;
            // 
            // panel35
            // 
            this.panel35.BackColor = System.Drawing.Color.White;
            this.panel35.Location = new System.Drawing.Point(42, 0);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(250, 1);
            this.panel35.TabIndex = 23;
            // 
            // panel40
            // 
            this.panel40.BackColor = System.Drawing.Color.Red;
            this.panel40.Location = new System.Drawing.Point(0, 1);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(336, 1);
            this.panel40.TabIndex = 22;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox12.BackgroundImage")));
            this.pictureBox12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox12.Location = new System.Drawing.Point(50, 608);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(30, 30);
            this.pictureBox12.TabIndex = 44;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox11.BackgroundImage")));
            this.pictureBox11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox11.Location = new System.Drawing.Point(50, 561);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(29, 27);
            this.pictureBox11.TabIndex = 43;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox10.BackgroundImage")));
            this.pictureBox10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox10.Location = new System.Drawing.Point(50, 513);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(29, 27);
            this.pictureBox10.TabIndex = 42;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox9.BackgroundImage")));
            this.pictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox9.Location = new System.Drawing.Point(50, 465);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(29, 27);
            this.pictureBox9.TabIndex = 41;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox8.BackgroundImage")));
            this.pictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox8.Location = new System.Drawing.Point(50, 415);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(29, 27);
            this.pictureBox8.TabIndex = 40;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox7.BackgroundImage")));
            this.pictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox7.Location = new System.Drawing.Point(50, 365);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(29, 27);
            this.pictureBox7.TabIndex = 39;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox4.BackgroundImage")));
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(50, 317);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(29, 27);
            this.pictureBox4.TabIndex = 32;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(50, 268);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(29, 27);
            this.pictureBox3.TabIndex = 31;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox6.BackgroundImage")));
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.Location = new System.Drawing.Point(50, 217);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(29, 27);
            this.pictureBox6.TabIndex = 36;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(50, 168);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(29, 27);
            this.pictureBox2.TabIndex = 38;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox5.BackgroundImage")));
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox5.Location = new System.Drawing.Point(50, 121);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(29, 27);
            this.pictureBox5.TabIndex = 37;
            this.pictureBox5.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel6.Location = new System.Drawing.Point(0, 646);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(334, 1);
            this.panel6.TabIndex = 23;
            // 
            // panel28
            // 
            this.panel28.BackColor = System.Drawing.Color.White;
            this.panel28.Location = new System.Drawing.Point(42, 645);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(250, 1);
            this.panel28.TabIndex = 22;
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.White;
            this.panel27.Location = new System.Drawing.Point(42, 597);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(250, 1);
            this.panel27.TabIndex = 22;
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.White;
            this.panel26.Location = new System.Drawing.Point(42, 548);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(250, 1);
            this.panel26.TabIndex = 22;
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.White;
            this.panel22.Location = new System.Drawing.Point(42, 352);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(250, 1);
            this.panel22.TabIndex = 22;
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.White;
            this.panel21.Location = new System.Drawing.Point(42, 303);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(250, 1);
            this.panel21.TabIndex = 22;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.White;
            this.panel19.Location = new System.Drawing.Point(42, 205);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(250, 1);
            this.panel19.TabIndex = 22;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel17.Location = new System.Drawing.Point(0, 597);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(334, 1);
            this.panel17.TabIndex = 25;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel16.Location = new System.Drawing.Point(0, 548);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(334, 1);
            this.panel16.TabIndex = 25;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel15.Location = new System.Drawing.Point(-3, 497);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(334, 1);
            this.panel15.TabIndex = 25;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel14.Controls.Add(this.panel24);
            this.panel14.Location = new System.Drawing.Point(0, 450);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(334, 1);
            this.panel14.TabIndex = 25;
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.White;
            this.panel24.Location = new System.Drawing.Point(42, 0);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(250, 1);
            this.panel24.TabIndex = 22;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel13.Controls.Add(this.panel23);
            this.panel13.Location = new System.Drawing.Point(0, 401);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(334, 1);
            this.panel13.TabIndex = 25;
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.White;
            this.panel23.Location = new System.Drawing.Point(42, 0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(250, 1);
            this.panel23.TabIndex = 22;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel12.Location = new System.Drawing.Point(0, 352);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(334, 1);
            this.panel12.TabIndex = 25;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel11.Controls.Add(this.panel57);
            this.panel11.Controls.Add(this.panel58);
            this.panel11.Location = new System.Drawing.Point(0, 303);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(334, 1);
            this.panel11.TabIndex = 25;
            // 
            // panel57
            // 
            this.panel57.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.panel57.Location = new System.Drawing.Point(0, 0);
            this.panel57.Name = "panel57";
            this.panel57.Size = new System.Drawing.Size(336, 1);
            this.panel57.TabIndex = 23;
            // 
            // panel58
            // 
            this.panel58.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.panel58.Location = new System.Drawing.Point(0, 0);
            this.panel58.Name = "panel58";
            this.panel58.Size = new System.Drawing.Size(336, 1);
            this.panel58.TabIndex = 24;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel10.Controls.Add(this.panel20);
            this.panel10.Location = new System.Drawing.Point(0, 254);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(334, 1);
            this.panel10.TabIndex = 24;
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.White;
            this.panel20.Location = new System.Drawing.Point(42, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(250, 1);
            this.panel20.TabIndex = 22;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel7.Controls.Add(this.panel29);
            this.panel7.Controls.Add(this.label1);
            this.panel7.Location = new System.Drawing.Point(3, 2);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(334, 82);
            this.panel7.TabIndex = 0;
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.Color.White;
            this.panel29.Location = new System.Drawing.Point(1, 68);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(334, 1);
            this.panel29.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(134, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Menu ";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel9.Location = new System.Drawing.Point(0, 205);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(334, 1);
            this.panel9.TabIndex = 23;
            // 
            // btnProv
            // 
            this.btnProv.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnProv.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProv.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProv.ForeColor = System.Drawing.Color.White;
            this.btnProv.Location = new System.Drawing.Point(-1, 595);
            this.btnProv.Name = "btnProv";
            this.btnProv.Size = new System.Drawing.Size(336, 50);
            this.btnProv.TabIndex = 50;
            this.btnProv.Text = "Provas";
            this.btnProv.UseVisualStyleBackColor = false;
            this.btnProv.Click += new System.EventHandler(this.btnProv_Click);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel8.Controls.Add(this.panel18);
            this.panel8.Location = new System.Drawing.Point(0, 156);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(334, 1);
            this.panel8.TabIndex = 22;
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.White;
            this.panel18.Location = new System.Drawing.Point(42, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(250, 1);
            this.panel18.TabIndex = 22;
            // 
            // btnProdt
            // 
            this.btnProdt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnProdt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProdt.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProdt.ForeColor = System.Drawing.Color.White;
            this.btnProdt.Location = new System.Drawing.Point(-1, 547);
            this.btnProdt.Name = "btnProdt";
            this.btnProdt.Size = new System.Drawing.Size(336, 50);
            this.btnProdt.TabIndex = 45;
            this.btnProdt.Text = "Produtos";
            this.btnProdt.UseVisualStyleBackColor = false;
            this.btnProdt.Click += new System.EventHandler(this.btnProdt_Click);
            // 
            // btnPacotes
            // 
            this.btnPacotes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnPacotes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPacotes.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPacotes.ForeColor = System.Drawing.Color.White;
            this.btnPacotes.Location = new System.Drawing.Point(-1, 498);
            this.btnPacotes.Name = "btnPacotes";
            this.btnPacotes.Size = new System.Drawing.Size(336, 50);
            this.btnPacotes.TabIndex = 40;
            this.btnPacotes.Text = "Pacotes";
            this.btnPacotes.UseVisualStyleBackColor = false;
            this.btnPacotes.Click += new System.EventHandler(this.btnPacotes_Click);
            // 
            // btnJornada
            // 
            this.btnJornada.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnJornada.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnJornada.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnJornada.ForeColor = System.Drawing.Color.White;
            this.btnJornada.Location = new System.Drawing.Point(-1, 449);
            this.btnJornada.Name = "btnJornada";
            this.btnJornada.Size = new System.Drawing.Size(336, 50);
            this.btnJornada.TabIndex = 35;
            this.btnJornada.Text = "Jornada de Trabalho";
            this.btnJornada.UseVisualStyleBackColor = false;
            this.btnJornada.Click += new System.EventHandler(this.btnJornada_Click);
            // 
            // btnHolerite
            // 
            this.btnHolerite.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnHolerite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHolerite.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHolerite.ForeColor = System.Drawing.Color.White;
            this.btnHolerite.Location = new System.Drawing.Point(-1, 400);
            this.btnHolerite.Name = "btnHolerite";
            this.btnHolerite.Size = new System.Drawing.Size(336, 50);
            this.btnHolerite.TabIndex = 30;
            this.btnHolerite.Text = "Holerites";
            this.btnHolerite.UseVisualStyleBackColor = false;
            this.btnHolerite.Click += new System.EventHandler(this.btnHolerite_Click);
            // 
            // btnFunc
            // 
            this.btnFunc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnFunc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFunc.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFunc.ForeColor = System.Drawing.Color.White;
            this.btnFunc.Location = new System.Drawing.Point(-1, 351);
            this.btnFunc.Name = "btnFunc";
            this.btnFunc.Size = new System.Drawing.Size(336, 50);
            this.btnFunc.TabIndex = 25;
            this.btnFunc.Text = "Funcionários";
            this.btnFunc.UseVisualStyleBackColor = false;
            this.btnFunc.Click += new System.EventHandler(this.btnFunc_Click);
            // 
            // btnForn
            // 
            this.btnForn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnForn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnForn.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnForn.ForeColor = System.Drawing.Color.White;
            this.btnForn.Location = new System.Drawing.Point(-1, 302);
            this.btnForn.Name = "btnForn";
            this.btnForn.Size = new System.Drawing.Size(336, 50);
            this.btnForn.TabIndex = 20;
            this.btnForn.Text = "Fornecedores";
            this.btnForn.UseVisualStyleBackColor = false;
            this.btnForn.Click += new System.EventHandler(this.btnForn_Click);
            // 
            // btnCliente
            // 
            this.btnCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCliente.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCliente.ForeColor = System.Drawing.Color.White;
            this.btnCliente.Location = new System.Drawing.Point(-1, 252);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(336, 50);
            this.btnCliente.TabIndex = 15;
            this.btnCliente.Text = "Clientes";
            this.btnCliente.UseVisualStyleBackColor = false;
            this.btnCliente.Click += new System.EventHandler(this.btnCliente_Click);
            // 
            // btnCarro
            // 
            this.btnCarro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnCarro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCarro.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCarro.ForeColor = System.Drawing.Color.White;
            this.btnCarro.Location = new System.Drawing.Point(-1, 204);
            this.btnCarro.Name = "btnCarro";
            this.btnCarro.Size = new System.Drawing.Size(336, 50);
            this.btnCarro.TabIndex = 10;
            this.btnCarro.Text = "Carros";
            this.btnCarro.UseVisualStyleBackColor = false;
            this.btnCarro.Click += new System.EventHandler(this.btnCarro_Click);
            // 
            // btnAula
            // 
            this.btnAula.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnAula.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAula.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAula.ForeColor = System.Drawing.Color.White;
            this.btnAula.Location = new System.Drawing.Point(-1, 155);
            this.btnAula.Name = "btnAula";
            this.btnAula.Size = new System.Drawing.Size(336, 50);
            this.btnAula.TabIndex = 5;
            this.btnAula.Text = "Aulas";
            this.btnAula.UseVisualStyleBackColor = false;
            this.btnAula.Click += new System.EventHandler(this.btnAula_Click);
            // 
            // btnAbast
            // 
            this.btnAbast.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.btnAbast.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAbast.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAbast.ForeColor = System.Drawing.Color.White;
            this.btnAbast.Location = new System.Drawing.Point(-3, 106);
            this.btnAbast.Name = "btnAbast";
            this.btnAbast.Size = new System.Drawing.Size(339, 50);
            this.btnAbast.TabIndex = 0;
            this.btnAbast.Text = "Abastecimentos";
            this.btnAbast.UseVisualStyleBackColor = false;
            this.btnAbast.Click += new System.EventHandler(this.btnAbast_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel3.Controls.Add(this.lblFechar);
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.lblRelogio1);
            this.panel3.Location = new System.Drawing.Point(0, -1);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1280, 47);
            this.panel3.TabIndex = 8;
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.BackColor = System.Drawing.Color.Transparent;
            this.lblFechar.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechar.ForeColor = System.Drawing.Color.Transparent;
            this.lblFechar.Location = new System.Drawing.Point(1244, 10);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(24, 25);
            this.lblFechar.TabIndex = 10;
            this.lblFechar.Text = "X";
            this.lblFechar.Click += new System.EventHandler(this.lblFechar_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.Location = new System.Drawing.Point(0, 46);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1280, 1);
            this.panel5.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(612, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(134, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Future Drivers";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 720);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu";
            this.panel2.ResumeLayout(false);
            this.pnlProva.ResumeLayout(false);
            this.pnlCarro.ResumeLayout(false);
            this.pnlAula.ResumeLayout(false);
            this.pnlProd.ResumeLayout(false);
            this.pnlAbast.ResumeLayout(false);
            this.pnlPact.ResumeLayout(false);
            this.pnlJornada.ResumeLayout(false);
            this.pnlCliente.ResumeLayout(false);
            this.panel30.ResumeLayout(false);
            this.panel32.ResumeLayout(false);
            this.pnlForn.ResumeLayout(false);
            this.pnlHoler.ResumeLayout(false);
            this.pnlFunc.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel55.ResumeLayout(false);
            this.panel36.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblRelogio1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button btnProv;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Button btnProdt;
        private System.Windows.Forms.Button btnPacotes;
        private System.Windows.Forms.Button btnJornada;
        private System.Windows.Forms.Button btnHolerite;
        private System.Windows.Forms.Button btnFunc;
        private System.Windows.Forms.Button btnForn;
        private System.Windows.Forms.Button btnCliente;
        private System.Windows.Forms.Button btnCarro;
        private System.Windows.Forms.Button btnAula;
        private System.Windows.Forms.Button btnAbast;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblFechar;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel pnlAbast;
        private System.Windows.Forms.Button btnCadastrar1;
        private System.Windows.Forms.Button btnRemover1;
        private System.Windows.Forms.Button btnAlterar1;
        private System.Windows.Forms.Button btnConsultar1;
        private System.Windows.Forms.Panel pnlProva;
        private System.Windows.Forms.Button btnCadastrar11;
        private System.Windows.Forms.Button btnRemover11;
        private System.Windows.Forms.Button btnConsultar11;
        private System.Windows.Forms.Button btnAlterar11;
        private System.Windows.Forms.Panel pnlProd;
        private System.Windows.Forms.Button btnCadastrar10;
        private System.Windows.Forms.Button btnRemover10;
        private System.Windows.Forms.Button btnConsultar10;
        private System.Windows.Forms.Button btnAlterar10;
        private System.Windows.Forms.Panel pnlPact;
        private System.Windows.Forms.Button btnCadastrar9;
        private System.Windows.Forms.Button btnRemover9;
        private System.Windows.Forms.Button btnConsultar9;
        private System.Windows.Forms.Button btnAlterar9;
        private System.Windows.Forms.Panel pnlJornada;
        private System.Windows.Forms.Button btnCadastrar8;
        private System.Windows.Forms.Button btnRemover8;
        private System.Windows.Forms.Button btnConsultar8;
        private System.Windows.Forms.Button btnAlterar8;
        private System.Windows.Forms.Panel pnlCarro;
        private System.Windows.Forms.Button btnCadastrar3;
        private System.Windows.Forms.Button btnRemover3;
        private System.Windows.Forms.Button btnConsultar3;
        private System.Windows.Forms.Button btnAlterar3;
        private System.Windows.Forms.Panel pnlHoler;
        private System.Windows.Forms.Button btnCadastrar7;
        private System.Windows.Forms.Button btnRemover7;
        private System.Windows.Forms.Button btnConsultar7;
        private System.Windows.Forms.Button btnAlterar7;
        private System.Windows.Forms.Panel pnlFunc;
        private System.Windows.Forms.Button btnCadastrar6;
        private System.Windows.Forms.Button btnRemover6;
        private System.Windows.Forms.Button btnConsultar6;
        private System.Windows.Forms.Button btnAlterar6;
        private System.Windows.Forms.Panel pnlForn;
        private System.Windows.Forms.Button btnCadastrar5;
        private System.Windows.Forms.Button btnRemover5;
        private System.Windows.Forms.Button btnConsultar5;
        private System.Windows.Forms.Button btnAlterar5;
        private System.Windows.Forms.Panel pnlCliente;
        private System.Windows.Forms.Button btnCadastrar4;
        private System.Windows.Forms.Button btnRemover4;
        private System.Windows.Forms.Button btnConsultar4;
        private System.Windows.Forms.Button btnAlterar4;
        private System.Windows.Forms.Panel pnlAula;
        private System.Windows.Forms.Button btnCadastrar2;
        private System.Windows.Forms.Button btnRemover2;
        private System.Windows.Forms.Button btnConsultar2;
        private System.Windows.Forms.Button btnAlterar2;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Panel panel47;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.Panel panel45;
        private System.Windows.Forms.Panel panel44;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.Panel panel50;
        private System.Windows.Forms.Panel panel51;
        private System.Windows.Forms.Panel panel52;
        private System.Windows.Forms.Panel panel53;
        private System.Windows.Forms.Panel panel54;
        private System.Windows.Forms.Panel panel55;
        private System.Windows.Forms.Panel panel56;
        private System.Windows.Forms.Panel panel57;
        private System.Windows.Forms.Panel panel58;
        private System.Windows.Forms.Panel panel65;
        private System.Windows.Forms.Panel panel64;
        private System.Windows.Forms.Panel panel63;
        private System.Windows.Forms.Panel panel62;
        private System.Windows.Forms.Panel panel61;
        private System.Windows.Forms.Panel panel60;
        private System.Windows.Forms.Panel panel59;
        private System.Windows.Forms.Panel panel66;
        private System.Windows.Forms.Panel panel67;
        private System.Windows.Forms.Panel panel68;
        private System.Windows.Forms.Panel panel48;
        private System.Windows.Forms.Panel panel49;
        private System.Windows.Forms.Panel panel76;
        private System.Windows.Forms.Panel panel77;
        private System.Windows.Forms.Panel panel78;
        private System.Windows.Forms.Panel panel72;
        private System.Windows.Forms.Panel panel73;
        private System.Windows.Forms.Panel panel74;
        private System.Windows.Forms.Panel panel75;
        private System.Windows.Forms.Panel panel71;
        private System.Windows.Forms.Panel panel70;
        private System.Windows.Forms.Panel panel69;
        private System.Windows.Forms.Panel panel79;
        private System.Windows.Forms.Panel panel80;
        private System.Windows.Forms.Panel panel81;
        private System.Windows.Forms.Panel panel82;
        private System.Windows.Forms.Panel panel83;
        private System.Windows.Forms.Panel panel84;
        private System.Windows.Forms.Panel panel85;
        private System.Windows.Forms.Panel panel86;
        private System.Windows.Forms.Panel panel103;
        private System.Windows.Forms.Panel panel104;
        private System.Windows.Forms.Panel panel105;
        private System.Windows.Forms.Panel panel106;
        private System.Windows.Forms.Panel panel99;
        private System.Windows.Forms.Panel panel100;
        private System.Windows.Forms.Panel panel101;
        private System.Windows.Forms.Panel panel102;
        private System.Windows.Forms.Panel panel95;
        private System.Windows.Forms.Panel panel96;
        private System.Windows.Forms.Panel panel97;
        private System.Windows.Forms.Panel panel98;
        private System.Windows.Forms.Panel panel91;
        private System.Windows.Forms.Panel panel92;
        private System.Windows.Forms.Panel panel93;
        private System.Windows.Forms.Panel panel94;
        private System.Windows.Forms.Panel panel87;
        private System.Windows.Forms.Panel panel88;
        private System.Windows.Forms.Panel panel89;
        private System.Windows.Forms.Panel panel90;
        private System.Windows.Forms.Panel panel107;
        private System.Windows.Forms.Panel panel108;
        private System.Windows.Forms.Panel panel109;
        private System.Windows.Forms.Panel panel110;
    }
}