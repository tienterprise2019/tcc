﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Funcionarios
{
    public partial class frmConsultar : Form
    {
        public frmConsultar()
        {
            InitializeComponent();
            this.CarregarFuncionarios();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Funcionarios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Funcionarios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtFuncionario_TextChanged(object sender, EventArgs e)
        {
            this.CarregarPorNomeFuncionarios();
        }

        public void CarregarFuncionarios()
        {
            try
            {
                Business.FuncionariosBusiness BSFuncionario = new Business.FuncionariosBusiness();
                List<Model.tb_funcionario> ModelFuncionarios = BSFuncionario.ConsultarFuncionarios();

                dgvFuncionarios.AutoGenerateColumns = false;
                dgvFuncionarios.DataSource = ModelFuncionarios;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Funcionarios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Funcionarios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarPorNomeFuncionarios()
        {
            try
            {
                string funcionario = txtFuncionario.Text;

                Business.FuncionariosBusiness BSFuncionario = new Business.FuncionariosBusiness();
                List<Model.tb_funcionario> ModelFuncionarios = BSFuncionario.ConsultarPorNomeFuncionarios(funcionario);

                dgvFuncionarios.AutoGenerateColumns = false;
                dgvFuncionarios.DataSource = ModelFuncionarios;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Funcionarios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Funcionarios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Funcionarios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Funcionarios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
