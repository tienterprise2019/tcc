﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Funcionarios
{
    public partial class frmAlterar : Form
    {
        public frmAlterar()
        {
            InitializeComponent();
            this.CarregarFuncionarios();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Funcionarios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Funcionarios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cboFuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.CarregarFuncionario();
        }

        private void txtCep_Leave(object sender, EventArgs e)
        {
            this.Buscar(txtCep.Text);
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            this.Alterar();
        }

        public void CarregarFuncionarios()
        {
            try
            {
                Business.FuncionariosBusiness BSFuncionarios = new Business.FuncionariosBusiness();
                List<Model.tb_funcionario> ModelFuncionarios = BSFuncionarios.ConsultarFuncionarios();

                cboFuncionario.DisplayMember = nameof(Model.tb_funcionario.nm_funcionario);
                cboFuncionario.DataSource = ModelFuncionarios;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Funcionarios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Funcionarios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarFuncionario()
        {
            try
            {
                Model.tb_funcionario ModelFuncionario = cboFuncionario.SelectedItem as Model.tb_funcionario;
                txtCPF.Text = ModelFuncionario.ds_cpf;
                dtpNasc.Value = ModelFuncionario.dt_nascimento;
                txtTelefone.Text = ModelFuncionario.ds_telefone;
                txtCelular.Text = ModelFuncionario.ds_celular;
                txtEmail.Text = ModelFuncionario.ds_email;
                txtCep.Text = ModelFuncionario.ds_cep;
                txtRua.Text = ModelFuncionario.nm_rua;
                txtNumero.Text = Convert.ToString(ModelFuncionario.nr_numero);
                txtBairro.Text = ModelFuncionario.nm_bairro;
                txtCidade.Text = ModelFuncionario.nm_cidade;
                txtCargo.Text = ModelFuncionario.ds_cargo;
                txtHrEntrada.Text = Convert.ToString(ModelFuncionario.hr_entrada);
                txtHrSaida.Text = Convert.ToString(ModelFuncionario.hr_saida);
                dtpContratacao.Value = ModelFuncionario.dt_contratacao;

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Funcionarios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Funcionarios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Alterar()
        {
            try
            {
                Model.tb_funcionario ModelFuncionario = cboFuncionario.SelectedItem as Model.tb_funcionario;
                ModelFuncionario.ds_cpf = txtCPF.Text;
                ModelFuncionario.dt_nascimento = dtpNasc.Value;
                ModelFuncionario.ds_telefone = txtTelefone.Text;
                ModelFuncionario.ds_celular = txtCelular.Text;
                ModelFuncionario.ds_email = txtEmail.Text;
                ModelFuncionario.ds_cep = txtCep.Text;
                ModelFuncionario.nm_rua = txtRua.Text;
                ModelFuncionario.nr_numero = Convert.ToInt32(txtNumero.Text);
                ModelFuncionario.nm_bairro = txtBairro.Text;
                ModelFuncionario.nm_cidade = txtCidade.Text;
                ModelFuncionario.ds_cargo = txtCargo.Text;
                ModelFuncionario.hr_entrada = TimeSpan.Parse(txtHrEntrada.Text);
                ModelFuncionario.hr_saida = TimeSpan.Parse(txtHrSaida.Text);
                ModelFuncionario.dt_contratacao = dtpContratacao.Value;

                Business.FuncionariosBusiness BSFuncionarios = new Business.FuncionariosBusiness();
                BSFuncionarios.AlterarFuncionarios(ModelFuncionario);

                MessageBox.Show("Funcionario alterado com sucesso!", "Future Drivers Auto Escola | Funcionarios", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Funcionarios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Funcionarios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void Buscar(string cep)
        {
            try
            {
                // Cria objeto responsável por conversar com uma API
                System.Net.WebClient rest = new System.Net.WebClient();
                rest.Encoding = Encoding.UTF8;

                // Chama API do correio, concatenando o cep
                string resposta = rest.DownloadString("https://viacep.com.br/ws/" + cep + "/json");

                // Transforma a resposta do correio em DTO
                dynamic r = Newtonsoft.Json.JsonConvert.DeserializeObject(resposta);

                txtRua.Text = r.logradouro;
                txtBairro.Text = r.bairro;
                txtCidade.Text = r.localidade;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Funcionarios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Funcionarios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Funcionarios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Funcionarios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
