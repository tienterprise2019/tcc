﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Holerites
{
    public partial class frmRemover : Form
    {
        public frmRemover()
        {
            InitializeComponent();
            this.CarregarHolerites();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRemover_Click(object sender, EventArgs e)
        {
            this.RemoverHolerites();
        }

        public void CarregarHolerites()
        {
            try
            {
                Business.HoleritesBusiness BSHolerite = new Business.HoleritesBusiness();
                List<Model.tb_holerite> ModelHolerites = BSHolerite.ConsultarHolerites();

                dgvHolerites.AutoGenerateColumns = false;
                dgvHolerites.DataSource = ModelHolerites;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void RemoverHolerites()
        {
            try
            {
                Model.tb_holerite ModelHolerite = dgvHolerites.CurrentRow.DataBoundItem as Model.tb_holerite;

                Business.HoleritesBusiness BSHolerite = new Business.HoleritesBusiness();
                BSHolerite.RemoverHolerites(ModelHolerite.id_holerite);

                MessageBox.Show("Holerite alterado com sucesso!", "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.CarregarHolerites();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
