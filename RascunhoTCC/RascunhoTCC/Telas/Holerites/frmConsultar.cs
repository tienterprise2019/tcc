﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Holerites
{
    public partial class frmConsultar : Form
    {
        public frmConsultar()
        {
            InitializeComponent();
            this.CarregarHolerites();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtFuncionario_TextChanged(object sender, EventArgs e)
        {
            this.CarregarPorNomeHolerites();
        }

        public void CarregarHolerites()
        {
            try
            {
                Business.HoleritesBusiness BSHolerite = new Business.HoleritesBusiness();
                List<Model.tb_holerite> ModelHolerites = BSHolerite.ConsultarHolerites();

                dgvHolerites.AutoGenerateColumns = false;
                dgvHolerites.DataSource = ModelHolerites;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarPorNomeHolerites()
        {
            try
            {
                string funcionario = txtFuncionario.Text;

                Business.HoleritesBusiness BSHolerite = new Business.HoleritesBusiness();
                List<Model.tb_holerite> ModelHolerites = BSHolerite.ConsultarPorNomeHolerites(funcionario);

                dgvHolerites.AutoGenerateColumns = false;
                dgvHolerites.DataSource = ModelHolerites;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
