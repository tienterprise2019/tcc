﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Holerites
{
    public partial class frmAlterar : Form
    {
        public frmAlterar()
        {
            InitializeComponent();
            this.CarregarHolerites();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvHolerites_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            this.CarregarHolerite();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            this.Alterar();
        }

        public void CarregarHolerites()
        {
            try
            {
                Business.HoleritesBusiness BSHolerites = new Business.HoleritesBusiness();
                List<Model.tb_holerite> ModelHolerites = BSHolerites.ConsultarHolerites();

                dgvHolerites.AutoGenerateColumns = false;
                dgvHolerites.DataSource = ModelHolerites;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarHolerite()
        {
            try
            {
                Model.tb_holerite ModelHolerites = dgvHolerites.CurrentRow.DataBoundItem as Model.tb_holerite;
                nudSlBruto.Value = ModelHolerites.vl_bruto;
                nudFgts.Value = ModelHolerites.vl_fgts;
                nudIrrf.Value = ModelHolerites.vl_irrf;
                nudSlLiquido.Value = ModelHolerites.sl_liquido;
                nudVlDecimoTerceiro.Value = ModelHolerites.vl_decimo_terceiro;
                dtpPagamento.Value = ModelHolerites.dt_pagamento;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Alterar()
        {
            try
            {
                Model.tb_holerite ModelHolerites = dgvHolerites.CurrentRow.DataBoundItem as Model.tb_holerite;
                ModelHolerites.vl_bruto = nudSlBruto.Value;
                ModelHolerites.vl_fgts = nudFgts.Value;
                ModelHolerites.vl_irrf = nudIrrf.Value;
                ModelHolerites.sl_liquido = nudSlLiquido.Value;
                ModelHolerites.vl_decimo_terceiro = nudVlDecimoTerceiro.Value;
                ModelHolerites.dt_pagamento = dtpPagamento.Value;

                Business.HoleritesBusiness BSHolerites = new Business.HoleritesBusiness();
                BSHolerites.AlterarHolerites(ModelHolerites);

                MessageBox.Show("Holerite alterado com sucesso!", "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
