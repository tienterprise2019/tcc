﻿namespace RascunhoTCC.Telas.Holerites
{
    partial class frmCadastrar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadastrar));
            this.label7 = new System.Windows.Forms.Label();
            this.dtpPagamento = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.nudVlDecimoTerceiro = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.nudSlLiquido = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nudIrrf = new System.Windows.Forms.NumericUpDown();
            this.nudFgts = new System.Windows.Forms.NumericUpDown();
            this.nudSlBruto = new System.Windows.Forms.NumericUpDown();
            this.cboFuncionario = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.chkPlSaude = new System.Windows.Forms.CheckBox();
            this.chkVA = new System.Windows.Forms.CheckBox();
            this.chkVT = new System.Windows.Forms.CheckBox();
            this.btnCadastrar = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rdnNaoSp = new System.Windows.Forms.RadioButton();
            this.rdnSimSp = new System.Windows.Forms.RadioButton();
            this.label14 = new System.Windows.Forms.Label();
            this.lblFechar = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.lblRelogio1 = new System.Windows.Forms.Label();
            this.panel33 = new System.Windows.Forms.Panel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel11 = new System.Windows.Forms.Panel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.button13 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.nudVlDecimoTerceiro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSlLiquido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIrrf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFgts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSlBruto)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.panel30.SuspendLayout();
            this.panel32.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(22, 333);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 21);
            this.label7.TabIndex = 12;
            this.label7.Text = "Pagamento:";
            // 
            // dtpPagamento
            // 
            this.dtpPagamento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpPagamento.Location = new System.Drawing.Point(153, 327);
            this.dtpPagamento.Name = "dtpPagamento";
            this.dtpPagamento.Size = new System.Drawing.Size(190, 29);
            this.dtpPagamento.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(29, 285);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 21);
            this.label6.TabIndex = 10;
            this.label6.Text = "Valor 13°:";
            // 
            // nudVlDecimoTerceiro
            // 
            this.nudVlDecimoTerceiro.Location = new System.Drawing.Point(153, 283);
            this.nudVlDecimoTerceiro.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.nudVlDecimoTerceiro.Name = "nudVlDecimoTerceiro";
            this.nudVlDecimoTerceiro.Size = new System.Drawing.Size(190, 29);
            this.nudVlDecimoTerceiro.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(9, 241);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 21);
            this.label5.TabIndex = 8;
            this.label5.Text = "Salário Líquido:";
            // 
            // nudSlLiquido
            // 
            this.nudSlLiquido.Location = new System.Drawing.Point(153, 239);
            this.nudSlLiquido.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.nudSlLiquido.Name = "nudSlLiquido";
            this.nudSlLiquido.Size = new System.Drawing.Size(190, 29);
            this.nudSlLiquido.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(45, 153);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 21);
            this.label4.TabIndex = 6;
            this.label4.Text = "IRRF:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(43, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 21);
            this.label3.TabIndex = 5;
            this.label3.Text = "FGTS:";
            // 
            // nudIrrf
            // 
            this.nudIrrf.Location = new System.Drawing.Point(153, 151);
            this.nudIrrf.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.nudIrrf.Name = "nudIrrf";
            this.nudIrrf.Size = new System.Drawing.Size(190, 29);
            this.nudIrrf.TabIndex = 3;
            // 
            // nudFgts
            // 
            this.nudFgts.Location = new System.Drawing.Point(153, 108);
            this.nudFgts.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.nudFgts.Name = "nudFgts";
            this.nudFgts.Size = new System.Drawing.Size(190, 29);
            this.nudFgts.TabIndex = 2;
            // 
            // nudSlBruto
            // 
            this.nudSlBruto.Location = new System.Drawing.Point(153, 195);
            this.nudSlBruto.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.nudSlBruto.Name = "nudSlBruto";
            this.nudSlBruto.Size = new System.Drawing.Size(190, 29);
            this.nudSlBruto.TabIndex = 4;
            // 
            // cboFuncionario
            // 
            this.cboFuncionario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFuncionario.FormattingEnabled = true;
            this.cboFuncionario.Location = new System.Drawing.Point(153, 66);
            this.cboFuncionario.Name = "cboFuncionario";
            this.cboFuncionario.Size = new System.Drawing.Size(190, 29);
            this.cboFuncionario.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(16, 197);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 21);
            this.label2.TabIndex = 2;
            this.label2.Text = "Salário Bruto:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.chkPlSaude);
            this.groupBox4.Controls.Add(this.chkVA);
            this.groupBox4.Controls.Add(this.chkVT);
            this.groupBox4.ForeColor = System.Drawing.Color.White;
            this.groupBox4.Location = new System.Drawing.Point(14, 386);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(346, 67);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Benefícios";
            // 
            // chkPlSaude
            // 
            this.chkPlSaude.AutoSize = true;
            this.chkPlSaude.Location = new System.Drawing.Point(131, 28);
            this.chkPlSaude.Name = "chkPlSaude";
            this.chkPlSaude.Size = new System.Drawing.Size(92, 25);
            this.chkPlSaude.TabIndex = 9;
            this.chkPlSaude.Text = "Pl. Saúde";
            this.chkPlSaude.UseVisualStyleBackColor = true;
            // 
            // chkVA
            // 
            this.chkVA.AutoSize = true;
            this.chkVA.Location = new System.Drawing.Point(279, 28);
            this.chkVA.Name = "chkVA";
            this.chkVA.Size = new System.Drawing.Size(48, 25);
            this.chkVA.TabIndex = 10;
            this.chkVA.Text = "VA";
            this.chkVA.UseVisualStyleBackColor = true;
            // 
            // chkVT
            // 
            this.chkVT.AutoSize = true;
            this.chkVT.Location = new System.Drawing.Point(19, 28);
            this.chkVT.Name = "chkVT";
            this.chkVT.Size = new System.Drawing.Size(47, 25);
            this.chkVT.TabIndex = 8;
            this.chkVT.Text = "VT";
            this.chkVT.UseVisualStyleBackColor = true;
            // 
            // btnCadastrar
            // 
            this.btnCadastrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastrar.ForeColor = System.Drawing.Color.White;
            this.btnCadastrar.Location = new System.Drawing.Point(22, 586);
            this.btnCadastrar.Name = "btnCadastrar";
            this.btnCadastrar.Size = new System.Drawing.Size(326, 60);
            this.btnCadastrar.TabIndex = 13;
            this.btnCadastrar.Text = "Cadastrar";
            this.btnCadastrar.UseVisualStyleBackColor = true;
            this.btnCadastrar.Click += new System.EventHandler(this.btnCadastrar_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rdnNaoSp);
            this.groupBox3.Controls.Add(this.rdnSimSp);
            this.groupBox3.ForeColor = System.Drawing.Color.White;
            this.groupBox3.Location = new System.Drawing.Point(14, 475);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(346, 67);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Salário Periculosidade";
            // 
            // rdnNaoSp
            // 
            this.rdnNaoSp.AutoSize = true;
            this.rdnNaoSp.Location = new System.Drawing.Point(222, 28);
            this.rdnNaoSp.Name = "rdnNaoSp";
            this.rdnNaoSp.Size = new System.Drawing.Size(57, 25);
            this.rdnNaoSp.TabIndex = 12;
            this.rdnNaoSp.TabStop = true;
            this.rdnNaoSp.Text = "Não";
            this.rdnNaoSp.UseVisualStyleBackColor = true;
            // 
            // rdnSimSp
            // 
            this.rdnSimSp.AutoSize = true;
            this.rdnSimSp.Location = new System.Drawing.Point(68, 28);
            this.rdnSimSp.Name = "rdnSimSp";
            this.rdnSimSp.Size = new System.Drawing.Size(55, 25);
            this.rdnSimSp.TabIndex = 11;
            this.rdnSimSp.TabStop = true;
            this.rdnSimSp.Text = "Sim";
            this.rdnSimSp.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(551, 9);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(184, 25);
            this.label14.TabIndex = 51;
            this.label14.Text = "Holerites- Cadastrar";
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.BackColor = System.Drawing.Color.Transparent;
            this.lblFechar.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechar.ForeColor = System.Drawing.Color.Transparent;
            this.lblFechar.Location = new System.Drawing.Point(1244, 10);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(24, 25);
            this.lblFechar.TabIndex = 14;
            this.lblFechar.Text = "X";
            this.lblFechar.Click += new System.EventHandler(this.lblFechar_Click);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.White;
            this.panel12.Location = new System.Drawing.Point(0, 46);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(1280, 1);
            this.panel12.TabIndex = 22;
            // 
            // lblRelogio1
            // 
            this.lblRelogio1.AutoSize = true;
            this.lblRelogio1.BackColor = System.Drawing.Color.Transparent;
            this.lblRelogio1.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRelogio1.ForeColor = System.Drawing.Color.White;
            this.lblRelogio1.Location = new System.Drawing.Point(45, 10);
            this.lblRelogio1.Name = "lblRelogio1";
            this.lblRelogio1.Size = new System.Drawing.Size(88, 25);
            this.lblRelogio1.TabIndex = 2;
            this.lblRelogio1.Text = "00:00:00";
            // 
            // panel33
            // 
            this.panel33.BackColor = System.Drawing.Color.White;
            this.panel33.Location = new System.Drawing.Point(375, 46);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(1, 674);
            this.panel33.TabIndex = 82;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel11.Controls.Add(this.label14);
            this.panel11.Controls.Add(this.pictureBox6);
            this.panel11.Controls.Add(this.lblFechar);
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Controls.Add(this.lblRelogio1);
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1280, 47);
            this.panel11.TabIndex = 79;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox6.BackgroundImage")));
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.Location = new System.Drawing.Point(12, 10);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(27, 25);
            this.pictureBox6.TabIndex = 31;
            this.pictureBox6.TabStop = false;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.Red;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Segoe UI Symbol", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.ForeColor = System.Drawing.Color.White;
            this.button13.Location = new System.Drawing.Point(-6, -7);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(38, 34);
            this.button13.TabIndex = 0;
            this.button13.Text = "X";
            this.button13.UseVisualStyleBackColor = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.ForeColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(0, 37);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(905, 1);
            this.panel3.TabIndex = 73;
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel30.Controls.Add(this.panel3);
            this.panel30.Controls.Add(this.panel9);
            this.panel30.Controls.Add(this.panel32);
            this.panel30.Location = new System.Drawing.Point(376, 682);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(905, 39);
            this.panel30.TabIndex = 80;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.White;
            this.panel9.ForeColor = System.Drawing.Color.White;
            this.panel9.Location = new System.Drawing.Point(1, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(905, 1);
            this.panel9.TabIndex = 71;
            // 
            // panel32
            // 
            this.panel32.BackColor = System.Drawing.Color.Red;
            this.panel32.Controls.Add(this.button13);
            this.panel32.Location = new System.Drawing.Point(1243, 12);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(25, 25);
            this.panel32.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.dtpPagamento);
            this.panel1.Controls.Add(this.nudVlDecimoTerceiro);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.nudSlLiquido);
            this.panel1.Controls.Add(this.btnCadastrar);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.nudSlBruto);
            this.panel1.Controls.Add(this.nudIrrf);
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.nudFgts);
            this.panel1.Controls.Add(this.cboFuncionario);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Location = new System.Drawing.Point(-1, 46);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(377, 675);
            this.panel1.TabIndex = 77;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(82, 40);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 1);
            this.panel2.TabIndex = 74;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(134, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(96, 21);
            this.label13.TabIndex = 73;
            this.label13.Text = "Informações";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.ForeColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(20, 69);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(95, 21);
            this.label25.TabIndex = 0;
            this.label25.Text = "Funcionário:";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.White;
            this.panel8.ForeColor = System.Drawing.Color.White;
            this.panel8.Location = new System.Drawing.Point(376, 699);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(905, 1);
            this.panel8.TabIndex = 81;
            // 
            // frmCadastrar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1280, 720);
            this.Controls.Add(this.panel33);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel30);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel8);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmCadastrar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastrar | Holerites";
            ((System.ComponentModel.ISupportInitialize)(this.nudVlDecimoTerceiro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSlLiquido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIrrf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFgts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSlBruto)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.panel30.ResumeLayout(false);
            this.panel32.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnCadastrar;
        private System.Windows.Forms.ComboBox cboFuncionario;
        private System.Windows.Forms.CheckBox chkVA;
        private System.Windows.Forms.CheckBox chkVT;
        private System.Windows.Forms.CheckBox chkPlSaude;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rdnNaoSp;
        private System.Windows.Forms.RadioButton rdnSimSp;
        private System.Windows.Forms.NumericUpDown nudSlBruto;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dtpPagamento;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nudVlDecimoTerceiro;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nudSlLiquido;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nudIrrf;
        private System.Windows.Forms.NumericUpDown nudFgts;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblFechar;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label lblRelogio1;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Panel panel8;
    }
}