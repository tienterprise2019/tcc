﻿namespace RascunhoTCC.Telas.Holerites
{
    partial class frmConsultar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConsultar));
            this.txtFuncionario = new System.Windows.Forms.TextBox();
            this.dgvHolerites = new System.Windows.Forms.DataGridView();
            this.id_holerite = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_funcionario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_bruto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tp_beneficio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_fgts = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_irrf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pl_convenio = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.sl_liquido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sl_periculosidade = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.vl_decimo_terceiro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dt_pagamento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.lblFechar = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.lblRelogio1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHolerites)).BeginInit();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtFuncionario
            // 
            this.txtFuncionario.Location = new System.Drawing.Point(222, 125);
            this.txtFuncionario.Name = "txtFuncionario";
            this.txtFuncionario.Size = new System.Drawing.Size(536, 29);
            this.txtFuncionario.TabIndex = 1;
            this.txtFuncionario.TextChanged += new System.EventHandler(this.txtFuncionario_TextChanged);
            // 
            // dgvHolerites
            // 
            this.dgvHolerites.AllowUserToAddRows = false;
            this.dgvHolerites.AllowUserToDeleteRows = false;
            this.dgvHolerites.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.dgvHolerites.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvHolerites.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHolerites.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_holerite,
            this.id_funcionario,
            this.vl_bruto,
            this.tp_beneficio,
            this.vl_fgts,
            this.vl_irrf,
            this.pl_convenio,
            this.sl_liquido,
            this.sl_periculosidade,
            this.vl_decimo_terceiro,
            this.dt_pagamento});
            this.dgvHolerites.Location = new System.Drawing.Point(0, 180);
            this.dgvHolerites.Name = "dgvHolerites";
            this.dgvHolerites.ReadOnly = true;
            this.dgvHolerites.Size = new System.Drawing.Size(843, 351);
            this.dgvHolerites.TabIndex = 2;
            // 
            // id_holerite
            // 
            this.id_holerite.DataPropertyName = "id_holerite";
            this.id_holerite.HeaderText = "Id";
            this.id_holerite.Name = "id_holerite";
            this.id_holerite.ReadOnly = true;
            // 
            // id_funcionario
            // 
            this.id_funcionario.DataPropertyName = "id_funcionario";
            this.id_funcionario.HeaderText = "Id. Funcionário";
            this.id_funcionario.Name = "id_funcionario";
            this.id_funcionario.ReadOnly = true;
            // 
            // vl_bruto
            // 
            this.vl_bruto.DataPropertyName = "vl_bruto";
            this.vl_bruto.HeaderText = "Salário Bruto";
            this.vl_bruto.Name = "vl_bruto";
            this.vl_bruto.ReadOnly = true;
            // 
            // tp_beneficio
            // 
            this.tp_beneficio.DataPropertyName = "tp_beneficio";
            this.tp_beneficio.HeaderText = "Benefício";
            this.tp_beneficio.Name = "tp_beneficio";
            this.tp_beneficio.ReadOnly = true;
            // 
            // vl_fgts
            // 
            this.vl_fgts.DataPropertyName = "vl_fgts";
            this.vl_fgts.HeaderText = "Fgts";
            this.vl_fgts.Name = "vl_fgts";
            this.vl_fgts.ReadOnly = true;
            // 
            // vl_irrf
            // 
            this.vl_irrf.DataPropertyName = "vl_irrf";
            this.vl_irrf.HeaderText = "Irrf";
            this.vl_irrf.Name = "vl_irrf";
            this.vl_irrf.ReadOnly = true;
            // 
            // pl_convenio
            // 
            this.pl_convenio.DataPropertyName = "pl_convenio";
            this.pl_convenio.HeaderText = "Convênio";
            this.pl_convenio.Name = "pl_convenio";
            this.pl_convenio.ReadOnly = true;
            // 
            // sl_liquido
            // 
            this.sl_liquido.DataPropertyName = "sl_liquido";
            this.sl_liquido.HeaderText = "Salário Líquido";
            this.sl_liquido.Name = "sl_liquido";
            this.sl_liquido.ReadOnly = true;
            // 
            // sl_periculosidade
            // 
            this.sl_periculosidade.DataPropertyName = "sl_periculosidade";
            this.sl_periculosidade.HeaderText = "Periculosidade";
            this.sl_periculosidade.Name = "sl_periculosidade";
            this.sl_periculosidade.ReadOnly = true;
            // 
            // vl_decimo_terceiro
            // 
            this.vl_decimo_terceiro.DataPropertyName = "vl_decimo_terceiro";
            this.vl_decimo_terceiro.HeaderText = "13°";
            this.vl_decimo_terceiro.Name = "vl_decimo_terceiro";
            this.vl_decimo_terceiro.ReadOnly = true;
            // 
            // dt_pagamento
            // 
            this.dt_pagamento.DataPropertyName = "dt_pagamento";
            this.dt_pagamento.HeaderText = "Dt. Pagamento";
            this.dt_pagamento.Name = "dt_pagamento";
            this.dt_pagamento.ReadOnly = true;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel11.Controls.Add(this.label8);
            this.panel11.Controls.Add(this.pictureBox6);
            this.panel11.Controls.Add(this.lblFechar);
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Controls.Add(this.lblRelogio1);
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1280, 47);
            this.panel11.TabIndex = 59;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(533, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(189, 25);
            this.label8.TabIndex = 51;
            this.label8.Text = "Holerites - Consultar";
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox6.BackgroundImage")));
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.Location = new System.Drawing.Point(12, 8);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(27, 25);
            this.pictureBox6.TabIndex = 31;
            this.pictureBox6.TabStop = false;
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.BackColor = System.Drawing.Color.Transparent;
            this.lblFechar.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechar.ForeColor = System.Drawing.Color.Transparent;
            this.lblFechar.Location = new System.Drawing.Point(1244, 8);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(24, 25);
            this.lblFechar.TabIndex = 3;
            this.lblFechar.Text = "X";
            this.lblFechar.Click += new System.EventHandler(this.lblFechar_Click);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.White;
            this.panel12.Location = new System.Drawing.Point(0, 44);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(1280, 1);
            this.panel12.TabIndex = 22;
            // 
            // lblRelogio1
            // 
            this.lblRelogio1.AutoSize = true;
            this.lblRelogio1.BackColor = System.Drawing.Color.Transparent;
            this.lblRelogio1.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRelogio1.ForeColor = System.Drawing.Color.White;
            this.lblRelogio1.Location = new System.Drawing.Point(45, 8);
            this.lblRelogio1.Name = "lblRelogio1";
            this.lblRelogio1.Size = new System.Drawing.Size(88, 25);
            this.lblRelogio1.TabIndex = 2;
            this.lblRelogio1.Text = "00:00:00";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(0, 698);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1280, 1);
            this.panel2.TabIndex = 58;
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel30.Location = new System.Drawing.Point(0, 698);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(1280, 22);
            this.panel30.TabIndex = 61;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(88, 125);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(27, 27);
            this.pictureBox3.TabIndex = 36;
            this.pictureBox3.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(26, 160);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(793, 1);
            this.panel3.TabIndex = 35;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.White;
            this.panel9.Location = new System.Drawing.Point(26, 548);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(793, 1);
            this.panel9.TabIndex = 29;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel9);
            this.panel1.Controls.Add(this.txtFuncionario);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.dgvHolerites);
            this.panel1.Location = new System.Drawing.Point(217, 45);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(842, 663);
            this.panel1.TabIndex = 60;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(121, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 21);
            this.label2.TabIndex = 2;
            this.label2.Text = "Funcionário:";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmConsultar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1280, 720);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel30);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmConsultar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consultar | Holerites";
            ((System.ComponentModel.ISupportInitialize)(this.dgvHolerites)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvHolerites;
        private System.Windows.Forms.TextBox txtFuncionario;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_holerite;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_funcionario;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_bruto;
        private System.Windows.Forms.DataGridViewTextBoxColumn tp_beneficio;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_fgts;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_irrf;
        private System.Windows.Forms.DataGridViewCheckBoxColumn pl_convenio;
        private System.Windows.Forms.DataGridViewTextBoxColumn sl_liquido;
        private System.Windows.Forms.DataGridViewCheckBoxColumn sl_periculosidade;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_decimo_terceiro;
        private System.Windows.Forms.DataGridViewTextBoxColumn dt_pagamento;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label lblFechar;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label lblRelogio1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer1;
    }
}