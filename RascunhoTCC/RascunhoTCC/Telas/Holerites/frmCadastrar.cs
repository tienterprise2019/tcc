﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Holerites
{
    public partial class frmCadastrar : Form
    {
        public frmCadastrar()
        {
            InitializeComponent();
            this.CarregarFuncionarios();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            this.Cadastrar();
        }

        public void CarregarFuncionarios()
        {
            try
            {

                Business.FuncionariosBusiness BSFuncionarios = new Business.FuncionariosBusiness();
                List<Model.tb_funcionario> ModelFuncionarios = BSFuncionarios.ConsultarFuncionarios();

                cboFuncionario.DisplayMember = nameof(Model.tb_funcionario.nm_funcionario);
                cboFuncionario.DataSource = ModelFuncionarios;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Cadastrar()
        {
            try
            {
                Model.tb_funcionario ModelFuncionario = cboFuncionario.SelectedItem as Model.tb_funcionario;
                Model.tb_holerite ModelHolerite = new Model.tb_holerite();

                ModelHolerite.id_funcionario = ModelFuncionario.id_funcionario;
                ModelHolerite.vl_bruto = nudSlBruto.Value;
                ModelHolerite.vl_fgts = nudFgts.Value;
                ModelHolerite.vl_irrf = nudIrrf.Value;
                ModelHolerite.sl_liquido = nudSlLiquido.Value;
                ModelHolerite.vl_decimo_terceiro = nudVlDecimoTerceiro.Value;
                ModelHolerite.dt_pagamento = dtpPagamento.Value;

                string beneficio = "";

                if (chkVT.Checked)
                {
                    beneficio = "VT";
                }
                else if (chkVA.Checked)
                {
                    beneficio = beneficio + ", VA";
                }

                ModelHolerite.tp_beneficio = beneficio;

                if (chkPlSaude.Checked)
                {
                    ModelHolerite.pl_convenio = true;
                }

                if (rdnSimSp.Checked)
                {
                    ModelHolerite.sl_periculosidade = true;
                }
                else if (rdnNaoSp.Checked)
                {
                    ModelHolerite.sl_periculosidade = false;
                }

                Business.HoleritesBusiness BSHolerites = new Business.HoleritesBusiness();
                BSHolerites.CadastrarHolerites(ModelHolerite);

                MessageBox.Show("Holerite cadastrado com sucesso!", "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Holerites", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
