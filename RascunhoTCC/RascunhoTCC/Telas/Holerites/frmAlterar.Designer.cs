﻿namespace RascunhoTCC.Telas.Holerites
{
    partial class frmAlterar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAlterar));
            this.btnAlterar = new System.Windows.Forms.Button();
            this.dgvHolerites = new System.Windows.Forms.DataGridView();
            this.id_holerite = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_funcionario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_bruto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tp_beneficio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_fgts = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_irrf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pl_convenio = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.sl_liquido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sl_periculosidade = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.vl_decimo_terceiro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dt_pagamento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label7 = new System.Windows.Forms.Label();
            this.dtpPagamento = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.nudVlDecimoTerceiro = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.nudSlLiquido = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nudIrrf = new System.Windows.Forms.NumericUpDown();
            this.nudFgts = new System.Windows.Forms.NumericUpDown();
            this.nudSlBruto = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.lblFechar = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.lblRelogio1 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHolerites)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVlDecimoTerceiro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSlLiquido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIrrf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFgts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSlBruto)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.panel11.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAlterar
            // 
            this.btnAlterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar.ForeColor = System.Drawing.Color.White;
            this.btnAlterar.Location = new System.Drawing.Point(186, 566);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(487, 57);
            this.btnAlterar.TabIndex = 8;
            this.btnAlterar.Text = "Alterar";
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            // 
            // dgvHolerites
            // 
            this.dgvHolerites.AllowUserToAddRows = false;
            this.dgvHolerites.AllowUserToDeleteRows = false;
            this.dgvHolerites.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.dgvHolerites.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvHolerites.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHolerites.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_holerite,
            this.id_funcionario,
            this.vl_bruto,
            this.tp_beneficio,
            this.vl_fgts,
            this.vl_irrf,
            this.pl_convenio,
            this.sl_liquido,
            this.sl_periculosidade,
            this.vl_decimo_terceiro,
            this.dt_pagamento});
            this.dgvHolerites.Location = new System.Drawing.Point(0, 271);
            this.dgvHolerites.Name = "dgvHolerites";
            this.dgvHolerites.ReadOnly = true;
            this.dgvHolerites.Size = new System.Drawing.Size(842, 258);
            this.dgvHolerites.TabIndex = 1;
            this.dgvHolerites.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvHolerites_CellContentClick);
            // 
            // id_holerite
            // 
            this.id_holerite.DataPropertyName = "id_holerite";
            this.id_holerite.HeaderText = "Id";
            this.id_holerite.Name = "id_holerite";
            this.id_holerite.ReadOnly = true;
            // 
            // id_funcionario
            // 
            this.id_funcionario.DataPropertyName = "id_funcionario";
            this.id_funcionario.HeaderText = "Id. Funcionário";
            this.id_funcionario.Name = "id_funcionario";
            this.id_funcionario.ReadOnly = true;
            // 
            // vl_bruto
            // 
            this.vl_bruto.DataPropertyName = "vl_bruto";
            this.vl_bruto.HeaderText = "Salário Bruto";
            this.vl_bruto.Name = "vl_bruto";
            this.vl_bruto.ReadOnly = true;
            // 
            // tp_beneficio
            // 
            this.tp_beneficio.DataPropertyName = "tp_beneficio";
            this.tp_beneficio.HeaderText = "Benefício";
            this.tp_beneficio.Name = "tp_beneficio";
            this.tp_beneficio.ReadOnly = true;
            // 
            // vl_fgts
            // 
            this.vl_fgts.DataPropertyName = "vl_fgts";
            this.vl_fgts.HeaderText = "Fgts";
            this.vl_fgts.Name = "vl_fgts";
            this.vl_fgts.ReadOnly = true;
            // 
            // vl_irrf
            // 
            this.vl_irrf.DataPropertyName = "vl_irrf";
            this.vl_irrf.HeaderText = "Irrf";
            this.vl_irrf.Name = "vl_irrf";
            this.vl_irrf.ReadOnly = true;
            // 
            // pl_convenio
            // 
            this.pl_convenio.DataPropertyName = "pl_convenio";
            this.pl_convenio.HeaderText = "Convênio";
            this.pl_convenio.Name = "pl_convenio";
            this.pl_convenio.ReadOnly = true;
            // 
            // sl_liquido
            // 
            this.sl_liquido.DataPropertyName = "sl_liquido";
            this.sl_liquido.HeaderText = "Salário Líquido";
            this.sl_liquido.Name = "sl_liquido";
            this.sl_liquido.ReadOnly = true;
            // 
            // sl_periculosidade
            // 
            this.sl_periculosidade.DataPropertyName = "sl_periculosidade";
            this.sl_periculosidade.HeaderText = "Periculosidade";
            this.sl_periculosidade.Name = "sl_periculosidade";
            this.sl_periculosidade.ReadOnly = true;
            // 
            // vl_decimo_terceiro
            // 
            this.vl_decimo_terceiro.DataPropertyName = "vl_decimo_terceiro";
            this.vl_decimo_terceiro.HeaderText = "13°";
            this.vl_decimo_terceiro.Name = "vl_decimo_terceiro";
            this.vl_decimo_terceiro.ReadOnly = true;
            // 
            // dt_pagamento
            // 
            this.dt_pagamento.DataPropertyName = "dt_pagamento";
            this.dt_pagamento.HeaderText = "Dt. Pagamento";
            this.dt_pagamento.Name = "dt_pagamento";
            this.dt_pagamento.ReadOnly = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(450, 202);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 21);
            this.label7.TabIndex = 12;
            this.label7.Text = "Pagamento:";
            // 
            // dtpPagamento
            // 
            this.dtpPagamento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpPagamento.Location = new System.Drawing.Point(558, 196);
            this.dtpPagamento.Name = "dtpPagamento";
            this.dtpPagamento.Size = new System.Drawing.Size(222, 29);
            this.dtpPagamento.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(67, 202);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 21);
            this.label6.TabIndex = 10;
            this.label6.Text = "Valor 13°:";
            // 
            // nudVlDecimoTerceiro
            // 
            this.nudVlDecimoTerceiro.Location = new System.Drawing.Point(161, 196);
            this.nudVlDecimoTerceiro.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.nudVlDecimoTerceiro.Name = "nudVlDecimoTerceiro";
            this.nudVlDecimoTerceiro.Size = new System.Drawing.Size(222, 29);
            this.nudVlDecimoTerceiro.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(435, 123);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 21);
            this.label5.TabIndex = 8;
            this.label5.Text = "Salário Líquido:";
            // 
            // nudSlLiquido
            // 
            this.nudSlLiquido.Location = new System.Drawing.Point(558, 121);
            this.nudSlLiquido.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.nudSlLiquido.Name = "nudSlLiquido";
            this.nudSlLiquido.Size = new System.Drawing.Size(222, 29);
            this.nudSlLiquido.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(86, 123);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 21);
            this.label4.TabIndex = 6;
            this.label4.Text = "Irrf:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(474, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 21);
            this.label3.TabIndex = 5;
            this.label3.Text = "Fgts:";
            // 
            // nudIrrf
            // 
            this.nudIrrf.Location = new System.Drawing.Point(161, 121);
            this.nudIrrf.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.nudIrrf.Name = "nudIrrf";
            this.nudIrrf.Size = new System.Drawing.Size(222, 29);
            this.nudIrrf.TabIndex = 3;
            // 
            // nudFgts
            // 
            this.nudFgts.Location = new System.Drawing.Point(558, 46);
            this.nudFgts.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.nudFgts.Name = "nudFgts";
            this.nudFgts.Size = new System.Drawing.Size(222, 29);
            this.nudFgts.TabIndex = 5;
            // 
            // nudSlBruto
            // 
            this.nudSlBruto.Location = new System.Drawing.Point(161, 44);
            this.nudSlBruto.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.nudSlBruto.Name = "nudSlBruto";
            this.nudSlBruto.Size = new System.Drawing.Size(222, 29);
            this.nudSlBruto.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(52, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 21);
            this.label2.TabIndex = 2;
            this.label2.Text = "Salário Bruto:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.dgvHolerites);
            this.panel1.Controls.Add(this.dtpPagamento);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.btnAlterar);
            this.panel1.Controls.Add(this.nudVlDecimoTerceiro);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.nudSlBruto);
            this.panel1.Controls.Add(this.nudSlLiquido);
            this.panel1.Controls.Add(this.nudFgts);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.nudIrrf);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(217, 47);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(842, 663);
            this.panel1.TabIndex = 71;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.Location = new System.Drawing.Point(45, 156);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(750, 1);
            this.panel5.TabIndex = 25;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Location = new System.Drawing.Point(45, 231);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(750, 1);
            this.panel4.TabIndex = 24;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(45, 79);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(750, 1);
            this.panel3.TabIndex = 23;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(542, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(166, 25);
            this.label8.TabIndex = 51;
            this.label8.Text = "Holerites - Alterar";
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox6.BackgroundImage")));
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.Location = new System.Drawing.Point(12, 8);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(27, 25);
            this.pictureBox6.TabIndex = 31;
            this.pictureBox6.TabStop = false;
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.BackColor = System.Drawing.Color.Transparent;
            this.lblFechar.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechar.ForeColor = System.Drawing.Color.Transparent;
            this.lblFechar.Location = new System.Drawing.Point(1244, 8);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(24, 25);
            this.lblFechar.TabIndex = 9;
            this.lblFechar.Text = "X";
            this.lblFechar.Click += new System.EventHandler(this.lblFechar_Click);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.White;
            this.panel12.Location = new System.Drawing.Point(0, 46);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(1280, 1);
            this.panel12.TabIndex = 22;
            // 
            // lblRelogio1
            // 
            this.lblRelogio1.AutoSize = true;
            this.lblRelogio1.BackColor = System.Drawing.Color.Transparent;
            this.lblRelogio1.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRelogio1.ForeColor = System.Drawing.Color.White;
            this.lblRelogio1.Location = new System.Drawing.Point(45, 8);
            this.lblRelogio1.Name = "lblRelogio1";
            this.lblRelogio1.Size = new System.Drawing.Size(88, 25);
            this.lblRelogio1.TabIndex = 2;
            this.lblRelogio1.Text = "00:00:00";
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel11.Controls.Add(this.label8);
            this.panel11.Controls.Add(this.pictureBox6);
            this.panel11.Controls.Add(this.lblFechar);
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Controls.Add(this.lblRelogio1);
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1280, 47);
            this.panel11.TabIndex = 70;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(0, 698);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1280, 1);
            this.panel2.TabIndex = 66;
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel30.Location = new System.Drawing.Point(0, 698);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(1280, 22);
            this.panel30.TabIndex = 72;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmAlterar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1280, 720);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel30);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmAlterar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alterar | Holerites";
            ((System.ComponentModel.ISupportInitialize)(this.dgvHolerites)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVlDecimoTerceiro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSlLiquido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIrrf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFgts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSlBruto)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dtpPagamento;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nudVlDecimoTerceiro;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nudSlLiquido;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nudIrrf;
        private System.Windows.Forms.NumericUpDown nudFgts;
        private System.Windows.Forms.NumericUpDown nudSlBruto;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgvHolerites;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_holerite;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_funcionario;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_bruto;
        private System.Windows.Forms.DataGridViewTextBoxColumn tp_beneficio;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_fgts;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_irrf;
        private System.Windows.Forms.DataGridViewCheckBoxColumn pl_convenio;
        private System.Windows.Forms.DataGridViewTextBoxColumn sl_liquido;
        private System.Windows.Forms.DataGridViewCheckBoxColumn sl_periculosidade;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_decimo_terceiro;
        private System.Windows.Forms.DataGridViewTextBoxColumn dt_pagamento;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label lblFechar;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label lblRelogio1;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Timer timer1;
    }
}