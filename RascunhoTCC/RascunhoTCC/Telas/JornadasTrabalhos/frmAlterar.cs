﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.JornadasTrabalhos
{
    public partial class frmAlterar : Form
    {
        public frmAlterar()
        {
            InitializeComponent();
            this.CarregarJornadasTrabalhos();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvJornadasTrabalhos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            this.CarregarJornadaTrabalho();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            this.Alterar();
        }

        public void CarregarJornadasTrabalhos()
        {
            try
            {
                Business.JornadasTrabalhosBusiness BSJornadasTrabalhos = new Business.JornadasTrabalhosBusiness();
                List<Model.tb_jornada_trabalho> ModelJornadasTrabalhos = BSJornadasTrabalhos.ConsultarJornadasTrabalhos();

                dgvJornadasTrabalhos.AutoGenerateColumns = false;
                dgvJornadasTrabalhos.DataSource = ModelJornadasTrabalhos;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarJornadaTrabalho()
        {
            try
            {
                Model.tb_jornada_trabalho ModelJornadaTrabalho = dgvJornadasTrabalhos.CurrentRow.DataBoundItem as Model.tb_jornada_trabalho;

                txtHrEntrada.Text = Convert.ToString(ModelJornadaTrabalho.hr_entrada);
                txtHrSaidaIntervalo.Text = Convert.ToString(ModelJornadaTrabalho.hr_saida_interv);
                txtHrRetornoIntervalo.Text = Convert.ToString(ModelJornadaTrabalho.hr_retorno_intrv);
                txtHrSaida.Text = Convert.ToString(ModelJornadaTrabalho.hr_saida);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Alterar()
        {
            try
            {
                Model.tb_jornada_trabalho ModelJornadaTrabalho = dgvJornadasTrabalhos.CurrentRow.DataBoundItem as Model.tb_jornada_trabalho;
                ModelJornadaTrabalho.hr_entrada = TimeSpan.Parse(txtHrEntrada.Text);
                ModelJornadaTrabalho.hr_saida_interv = TimeSpan.Parse(txtHrSaidaIntervalo.Text);
                ModelJornadaTrabalho.hr_retorno_intrv = TimeSpan.Parse(txtHrRetornoIntervalo.Text);
                ModelJornadaTrabalho.hr_saida = TimeSpan.Parse(txtHrSaida.Text);

                Business.JornadasTrabalhosBusiness BSJornadaTrabalho = new Business.JornadasTrabalhosBusiness();
                BSJornadaTrabalho.AlterarJornadasTrabalhos(ModelJornadaTrabalho);

                MessageBox.Show("Jornada de Trabalho alterada com sucesso!", "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
