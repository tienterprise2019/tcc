﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.JornadasTrabalhos
{
    public partial class frmRemover : Form
    {
        public frmRemover()
        {
            InitializeComponent();
            this.CarregarJornadasTrabalhos();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRemover_Click(object sender, EventArgs e)
        {
            this.Remover();
        }

        public void CarregarJornadasTrabalhos()
        {
            try
            {
                Business.JornadasTrabalhosBusiness BSJornadaTrabalho = new Business.JornadasTrabalhosBusiness();
                List<Model.tb_jornada_trabalho> ModelJornadasTrabalhos = BSJornadaTrabalho.ConsultarJornadasTrabalhos();

                dgvJornadasTrabalhos.AutoGenerateColumns = false;
                dgvJornadasTrabalhos.DataSource = ModelJornadasTrabalhos;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Remover()
        {
            try
            {
                Model.tb_jornada_trabalho ModelJornadaTrabalho = dgvJornadasTrabalhos.CurrentRow.DataBoundItem as Model.tb_jornada_trabalho;

                Business.JornadasTrabalhosBusiness BSJornadaTrabalho = new Business.JornadasTrabalhosBusiness();
                BSJornadaTrabalho.RemoverJornadasTrabalhos(ModelJornadaTrabalho.id_jornada_trabalho);

                MessageBox.Show("Jornada de Trabalho removida com sucesso!", "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.CarregarJornadasTrabalhos();

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
