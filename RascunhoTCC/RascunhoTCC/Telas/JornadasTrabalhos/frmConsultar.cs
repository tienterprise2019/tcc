﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.JornadasTrabalhos
{
    public partial class frmConsultar : Form
    {
        public frmConsultar()
        {
            InitializeComponent();
            this.CarregarJornadasTrabalhos();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtFuncionario_TextChanged(object sender, EventArgs e)
        {
            this.CarregarPorNomeFuncionarios();
        }

        public void CarregarJornadasTrabalhos()
        {
            try
            {
                Business.JornadasTrabalhosBusiness BSJornadaTrabalho = new Business.JornadasTrabalhosBusiness();
                List<Model.tb_jornada_trabalho> ModelJornadasTrabalhos = BSJornadaTrabalho.ConsultarJornadasTrabalhos();

                dgvJornadasTrabalhos.AutoGenerateColumns = false;
                dgvJornadasTrabalhos.DataSource = ModelJornadasTrabalhos;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarPorNomeFuncionarios()
        {
            try
            {
                string funcionario = txtFuncionario.Text;

                Business.JornadasTrabalhosBusiness BSJornadaTrabalho = new Business.JornadasTrabalhosBusiness();
                List<Model.tb_jornada_trabalho> ModelJornadasTrabalhos = BSJornadaTrabalho.ConsultarPorNomeFuncionario(funcionario);

                dgvJornadasTrabalhos.AutoGenerateColumns = false;
                dgvJornadasTrabalhos.DataSource = ModelJornadasTrabalhos;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
