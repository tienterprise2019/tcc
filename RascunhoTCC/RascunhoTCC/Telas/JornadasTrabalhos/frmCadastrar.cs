﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.JornadasTrabalhos
{
    public partial class frmCadastrar : Form
    {
        public frmCadastrar()
        {
            InitializeComponent();
            this.CarregarFuncionarios();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            this.Cadastrar();
        }

        public void CarregarFuncionarios()
        {
            try
            {
                Business.FuncionariosBusiness BSFuncionario = new Business.FuncionariosBusiness();
                List<Model.tb_funcionario> ModelFuncionarios = BSFuncionario.ConsultarFuncionarios();

                cboFuncionario.DisplayMember = nameof(Model.tb_funcionario.nm_funcionario);
                cboFuncionario.DataSource = ModelFuncionarios;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Cadastrar()
        {
            try
            {
                Model.tb_funcionario ModelFuncionario = cboFuncionario.SelectedItem as Model.tb_funcionario;

                Model.tb_jornada_trabalho ModelJornadaTrabalho = new Model.tb_jornada_trabalho();
                ModelJornadaTrabalho.id_funcionario = ModelFuncionario.id_funcionario;
                ModelJornadaTrabalho.hr_entrada = TimeSpan.Parse(txtHrEntrada.Text);
                ModelJornadaTrabalho.hr_saida_interv = TimeSpan.Parse(txtHrSaidaIntervalo.Text);
                ModelJornadaTrabalho.hr_retorno_intrv = TimeSpan.Parse(txtHrRetornoIntervalo.Text);
                ModelJornadaTrabalho.hr_saida = TimeSpan.Parse(txtHrSaida.Text);

                Business.JornadasTrabalhosBusiness BSJornadaTrabalho = new Business.JornadasTrabalhosBusiness();
                BSJornadaTrabalho.CadastrarJornadasTrabalhos(ModelJornadaTrabalho);

                MessageBox.Show("Jornada de trabalho cadastrada com sucesso!", "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Jornada de Trabalho", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
