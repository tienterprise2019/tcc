﻿namespace RascunhoTCC.Telas.Clientes
{
    partial class frmRemover
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRemover));
            this.dgvClientes = new System.Windows.Forms.DataGridView();
            this.id_cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm_cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ds_cpf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dt_nascimento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ds_telefone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ds_celular = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ds_email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tp_deficiente = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ds_obs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm_rua = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nr_numero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm_bairro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm_cidade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ds_cep = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnRemover = new System.Windows.Forms.Button();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.lblFechar = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.lblRelogio1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgvClientes)).BeginInit();
            this.panel30.SuspendLayout();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvClientes
            // 
            this.dgvClientes.AllowUserToAddRows = false;
            this.dgvClientes.AllowUserToDeleteRows = false;
            this.dgvClientes.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.dgvClientes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvClientes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_cliente,
            this.nm_cliente,
            this.ds_cpf,
            this.dt_nascimento,
            this.ds_telefone,
            this.ds_celular,
            this.ds_email,
            this.tp_deficiente,
            this.ds_obs,
            this.nm_rua,
            this.nr_numero,
            this.nm_bairro,
            this.nm_cidade,
            this.ds_cep});
            this.dgvClientes.Location = new System.Drawing.Point(0, 175);
            this.dgvClientes.Name = "dgvClientes";
            this.dgvClientes.ReadOnly = true;
            this.dgvClientes.Size = new System.Drawing.Size(842, 362);
            this.dgvClientes.TabIndex = 1;
            // 
            // id_cliente
            // 
            this.id_cliente.DataPropertyName = "id_cliente";
            this.id_cliente.HeaderText = "Id";
            this.id_cliente.Name = "id_cliente";
            this.id_cliente.ReadOnly = true;
            // 
            // nm_cliente
            // 
            this.nm_cliente.DataPropertyName = "nm_cliente";
            this.nm_cliente.HeaderText = "Nome";
            this.nm_cliente.Name = "nm_cliente";
            this.nm_cliente.ReadOnly = true;
            // 
            // ds_cpf
            // 
            this.ds_cpf.DataPropertyName = "ds_cpf";
            this.ds_cpf.HeaderText = "Cpf";
            this.ds_cpf.Name = "ds_cpf";
            this.ds_cpf.ReadOnly = true;
            // 
            // dt_nascimento
            // 
            this.dt_nascimento.DataPropertyName = "dt_nascimento";
            this.dt_nascimento.HeaderText = "Dt. Nascimento";
            this.dt_nascimento.Name = "dt_nascimento";
            this.dt_nascimento.ReadOnly = true;
            // 
            // ds_telefone
            // 
            this.ds_telefone.DataPropertyName = "ds_telefone";
            this.ds_telefone.HeaderText = "Telefone";
            this.ds_telefone.Name = "ds_telefone";
            this.ds_telefone.ReadOnly = true;
            // 
            // ds_celular
            // 
            this.ds_celular.DataPropertyName = "ds_celular";
            this.ds_celular.HeaderText = "Celular";
            this.ds_celular.Name = "ds_celular";
            this.ds_celular.ReadOnly = true;
            // 
            // ds_email
            // 
            this.ds_email.DataPropertyName = "ds_email";
            this.ds_email.HeaderText = "E-mail";
            this.ds_email.Name = "ds_email";
            this.ds_email.ReadOnly = true;
            // 
            // tp_deficiente
            // 
            this.tp_deficiente.DataPropertyName = "tp_deficiente";
            this.tp_deficiente.HeaderText = "Deficiente";
            this.tp_deficiente.Name = "tp_deficiente";
            this.tp_deficiente.ReadOnly = true;
            // 
            // ds_obs
            // 
            this.ds_obs.DataPropertyName = "ds_obs";
            this.ds_obs.HeaderText = "Obs";
            this.ds_obs.Name = "ds_obs";
            this.ds_obs.ReadOnly = true;
            // 
            // nm_rua
            // 
            this.nm_rua.DataPropertyName = "nm_rua";
            this.nm_rua.HeaderText = "Rua";
            this.nm_rua.Name = "nm_rua";
            this.nm_rua.ReadOnly = true;
            // 
            // nr_numero
            // 
            this.nr_numero.DataPropertyName = "nr_numero";
            this.nr_numero.HeaderText = "N°";
            this.nr_numero.Name = "nr_numero";
            this.nr_numero.ReadOnly = true;
            // 
            // nm_bairro
            // 
            this.nm_bairro.DataPropertyName = "nm_bairro";
            this.nm_bairro.HeaderText = "Bairro";
            this.nm_bairro.Name = "nm_bairro";
            this.nm_bairro.ReadOnly = true;
            // 
            // nm_cidade
            // 
            this.nm_cidade.DataPropertyName = "nm_cidade";
            this.nm_cidade.HeaderText = "Cidade";
            this.nm_cidade.Name = "nm_cidade";
            this.nm_cidade.ReadOnly = true;
            // 
            // ds_cep
            // 
            this.ds_cep.DataPropertyName = "ds_cep";
            this.ds_cep.HeaderText = "Cep";
            this.ds_cep.Name = "ds_cep";
            this.ds_cep.ReadOnly = true;
            // 
            // btnRemover
            // 
            this.btnRemover.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemover.ForeColor = System.Drawing.Color.White;
            this.btnRemover.Location = new System.Drawing.Point(196, 579);
            this.btnRemover.Name = "btnRemover";
            this.btnRemover.Size = new System.Drawing.Size(487, 52);
            this.btnRemover.TabIndex = 2;
            this.btnRemover.Text = "Remover";
            this.btnRemover.UseVisualStyleBackColor = true;
            this.btnRemover.Click += new System.EventHandler(this.btnRemover_Click);
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel30.Controls.Add(this.panel2);
            this.panel30.Location = new System.Drawing.Point(0, 698);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(1280, 22);
            this.panel30.TabIndex = 69;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(1, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1280, 1);
            this.panel2.TabIndex = 66;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel11.Controls.Add(this.label8);
            this.panel11.Controls.Add(this.pictureBox6);
            this.panel11.Controls.Add(this.lblFechar);
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Controls.Add(this.lblRelogio1);
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1280, 47);
            this.panel11.TabIndex = 67;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(536, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(174, 25);
            this.label8.TabIndex = 51;
            this.label8.Text = "Clientes - Remover";
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox6.BackgroundImage")));
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.Location = new System.Drawing.Point(12, 8);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(27, 25);
            this.pictureBox6.TabIndex = 31;
            this.pictureBox6.TabStop = false;
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.BackColor = System.Drawing.Color.Transparent;
            this.lblFechar.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechar.ForeColor = System.Drawing.Color.Transparent;
            this.lblFechar.Location = new System.Drawing.Point(1244, 8);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(24, 25);
            this.lblFechar.TabIndex = 3;
            this.lblFechar.Text = "X";
            this.lblFechar.Click += new System.EventHandler(this.lblFechar_Click);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.White;
            this.panel12.Location = new System.Drawing.Point(0, 46);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(1280, 1);
            this.panel12.TabIndex = 22;
            // 
            // lblRelogio1
            // 
            this.lblRelogio1.AutoSize = true;
            this.lblRelogio1.BackColor = System.Drawing.Color.Transparent;
            this.lblRelogio1.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRelogio1.ForeColor = System.Drawing.Color.White;
            this.lblRelogio1.Location = new System.Drawing.Point(45, 8);
            this.lblRelogio1.Name = "lblRelogio1";
            this.lblRelogio1.Size = new System.Drawing.Size(88, 25);
            this.lblRelogio1.TabIndex = 2;
            this.lblRelogio1.Text = "00:00:00";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel1.Controls.Add(this.dgvClientes);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel9);
            this.panel1.Controls.Add(this.btnRemover);
            this.panel1.Location = new System.Drawing.Point(217, 45);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(842, 663);
            this.panel1.TabIndex = 68;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(26, 160);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(793, 1);
            this.panel3.TabIndex = 35;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.White;
            this.panel9.Location = new System.Drawing.Point(26, 548);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(793, 1);
            this.panel9.TabIndex = 29;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmRemover
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1280, 720);
            this.Controls.Add(this.panel30);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmRemover";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Remover | Clientes";
            ((System.ComponentModel.ISupportInitialize)(this.dgvClientes)).EndInit();
            this.panel30.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnRemover;
        private System.Windows.Forms.DataGridView dgvClientes;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_cliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm_cliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn ds_cpf;
        private System.Windows.Forms.DataGridViewTextBoxColumn dt_nascimento;
        private System.Windows.Forms.DataGridViewTextBoxColumn ds_telefone;
        private System.Windows.Forms.DataGridViewTextBoxColumn ds_celular;
        private System.Windows.Forms.DataGridViewTextBoxColumn ds_email;
        private System.Windows.Forms.DataGridViewCheckBoxColumn tp_deficiente;
        private System.Windows.Forms.DataGridViewTextBoxColumn ds_obs;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm_rua;
        private System.Windows.Forms.DataGridViewTextBoxColumn nr_numero;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm_bairro;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm_cidade;
        private System.Windows.Forms.DataGridViewTextBoxColumn ds_cep;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label lblFechar;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label lblRelogio1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Timer timer1;
    }
}