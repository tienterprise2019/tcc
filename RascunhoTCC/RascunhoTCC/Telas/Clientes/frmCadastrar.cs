﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Clientes
{
    public partial class frmCadastrar : Form
    {
        public frmCadastrar()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtCep_Leave(object sender, EventArgs e)
        {
            this.Buscar(txtCep.Text);
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            this.Cadastrar();
        }

        private void rdnSim_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                lblObs.Visible = true;
                txtObs.Visible = true;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rdnNao_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                lblObs.Visible = false;
                txtObs.Visible = false;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Cadastrar()
        {
            try
            {
                Model.tb_cliente ModelCliente = new Model.tb_cliente();
                ModelCliente.nm_cliente = txtNome.Text;
                ModelCliente.ds_cpf = txtCPF.Text;
                ModelCliente.dt_nascimento = dtpNasc.Value;
                ModelCliente.ds_telefone = txtTelefone.Text;
                ModelCliente.ds_celular = txtCelular.Text;
                ModelCliente.ds_email = txtEmail.Text;
                ModelCliente.ds_cep = txtCep.Text;
                ModelCliente.nm_rua = txtRua.Text;
                ModelCliente.nr_numero = Convert.ToInt32(txtNumero.Text);
                ModelCliente.nm_bairro = txtBairro.Text;
                ModelCliente.nm_cidade = txtCidade.Text;

                if(rdnSim.Checked)
                {
                    ModelCliente.tp_deficiente = true;
                    ModelCliente.ds_obs = txtObs.Text;
                }
                else if(rdnNao.Checked)
                {
                    ModelCliente.tp_deficiente = false;
                }

                Business.ClientesBusiness BSCliente = new Business.ClientesBusiness();
                BSCliente.CadastrarClientes(ModelCliente);

                MessageBox.Show("Cliente cadastrado com sucesso!", "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Buscar(string cep)
        {
            try
            {
                // Cria objeto responsável por conversar com uma API
                System.Net.WebClient rest = new System.Net.WebClient();
                rest.Encoding = Encoding.UTF8;

                // Chama API do correio, concatenando o cep
                string resposta = rest.DownloadString("https://viacep.com.br/ws/" + cep + "/json");

                // Transforma a resposta do correio em DTO
                dynamic r = Newtonsoft.Json.JsonConvert.DeserializeObject(resposta);

                txtRua.Text = r.logradouro;
                txtBairro.Text = r.bairro;
                txtCidade.Text = r.localidade;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
