﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Clientes
{
    public partial class frmAlterar : Form
    {
        public frmAlterar()
        {
            InitializeComponent();
            this.CarregarClientes();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cboClientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.CarregarCliente();
        }

        private void rdnSim_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                lblObs.Visible = true;
                txtObs.Visible = true;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rdnNao_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                lblObs.Visible = false;
                txtObs.Visible = false;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtCep_Leave(object sender, EventArgs e)
        {
            this.Buscar(txtCep.Text);
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            this.Alterar();
        }

        public void CarregarClientes()
        {
            try
            {
                Business.ClientesBusiness BSClientes = new Business.ClientesBusiness();
                List<Model.tb_cliente> ModelClientes = BSClientes.ConsultarClientes();

                cboClientes.DisplayMember = nameof(Model.tb_cliente.nm_cliente);
                cboClientes.DataSource = ModelClientes;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarCliente()
        {
            try
            {
                Model.tb_cliente ModelCliente = cboClientes.SelectedItem as Model.tb_cliente;
                txtCPF.Text = ModelCliente.ds_cpf;
                dtpNasc.Value = ModelCliente.dt_nascimento;
                txtTelefone.Text = ModelCliente.ds_telefone;
                txtCelular.Text = ModelCliente.ds_celular;
                txtEmail.Text = ModelCliente.ds_email;
                txtCep.Text = ModelCliente.ds_cep;
                txtNumero.Text = Convert.ToString(ModelCliente.nr_numero);
                txtRua.Text = ModelCliente.nm_rua;
                txtBairro.Text = ModelCliente.nm_bairro;
                txtCidade.Text = ModelCliente.nm_cidade;
                
                if(ModelCliente.tp_deficiente == true)
                {
                    rdnSim.Checked = true;
                    lblObs.Visible = true;
                    txtObs.Visible = true;
                    txtObs.Text = ModelCliente.ds_obs;
                    
                }
                else if(ModelCliente.tp_deficiente == false)
                {
                    rdnNao.Checked = true;
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Alterar()
        {
            try
            {
                Model.tb_cliente ModelCliente = cboClientes.SelectedItem as Model.tb_cliente;
                ModelCliente.ds_cpf = txtCPF.Text;
                ModelCliente.dt_nascimento = dtpNasc.Value;
                ModelCliente.ds_telefone = txtTelefone.Text;
                ModelCliente.ds_celular = txtCelular.Text;
                ModelCliente.ds_email = txtEmail.Text;
                ModelCliente.ds_cep = txtCep.Text;
                ModelCliente.nm_rua = txtRua.Text;
                ModelCliente.nr_numero = Convert.ToInt32(txtNumero.Text);
                ModelCliente.nm_bairro = txtBairro.Text;
                ModelCliente.nm_cidade = txtCidade.Text;

                if (rdnSim.Checked)
                {
                    ModelCliente.tp_deficiente = true;
                    ModelCliente.ds_obs = txtObs.Text;
                }
                else if (rdnNao.Checked)
                {
                    ModelCliente.tp_deficiente = false;
                }

                Business.ClientesBusiness BSClientes = new Business.ClientesBusiness();
                BSClientes.AlterarClientes(ModelCliente);

                MessageBox.Show("Clientes alterado com sucesso!", "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void Buscar(string cep)
        {
            try
            {
                // Cria objeto responsável por conversar com uma API
                System.Net.WebClient rest = new System.Net.WebClient();
                rest.Encoding = Encoding.UTF8;

                // Chama API do correio, concatenando o cep
                string resposta = rest.DownloadString("https://viacep.com.br/ws/" + cep + "/json");

                // Transforma a resposta do correio em DTO
                dynamic r = Newtonsoft.Json.JsonConvert.DeserializeObject(resposta);

                txtRua.Text = r.logradouro;
                txtBairro.Text = r.bairro;
                txtCidade.Text = r.localidade;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
