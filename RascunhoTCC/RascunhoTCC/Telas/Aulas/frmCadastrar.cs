﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Aulas
{
    public partial class frmCadastrar : Form
    {
        public frmCadastrar()
        {
            InitializeComponent();
            this.CarregarClientes();
            this.CarregarFuncionarios();
            this.CarregarModelos();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rdnTeorica_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                grpCarro.Visible = false;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rdnPratica_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                grpCarro.Visible = true;
                this.CarregarModelos();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            this.Cadastrar();
        }

        public void CarregarClientes()
        {
            try
            {
                Business.ClientesBusiness BSCliente = new Business.ClientesBusiness();
                List<Model.tb_cliente> ModelClientes = BSCliente.ConsultarClientes();

                cboAluno.DisplayMember = nameof(Model.tb_cliente.nm_cliente);
                cboAluno.DataSource = ModelClientes;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarFuncionarios()
        {
            try
            {
                Business.FuncionariosBusiness BSFuncionario = new Business.FuncionariosBusiness();
                List<Model.tb_funcionario> ModelFuncionarios = BSFuncionario.ConsultarFuncionarios();

                cboInstrutor.DisplayMember = nameof(Model.tb_funcionario.nm_funcionario);
                cboInstrutor.DataSource = ModelFuncionarios;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarModelos()
        {
            try
            {
                Business.CarrosBusiness BSCarro = new Business.CarrosBusiness();
                List<Model.tb_carro> ModelCarros = BSCarro.ConsultarCarros();

                cboCarro.DisplayMember = nameof(Model.tb_carro.nm_modelo);
                cboCarro.DataSource = ModelCarros;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Cadastrar()
        {
            try
            {
                Model.tb_cliente ModelCliente = cboAluno.SelectedItem as Model.tb_cliente;
                Model.tb_funcionario ModelFuncionario = cboInstrutor.SelectedItem as Model.tb_funcionario;
                Model.tb_carro ModelCarro = cboCarro.SelectedItem as Model.tb_carro;

                Model.tb_aula ModelAulas = new Model.tb_aula();
                ModelAulas.id_cliente = ModelCliente.id_cliente;
                ModelAulas.id_instrutor = ModelFuncionario.id_funcionario;
                ModelAulas.dt_aula = dtpData.Value;
                ModelAulas.hr_saida = TimeSpan.Parse(txtSaida.Text);
                ModelAulas.vl_preco = nudPreco.Value;

                if (rdnA.Checked)
                {
                    ModelAulas.tp_carteira = "A";
                }
                else if (rdnB.Checked)
                {
                    ModelAulas.tp_carteira = "B";
                }
                else if (rdnD.Checked)
                {
                    ModelAulas.tp_carteira = "D";
                }
                else if (rdnAB.Checked)
                {
                    ModelAulas.tp_carteira = "AB";
                }

                if (rdnPratica.Checked)
                {
                    ModelAulas.id_carro = ModelCarro.id_carro;
                    ModelAulas.km_inicial = nudKmInicial.Value;
                    ModelAulas.tp_aula = "Pratica";
                }
                else if (rdnTeorica.Checked)
                {
                    ModelAulas.tp_aula = "Teórica";
                }

                Business.AulasBusiness BSAula = new Business.AulasBusiness();
                BSAula.CadastrarAulas(ModelAulas);

                MessageBox.Show("Aula cadastrada com sucesso!", "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
