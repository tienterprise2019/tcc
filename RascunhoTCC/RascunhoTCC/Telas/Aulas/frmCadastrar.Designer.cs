﻿namespace RascunhoTCC.Telas.Aulas
{
    partial class frmCadastrar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadastrar));
            this.panel2 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.lblRelogio1 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.lblFechar = new System.Windows.Forms.Label();
            this.panel30 = new System.Windows.Forms.Panel();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.nudPreco = new System.Windows.Forms.NumericUpDown();
            this.txtSaida = new System.Windows.Forms.MaskedTextBox();
            this.dtpData = new System.Windows.Forms.DateTimePicker();
            this.cboAluno = new System.Windows.Forms.ComboBox();
            this.cboInstrutor = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.rdnPratica = new System.Windows.Forms.RadioButton();
            this.rdnTeorica = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grpCarro = new System.Windows.Forms.GroupBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.nudKmInicial = new System.Windows.Forms.NumericUpDown();
            this.cboCarro = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdnAB = new System.Windows.Forms.RadioButton();
            this.rdnD = new System.Windows.Forms.RadioButton();
            this.rdnB = new System.Windows.Forms.RadioButton();
            this.rdnA = new System.Windows.Forms.RadioButton();
            this.btnCadastrar = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.panel11.SuspendLayout();
            this.panel30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPreco)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.grpCarro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudKmInicial)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(1, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1280, 1);
            this.panel2.TabIndex = 35;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(532, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(159, 25);
            this.label10.TabIndex = 51;
            this.label10.Text = "Aulas - Cadastrar";
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox8.BackgroundImage")));
            this.pictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox8.Location = new System.Drawing.Point(12, 9);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(27, 25);
            this.pictureBox8.TabIndex = 31;
            this.pictureBox8.TabStop = false;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.White;
            this.panel12.Location = new System.Drawing.Point(0, 46);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(1280, 1);
            this.panel12.TabIndex = 22;
            // 
            // lblRelogio1
            // 
            this.lblRelogio1.AutoSize = true;
            this.lblRelogio1.BackColor = System.Drawing.Color.Transparent;
            this.lblRelogio1.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRelogio1.ForeColor = System.Drawing.Color.White;
            this.lblRelogio1.Location = new System.Drawing.Point(47, 9);
            this.lblRelogio1.Name = "lblRelogio1";
            this.lblRelogio1.Size = new System.Drawing.Size(88, 25);
            this.lblRelogio1.TabIndex = 2;
            this.lblRelogio1.Text = "00:00:00";
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel11.Controls.Add(this.label10);
            this.panel11.Controls.Add(this.lblFechar);
            this.panel11.Controls.Add(this.pictureBox8);
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Controls.Add(this.lblRelogio1);
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1280, 47);
            this.panel11.TabIndex = 54;
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.BackColor = System.Drawing.Color.Transparent;
            this.lblFechar.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechar.ForeColor = System.Drawing.Color.Transparent;
            this.lblFechar.Location = new System.Drawing.Point(1243, 9);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(24, 25);
            this.lblFechar.TabIndex = 15;
            this.lblFechar.Text = "X";
            this.lblFechar.Click += new System.EventHandler(this.lblFechar_Click);
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel30.Controls.Add(this.panel2);
            this.panel30.Location = new System.Drawing.Point(1, 698);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(1280, 22);
            this.panel30.TabIndex = 55;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox7.BackgroundImage")));
            this.pictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox7.Location = new System.Drawing.Point(37, 135);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(27, 28);
            this.pictureBox7.TabIndex = 36;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox6.BackgroundImage")));
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.Location = new System.Drawing.Point(37, 190);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(26, 23);
            this.pictureBox6.TabIndex = 36;
            this.pictureBox6.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.pictureBox7);
            this.groupBox1.Controls.Add(this.pictureBox6);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.pictureBox3);
            this.groupBox1.Controls.Add(this.pictureBox5);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.nudPreco);
            this.groupBox1.Controls.Add(this.txtSaida);
            this.groupBox1.Controls.Add(this.dtpData);
            this.groupBox1.Controls.Add(this.cboAluno);
            this.groupBox1.Controls.Add(this.cboInstrutor);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(269, 89);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(461, 278);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informações da Aula";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(34, 241);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(30, 25);
            this.pictureBox1.TabIndex = 35;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(36, 32);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(27, 27);
            this.pictureBox3.TabIndex = 33;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox5.BackgroundImage")));
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox5.Location = new System.Drawing.Point(36, 81);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(31, 32);
            this.pictureBox5.TabIndex = 34;
            this.pictureBox5.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(76, 136);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 21);
            this.label7.TabIndex = 14;
            this.label7.Text = "Valor:";
            // 
            // nudPreco
            // 
            this.nudPreco.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.nudPreco.ForeColor = System.Drawing.Color.White;
            this.nudPreco.Location = new System.Drawing.Point(167, 132);
            this.nudPreco.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.nudPreco.Name = "nudPreco";
            this.nudPreco.Size = new System.Drawing.Size(263, 29);
            this.nudPreco.TabIndex = 9;
            // 
            // txtSaida
            // 
            this.txtSaida.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.txtSaida.ForeColor = System.Drawing.Color.White;
            this.txtSaida.Location = new System.Drawing.Point(167, 184);
            this.txtSaida.Mask = "00:00";
            this.txtSaida.Name = "txtSaida";
            this.txtSaida.Size = new System.Drawing.Size(263, 29);
            this.txtSaida.TabIndex = 10;
            // 
            // dtpData
            // 
            this.dtpData.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpData.Location = new System.Drawing.Point(167, 237);
            this.dtpData.Name = "dtpData";
            this.dtpData.Size = new System.Drawing.Size(263, 29);
            this.dtpData.TabIndex = 11;
            // 
            // cboAluno
            // 
            this.cboAluno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.cboAluno.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAluno.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboAluno.ForeColor = System.Drawing.Color.White;
            this.cboAluno.FormattingEnabled = true;
            this.cboAluno.Location = new System.Drawing.Point(167, 24);
            this.cboAluno.Name = "cboAluno";
            this.cboAluno.Size = new System.Drawing.Size(263, 29);
            this.cboAluno.TabIndex = 7;
            // 
            // cboInstrutor
            // 
            this.cboInstrutor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.cboInstrutor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInstrutor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboInstrutor.ForeColor = System.Drawing.Color.White;
            this.cboInstrutor.FormattingEnabled = true;
            this.cboInstrutor.Location = new System.Drawing.Point(167, 78);
            this.cboInstrutor.Name = "cboInstrutor";
            this.cboInstrutor.Size = new System.Drawing.Size(263, 29);
            this.cboInstrutor.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(76, 192);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 21);
            this.label5.TabIndex = 8;
            this.label5.Text = "Saída:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(76, 243);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 21);
            this.label4.TabIndex = 6;
            this.label4.Text = "Data:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(76, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 21);
            this.label2.TabIndex = 2;
            this.label2.Text = "Instrutor:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(76, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Aluno:";
            // 
            // rdnPratica
            // 
            this.rdnPratica.AutoSize = true;
            this.rdnPratica.Location = new System.Drawing.Point(122, 31);
            this.rdnPratica.Name = "rdnPratica";
            this.rdnPratica.Size = new System.Drawing.Size(75, 25);
            this.rdnPratica.TabIndex = 2;
            this.rdnPratica.TabStop = true;
            this.rdnPratica.Text = "Pratica";
            this.rdnPratica.UseVisualStyleBackColor = true;
            this.rdnPratica.CheckedChanged += new System.EventHandler(this.rdnPratica_CheckedChanged);
            // 
            // rdnTeorica
            // 
            this.rdnTeorica.AutoSize = true;
            this.rdnTeorica.Location = new System.Drawing.Point(27, 31);
            this.rdnTeorica.Name = "rdnTeorica";
            this.rdnTeorica.Size = new System.Drawing.Size(76, 25);
            this.rdnTeorica.TabIndex = 1;
            this.rdnTeorica.TabStop = true;
            this.rdnTeorica.Text = "Teorica";
            this.rdnTeorica.UseVisualStyleBackColor = true;
            this.rdnTeorica.CheckedChanged += new System.EventHandler(this.rdnTeorica_CheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.rdnPratica);
            this.groupBox4.Controls.Add(this.rdnTeorica);
            this.groupBox4.ForeColor = System.Drawing.Color.White;
            this.groupBox4.Location = new System.Drawing.Point(20, 101);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(219, 74);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Aula";
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.Controls.Add(this.grpCarro);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.btnCadastrar);
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Location = new System.Drawing.Point(268, 47);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(745, 661);
            this.panel1.TabIndex = 53;
            // 
            // grpCarro
            // 
            this.grpCarro.BackColor = System.Drawing.Color.Transparent;
            this.grpCarro.Controls.Add(this.pictureBox4);
            this.grpCarro.Controls.Add(this.pictureBox2);
            this.grpCarro.Controls.Add(this.label6);
            this.grpCarro.Controls.Add(this.nudKmInicial);
            this.grpCarro.Controls.Add(this.cboCarro);
            this.grpCarro.Controls.Add(this.label3);
            this.grpCarro.ForeColor = System.Drawing.Color.White;
            this.grpCarro.Location = new System.Drawing.Point(54, 388);
            this.grpCarro.Name = "grpCarro";
            this.grpCarro.Size = new System.Drawing.Size(633, 124);
            this.grpCarro.TabIndex = 4;
            this.grpCarro.TabStop = false;
            this.grpCarro.Text = "Carro";
            this.grpCarro.Visible = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox4.BackgroundImage")));
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(90, 79);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(34, 31);
            this.pictureBox4.TabIndex = 33;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(92, 35);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(29, 27);
            this.pictureBox2.TabIndex = 35;
            this.pictureBox2.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(131, 83);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 21);
            this.label6.TabIndex = 6;
            this.label6.Text = "Km. Inicial:";
            // 
            // nudKmInicial
            // 
            this.nudKmInicial.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.nudKmInicial.ForeColor = System.Drawing.Color.White;
            this.nudKmInicial.Location = new System.Drawing.Point(300, 81);
            this.nudKmInicial.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.nudKmInicial.Name = "nudKmInicial";
            this.nudKmInicial.Size = new System.Drawing.Size(263, 29);
            this.nudKmInicial.TabIndex = 13;
            // 
            // cboCarro
            // 
            this.cboCarro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.cboCarro.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCarro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboCarro.ForeColor = System.Drawing.Color.White;
            this.cboCarro.FormattingEnabled = true;
            this.cboCarro.Location = new System.Drawing.Point(300, 33);
            this.cboCarro.Name = "cboCarro";
            this.cboCarro.Size = new System.Drawing.Size(263, 29);
            this.cboCarro.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(131, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 21);
            this.label3.TabIndex = 4;
            this.label3.Text = "Modelo:";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.rdnAB);
            this.groupBox2.Controls.Add(this.rdnD);
            this.groupBox2.Controls.Add(this.rdnB);
            this.groupBox2.Controls.Add(this.rdnA);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(20, 209);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(219, 146);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Carteira";
            // 
            // rdnAB
            // 
            this.rdnAB.AutoSize = true;
            this.rdnAB.Location = new System.Drawing.Point(129, 101);
            this.rdnAB.Name = "rdnAB";
            this.rdnAB.Size = new System.Drawing.Size(47, 25);
            this.rdnAB.TabIndex = 6;
            this.rdnAB.TabStop = true;
            this.rdnAB.Text = "AB";
            this.rdnAB.UseVisualStyleBackColor = true;
            // 
            // rdnD
            // 
            this.rdnD.AutoSize = true;
            this.rdnD.Location = new System.Drawing.Point(36, 101);
            this.rdnD.Name = "rdnD";
            this.rdnD.Size = new System.Drawing.Size(39, 25);
            this.rdnD.TabIndex = 5;
            this.rdnD.TabStop = true;
            this.rdnD.Text = "D";
            this.rdnD.UseVisualStyleBackColor = true;
            // 
            // rdnB
            // 
            this.rdnB.AutoSize = true;
            this.rdnB.Location = new System.Drawing.Point(129, 38);
            this.rdnB.Name = "rdnB";
            this.rdnB.Size = new System.Drawing.Size(37, 25);
            this.rdnB.TabIndex = 4;
            this.rdnB.TabStop = true;
            this.rdnB.Text = "B";
            this.rdnB.UseVisualStyleBackColor = true;
            // 
            // rdnA
            // 
            this.rdnA.AutoSize = true;
            this.rdnA.Location = new System.Drawing.Point(36, 38);
            this.rdnA.Name = "rdnA";
            this.rdnA.Size = new System.Drawing.Size(38, 25);
            this.rdnA.TabIndex = 3;
            this.rdnA.TabStop = true;
            this.rdnA.Text = "A";
            this.rdnA.UseVisualStyleBackColor = true;
            // 
            // btnCadastrar
            // 
            this.btnCadastrar.BackColor = System.Drawing.Color.Transparent;
            this.btnCadastrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastrar.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrar.ForeColor = System.Drawing.Color.White;
            this.btnCadastrar.Location = new System.Drawing.Point(189, 560);
            this.btnCadastrar.Name = "btnCadastrar";
            this.btnCadastrar.Size = new System.Drawing.Size(382, 56);
            this.btnCadastrar.TabIndex = 14;
            this.btnCadastrar.Text = "Cadastrar";
            this.btnCadastrar.UseVisualStyleBackColor = false;
            this.btnCadastrar.Click += new System.EventHandler(this.btnCadastrar_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmCadastrar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1280, 720);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel30);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmCadastrar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastrar | Aulas";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel30.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPreco)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.grpCarro.ResumeLayout(false);
            this.grpCarro.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudKmInicial)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label lblRelogio1;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label lblFechar;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown nudPreco;
        private System.Windows.Forms.MaskedTextBox txtSaida;
        private System.Windows.Forms.DateTimePicker dtpData;
        private System.Windows.Forms.ComboBox cboAluno;
        private System.Windows.Forms.ComboBox cboInstrutor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rdnPratica;
        private System.Windows.Forms.RadioButton rdnTeorica;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox grpCarro;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nudKmInicial;
        private System.Windows.Forms.ComboBox cboCarro;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rdnAB;
        private System.Windows.Forms.RadioButton rdnD;
        private System.Windows.Forms.RadioButton rdnB;
        private System.Windows.Forms.RadioButton rdnA;
        private System.Windows.Forms.Button btnCadastrar;
        private System.Windows.Forms.Timer timer1;
    }
}