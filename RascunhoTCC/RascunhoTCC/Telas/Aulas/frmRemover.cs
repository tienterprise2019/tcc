﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Aulas
{
    public partial class frmRemover : Form
    {
        public frmRemover()
        {
            InitializeComponent();
            this.CarregarAulas();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRemover_Click(object sender, EventArgs e)
        {
            this.Remover();
        }

        public void CarregarAulas()
        {
            try
            {
                Business.AulasBusiness BSAula = new Business.AulasBusiness();
                List<Model.tb_aula> ModelAulas = BSAula.ConsultarAulas();

                dgvAulas.AutoGenerateColumns = false;
                dgvAulas.DataSource = ModelAulas;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Remover()
        {
            try
            {
                Model.tb_aula ModelAula = dgvAulas.CurrentRow.DataBoundItem as Model.tb_aula;

                Business.AulasBusiness BSAula = new Business.AulasBusiness();
                BSAula.RemoverAulas(ModelAula.id_aula);

                MessageBox.Show("Aula removida com sucesso!", "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.CarregarAulas();

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
