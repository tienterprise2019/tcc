﻿namespace RascunhoTCC.Telas.Aulas
{
    partial class frmRemover
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRemover));
            this.dgvAulas = new System.Windows.Forms.DataGridView();
            this.id_aula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_instrutor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tp_aula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tp_carteira = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dt_aula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_preco = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hr_saida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hr_chegada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_carro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.km_inicial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.km_final = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ds_obs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnRemover = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.lblFechar = new System.Windows.Forms.Label();
            this.lblRelogio1 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAulas)).BeginInit();
            this.panel30.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.panel11.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvAulas
            // 
            this.dgvAulas.AllowUserToAddRows = false;
            this.dgvAulas.AllowUserToDeleteRows = false;
            this.dgvAulas.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.dgvAulas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvAulas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAulas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_aula,
            this.id_cliente,
            this.id_instrutor,
            this.tp_aula,
            this.tp_carteira,
            this.dt_aula,
            this.vl_preco,
            this.hr_saida,
            this.hr_chegada,
            this.id_carro,
            this.km_inicial,
            this.km_final,
            this.ds_obs});
            this.dgvAulas.Location = new System.Drawing.Point(-1, 135);
            this.dgvAulas.Name = "dgvAulas";
            this.dgvAulas.ReadOnly = true;
            this.dgvAulas.Size = new System.Drawing.Size(844, 351);
            this.dgvAulas.TabIndex = 1;
            // 
            // id_aula
            // 
            this.id_aula.DataPropertyName = "id_aula";
            this.id_aula.HeaderText = "Id";
            this.id_aula.Name = "id_aula";
            this.id_aula.ReadOnly = true;
            // 
            // id_cliente
            // 
            this.id_cliente.DataPropertyName = "id_cliente";
            this.id_cliente.HeaderText = "Id. Cliente";
            this.id_cliente.Name = "id_cliente";
            this.id_cliente.ReadOnly = true;
            // 
            // id_instrutor
            // 
            this.id_instrutor.DataPropertyName = "id_instrutor";
            this.id_instrutor.HeaderText = "Id. Instrutor";
            this.id_instrutor.Name = "id_instrutor";
            this.id_instrutor.ReadOnly = true;
            // 
            // tp_aula
            // 
            this.tp_aula.DataPropertyName = "tp_aula";
            this.tp_aula.HeaderText = "Aula";
            this.tp_aula.Name = "tp_aula";
            this.tp_aula.ReadOnly = true;
            // 
            // tp_carteira
            // 
            this.tp_carteira.DataPropertyName = "tp_carteira";
            this.tp_carteira.HeaderText = "Tp. Carteira";
            this.tp_carteira.Name = "tp_carteira";
            this.tp_carteira.ReadOnly = true;
            // 
            // dt_aula
            // 
            this.dt_aula.DataPropertyName = "dt_aula";
            this.dt_aula.HeaderText = "Data";
            this.dt_aula.Name = "dt_aula";
            this.dt_aula.ReadOnly = true;
            // 
            // vl_preco
            // 
            this.vl_preco.DataPropertyName = "vl_preco";
            this.vl_preco.HeaderText = "Valor";
            this.vl_preco.Name = "vl_preco";
            this.vl_preco.ReadOnly = true;
            // 
            // hr_saida
            // 
            this.hr_saida.DataPropertyName = "hr_saida";
            this.hr_saida.HeaderText = "Hr. Saída";
            this.hr_saida.Name = "hr_saida";
            this.hr_saida.ReadOnly = true;
            // 
            // hr_chegada
            // 
            this.hr_chegada.DataPropertyName = "hr_chegada";
            this.hr_chegada.HeaderText = "Hr. Chegada";
            this.hr_chegada.Name = "hr_chegada";
            this.hr_chegada.ReadOnly = true;
            // 
            // id_carro
            // 
            this.id_carro.DataPropertyName = "id_carro";
            this.id_carro.HeaderText = "Id. Carro";
            this.id_carro.Name = "id_carro";
            this.id_carro.ReadOnly = true;
            // 
            // km_inicial
            // 
            this.km_inicial.DataPropertyName = "km_inicial";
            this.km_inicial.HeaderText = "Km. Inicial";
            this.km_inicial.Name = "km_inicial";
            this.km_inicial.ReadOnly = true;
            // 
            // km_final
            // 
            this.km_final.DataPropertyName = "km_final";
            this.km_final.HeaderText = "Km. Final";
            this.km_final.Name = "km_final";
            this.km_final.ReadOnly = true;
            // 
            // ds_obs
            // 
            this.ds_obs.DataPropertyName = "ds_obs";
            this.ds_obs.HeaderText = "Obs";
            this.ds_obs.Name = "ds_obs";
            this.ds_obs.ReadOnly = true;
            // 
            // btnRemover
            // 
            this.btnRemover.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemover.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold);
            this.btnRemover.ForeColor = System.Drawing.Color.White;
            this.btnRemover.Location = new System.Drawing.Point(200, 551);
            this.btnRemover.Name = "btnRemover";
            this.btnRemover.Size = new System.Drawing.Size(487, 64);
            this.btnRemover.TabIndex = 2;
            this.btnRemover.Text = "Remover";
            this.btnRemover.UseVisualStyleBackColor = true;
            this.btnRemover.Click += new System.EventHandler(this.btnRemover_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(0, 47);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1280, 1);
            this.panel3.TabIndex = 66;
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel30.Controls.Add(this.panel5);
            this.panel30.Location = new System.Drawing.Point(0, 698);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(1280, 22);
            this.panel30.TabIndex = 65;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1280, 1);
            this.panel5.TabIndex = 67;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(0, 698);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1280, 1);
            this.panel2.TabIndex = 62;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.dgvAulas);
            this.panel1.Controls.Add(this.btnRemover);
            this.panel1.Location = new System.Drawing.Point(214, 46);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(843, 662);
            this.panel1.TabIndex = 64;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Location = new System.Drawing.Point(26, 504);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(793, 1);
            this.panel4.TabIndex = 36;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(554, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(153, 25);
            this.label8.TabIndex = 51;
            this.label8.Text = "Aulas - Remover";
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox6.BackgroundImage")));
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.Location = new System.Drawing.Point(12, 8);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(27, 25);
            this.pictureBox6.TabIndex = 31;
            this.pictureBox6.TabStop = false;
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.BackColor = System.Drawing.Color.Transparent;
            this.lblFechar.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechar.ForeColor = System.Drawing.Color.Transparent;
            this.lblFechar.Location = new System.Drawing.Point(1244, 8);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(24, 25);
            this.lblFechar.TabIndex = 3;
            this.lblFechar.Text = "X";
            this.lblFechar.Click += new System.EventHandler(this.lblFechar_Click);
            // 
            // lblRelogio1
            // 
            this.lblRelogio1.AutoSize = true;
            this.lblRelogio1.BackColor = System.Drawing.Color.Transparent;
            this.lblRelogio1.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRelogio1.ForeColor = System.Drawing.Color.White;
            this.lblRelogio1.Location = new System.Drawing.Point(45, 8);
            this.lblRelogio1.Name = "lblRelogio1";
            this.lblRelogio1.Size = new System.Drawing.Size(88, 25);
            this.lblRelogio1.TabIndex = 2;
            this.lblRelogio1.Text = "00:00:00";
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel11.Controls.Add(this.label8);
            this.panel11.Controls.Add(this.pictureBox6);
            this.panel11.Controls.Add(this.lblFechar);
            this.panel11.Controls.Add(this.lblRelogio1);
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1280, 47);
            this.panel11.TabIndex = 63;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmRemover
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1280, 720);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel30);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel11);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmRemover";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Remover | Aulas";
            ((System.ComponentModel.ISupportInitialize)(this.dgvAulas)).EndInit();
            this.panel30.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnRemover;
        private System.Windows.Forms.DataGridView dgvAulas;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_aula;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_cliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_instrutor;
        private System.Windows.Forms.DataGridViewTextBoxColumn tp_aula;
        private System.Windows.Forms.DataGridViewTextBoxColumn tp_carteira;
        private System.Windows.Forms.DataGridViewTextBoxColumn dt_aula;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_preco;
        private System.Windows.Forms.DataGridViewTextBoxColumn hr_saida;
        private System.Windows.Forms.DataGridViewTextBoxColumn hr_chegada;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_carro;
        private System.Windows.Forms.DataGridViewTextBoxColumn km_inicial;
        private System.Windows.Forms.DataGridViewTextBoxColumn km_final;
        private System.Windows.Forms.DataGridViewTextBoxColumn ds_obs;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label lblFechar;
        private System.Windows.Forms.Label lblRelogio1;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Timer timer1;
    }
}