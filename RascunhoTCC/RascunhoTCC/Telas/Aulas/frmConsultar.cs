﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Aulas
{
    public partial class frmConsultar : Form
    {
        public frmConsultar()
        {
            InitializeComponent();
            this.CarregarAulas();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtAluno_TextChanged(object sender, EventArgs e)
        {
            this.CarregarPorNomeClientes();
        }

        public void CarregarAulas()
        {
            try
            {
                Business.AulasBusiness BSAula = new Business.AulasBusiness();
                List<Model.tb_aula> ModelAulas = BSAula.ConsultarAulas();

                dgvAulas.AutoGenerateColumns = false;
                dgvAulas.DataSource = ModelAulas;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarPorNomeClientes()
        {
            try
            {
                string cliente = txtAluno.Text;

                Business.AulasBusiness BSAula = new Business.AulasBusiness();
                List<Model.tb_aula> ModelAulas = BSAula.ConsultarPorNomeClientes(cliente);

                dgvAulas.AutoGenerateColumns = false;
                dgvAulas.DataSource = ModelAulas;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Aulas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
