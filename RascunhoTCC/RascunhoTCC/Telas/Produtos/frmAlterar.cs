﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Produtos
{
    public partial class frmAlterar : Form
    {
        public frmAlterar()
        {
            InitializeComponent();
            this.CarregarProdutos();
            this.CarregarFornecedores();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cboNome_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.CarregarProduto();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            this.Alterar();
        }

        public void CarregarProdutos()
        {
            try
            {
                Business.ProdutosBusiness BSProdutos = new Business.ProdutosBusiness();
                List<Model.tb_produto> ModelProdutos = BSProdutos.ConsultarProdutos();

                cboNome.DisplayMember = nameof(Model.tb_produto.nm_produto);
                cboNome.DataSource = ModelProdutos;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarFornecedores()
        {
            try
            {
                Business.FornecedoresBusiness BSFornecedor = new Business.FornecedoresBusiness();
                List<Model.tb_fornecedor> ModelFornecedores = BSFornecedor.ConsultarFornecedores();

                cboFornecedor.DisplayMember = nameof(Model.tb_fornecedor.nm_fornecedor);
                cboFornecedor.DataSource = ModelFornecedores;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarProduto()
        {
            try
            {
                Model.tb_fornecedor ModelFornecedor = cboFornecedor.SelectedItem as Model.tb_fornecedor;
                Model.tb_produto ModelProduto = cboNome.SelectedItem as Model.tb_produto;

                nudPreco.Value = ModelProduto.vl_preco;
                nudQtdAntiga.Value = ModelProduto.qt_antiga;
                nudQtdAtual.Value = ModelProduto.qt_atual;
                txtTipo.Text = ModelProduto.ds_tipo;
                dtpCompra.Value = ModelProduto.dt_compra;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Alterar()
        {
            try
            {
                Model.tb_fornecedor ModelFornecedor = cboFornecedor.SelectedItem as Model.tb_fornecedor;
                Model.tb_produto ModelProduto = cboNome.SelectedItem as Model.tb_produto;
                ModelProduto.vl_preco = nudPreco.Value;
                ModelProduto.qt_antiga = Convert.ToInt32(nudQtdAntiga.Value);
                ModelProduto.qt_atual = Convert.ToInt32(nudQtdAtual.Value);
                ModelProduto.ds_tipo = txtTipo.Text;
                ModelProduto.dt_compra = dtpCompra.Value;
                ModelProduto.id_fornecedor = ModelFornecedor.id_fornecedor;

                Business.ProdutosBusiness BSProdutos = new Business.ProdutosBusiness();
                BSProdutos.AlterarProdutos(ModelProduto);

                MessageBox.Show("Produto alterado com sucesso!", "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
