﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Produtos
{
    public partial class frmConsultar : Form
    {
        public frmConsultar()
        {
            InitializeComponent();
            this.CarregarProdutos();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtProduto_TextChanged(object sender, EventArgs e)
        {
            this.CarregarPorNomeProdutos();
        }

        public void CarregarProdutos()
        {
            try
            {
                Business.ProdutosBusiness BSProduto = new Business.ProdutosBusiness();
                List<Model.tb_produto> ModelProdutos = BSProduto.ConsultarProdutos();

                dgvProdutos.AutoGenerateColumns = false;
                dgvProdutos.DataSource = ModelProdutos;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarPorNomeProdutos()
        {
            try
            {
                string produto = txtProduto.Text;

                Business.ProdutosBusiness BSProduto = new Business.ProdutosBusiness();
                List<Model.tb_produto> ModelProdutos = BSProduto.ConsultarPorNomeProdutos(produto);

                dgvProdutos.DataSource = ModelProdutos;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
