﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Produtos
{
    public partial class frmCadastrar : Form
    {
        public frmCadastrar()
        {
            InitializeComponent();
            this.CarregarFornecedores();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            this.Cadastrar();
        }

        public void CarregarFornecedores()
        {
            try
            {
                Business.FornecedoresBusiness BSFornecedor = new Business.FornecedoresBusiness();
                List<Model.tb_fornecedor> ModelFornecedores = BSFornecedor.ConsultarFornecedores();

                cboFornecedor.DisplayMember = nameof(Model.tb_fornecedor.nm_fornecedor);
                cboFornecedor.DataSource = ModelFornecedores;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Cadastrar()
        {
            try
            {
                Model.tb_fornecedor ModelFornecedor = cboFornecedor.SelectedItem as Model.tb_fornecedor;
                Model.tb_produto ModelProduto = new Model.tb_produto();
                ModelProduto.nm_produto = txtProduto.Text;
                ModelProduto.vl_preco = nudPreco.Value;
                ModelProduto.qt_antiga = Convert.ToInt32(nudQtdAntiga.Value);
                ModelProduto.qt_atual = Convert.ToInt32(nudQtdAtual.Value);
                ModelProduto.ds_tipo = txtTipo.Text;
                ModelProduto.dt_compra = dtpCompra.Value;
                ModelProduto.id_fornecedor = ModelFornecedor.id_fornecedor;

                Business.ProdutosBusiness BSProduto = new Business.ProdutosBusiness();
                BSProduto.CadastrarProdutos(ModelProduto);

                MessageBox.Show("Produto cadastrado com sucesso!", "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
