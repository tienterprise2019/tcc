﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Provas
{
    public partial class frmAlterar : Form
    {
        public frmAlterar()
        {
            InitializeComponent();
            this.CarregarProvas();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvProvas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            this.CarregarProva();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            this.Alterar();
        }

        public void CarregarProvas()
        {
            try
            {
                Business.ProvasBusiness BSProvas = new Business.ProvasBusiness();
                List<Model.tb_prova> ModelProvas = BSProvas.ConsultarProvas();

                dgvProvas.AutoGenerateColumns = false;
                dgvProvas.DataSource = ModelProvas;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarProva()
        {
            try
            {
                Model.tb_prova ModelProva = dgvProvas.CurrentRow.DataBoundItem as Model.tb_prova;

                txtTmInicio.Text = Convert.ToString(ModelProva.tm_inicial);
                txtTmFinal.Text = Convert.ToString(ModelProva.tm_final);
                dtpData.Value = ModelProva.dt_prova;
                cboTpProva.Text = ModelProva.tp_prova;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Alterar()
        {
            try
            {
                Model.tb_prova ModelProva = dgvProvas.CurrentRow.DataBoundItem as Model.tb_prova;
                ModelProva.tm_inicial = TimeSpan.Parse(txtTmInicio.Text);
                ModelProva.tm_final = TimeSpan.Parse(txtTmFinal.Text);
                ModelProva.dt_prova = dtpData.Value;
                ModelProva.tp_prova = cboTpProva.Text;

                Business.ProvasBusiness BSProva = new Business.ProvasBusiness();
                BSProva.AlterarProvas(ModelProva);

                MessageBox.Show("Prova alterada com sucesso!", "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
