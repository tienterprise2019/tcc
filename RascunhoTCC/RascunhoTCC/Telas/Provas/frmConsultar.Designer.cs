﻿namespace RascunhoTCC.Telas.Provas
{
    partial class frmConsultar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConsultar));
            this.txtAluno = new System.Windows.Forms.TextBox();
            this.dgvProvas = new System.Windows.Forms.DataGridView();
            this.id_prova = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_funcionario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_carro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tm_inicial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tm_final = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dt_prova = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tp_prova = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.lblFechar = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.lblRelogio1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProvas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel11.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtAluno
            // 
            this.txtAluno.Location = new System.Drawing.Point(185, 125);
            this.txtAluno.Name = "txtAluno";
            this.txtAluno.Size = new System.Drawing.Size(595, 29);
            this.txtAluno.TabIndex = 1;
            this.txtAluno.TextChanged += new System.EventHandler(this.txtModelo_TextChanged);
            // 
            // dgvProvas
            // 
            this.dgvProvas.AllowUserToAddRows = false;
            this.dgvProvas.AllowUserToDeleteRows = false;
            this.dgvProvas.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.dgvProvas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvProvas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProvas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_prova,
            this.id_cliente,
            this.id_funcionario,
            this.id_carro,
            this.tm_inicial,
            this.tm_final,
            this.dt_prova,
            this.tp_prova});
            this.dgvProvas.Location = new System.Drawing.Point(2, 183);
            this.dgvProvas.Name = "dgvProvas";
            this.dgvProvas.ReadOnly = true;
            this.dgvProvas.Size = new System.Drawing.Size(839, 356);
            this.dgvProvas.TabIndex = 2;
            // 
            // id_prova
            // 
            this.id_prova.DataPropertyName = "id_prova";
            this.id_prova.HeaderText = "Id";
            this.id_prova.Name = "id_prova";
            this.id_prova.ReadOnly = true;
            // 
            // id_cliente
            // 
            this.id_cliente.DataPropertyName = "id_cliente";
            this.id_cliente.HeaderText = "Id. Cliente";
            this.id_cliente.Name = "id_cliente";
            this.id_cliente.ReadOnly = true;
            // 
            // id_funcionario
            // 
            this.id_funcionario.DataPropertyName = "id_funcionario";
            this.id_funcionario.HeaderText = "Id. Funcionário";
            this.id_funcionario.Name = "id_funcionario";
            this.id_funcionario.ReadOnly = true;
            // 
            // id_carro
            // 
            this.id_carro.DataPropertyName = "id_carro";
            this.id_carro.HeaderText = "Id. Carro";
            this.id_carro.Name = "id_carro";
            this.id_carro.ReadOnly = true;
            // 
            // tm_inicial
            // 
            this.tm_inicial.DataPropertyName = "tm_inicial";
            this.tm_inicial.HeaderText = "Hr. Inicial";
            this.tm_inicial.Name = "tm_inicial";
            this.tm_inicial.ReadOnly = true;
            // 
            // tm_final
            // 
            this.tm_final.DataPropertyName = "tm_final";
            this.tm_final.HeaderText = "Hr. Final";
            this.tm_final.Name = "tm_final";
            this.tm_final.ReadOnly = true;
            // 
            // dt_prova
            // 
            this.dt_prova.DataPropertyName = "dt_prova";
            this.dt_prova.HeaderText = "Dt. Prova";
            this.dt_prova.Name = "dt_prova";
            this.dt_prova.ReadOnly = true;
            // 
            // tp_prova
            // 
            this.tp_prova.DataPropertyName = "tp_prova";
            this.tp_prova.HeaderText = "Prova";
            this.tp_prova.Name = "tp_prova";
            this.tp_prova.ReadOnly = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(0, 698);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1280, 1);
            this.panel2.TabIndex = 78;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(515, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(164, 25);
            this.label8.TabIndex = 51;
            this.label8.Text = "Provas- Consultar";
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox6.BackgroundImage")));
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.Location = new System.Drawing.Point(12, 8);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(27, 25);
            this.pictureBox6.TabIndex = 31;
            this.pictureBox6.TabStop = false;
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.BackColor = System.Drawing.Color.Transparent;
            this.lblFechar.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechar.ForeColor = System.Drawing.Color.Transparent;
            this.lblFechar.Location = new System.Drawing.Point(1244, 8);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(24, 25);
            this.lblFechar.TabIndex = 3;
            this.lblFechar.Text = "X";
            this.lblFechar.Click += new System.EventHandler(this.lblFechar_Click);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.White;
            this.panel12.Location = new System.Drawing.Point(0, 44);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(1280, 1);
            this.panel12.TabIndex = 22;
            // 
            // lblRelogio1
            // 
            this.lblRelogio1.AutoSize = true;
            this.lblRelogio1.BackColor = System.Drawing.Color.Transparent;
            this.lblRelogio1.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRelogio1.ForeColor = System.Drawing.Color.White;
            this.lblRelogio1.Location = new System.Drawing.Point(45, 8);
            this.lblRelogio1.Name = "lblRelogio1";
            this.lblRelogio1.Size = new System.Drawing.Size(88, 25);
            this.lblRelogio1.TabIndex = 2;
            this.lblRelogio1.Text = "00:00:00";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(87, 125);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(27, 25);
            this.pictureBox1.TabIndex = 36;
            this.pictureBox1.TabStop = false;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.White;
            this.panel9.Location = new System.Drawing.Point(187, 562);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(500, 1);
            this.panel9.TabIndex = 29;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel1.Controls.Add(this.dgvProvas);
            this.panel1.Controls.Add(this.txtAluno);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel9);
            this.panel1.Location = new System.Drawing.Point(217, 45);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(842, 663);
            this.panel1.TabIndex = 80;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(122, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 21);
            this.label2.TabIndex = 2;
            this.label2.Text = "Aluno:";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(26, 160);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(793, 1);
            this.panel3.TabIndex = 35;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel11.Controls.Add(this.label8);
            this.panel11.Controls.Add(this.pictureBox6);
            this.panel11.Controls.Add(this.lblFechar);
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Controls.Add(this.lblRelogio1);
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1280, 47);
            this.panel11.TabIndex = 79;
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel30.Location = new System.Drawing.Point(0, 698);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(1280, 22);
            this.panel30.TabIndex = 81;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmConsultar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1280, 720);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel30);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmConsultar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consultar | Carros";
            ((System.ComponentModel.ISupportInitialize)(this.dgvProvas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvProvas;
        private System.Windows.Forms.TextBox txtAluno;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_prova;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_cliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_funcionario;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_carro;
        private System.Windows.Forms.DataGridViewTextBoxColumn tm_inicial;
        private System.Windows.Forms.DataGridViewTextBoxColumn tm_final;
        private System.Windows.Forms.DataGridViewTextBoxColumn dt_prova;
        private System.Windows.Forms.DataGridViewTextBoxColumn tp_prova;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label lblFechar;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label lblRelogio1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Timer timer1;
    }
}