﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Provas
{
    public partial class frmCadastrar : Form
    {
        public frmCadastrar()
        {
            InitializeComponent();
            this.CarregarClientes();
            this.CarregarFuncionarios();
            this.CarregarModelos();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            this.Cadastrar();
        }

        public void CarregarClientes()
        {
            try
            {
                Business.ClientesBusiness BSCliente = new Business.ClientesBusiness();
                List<Model.tb_cliente> ModelClientes = BSCliente.ConsultarClientes();

                cboCliente.DisplayMember = nameof(Model.tb_cliente.nm_cliente);
                cboCliente.DataSource = ModelClientes;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarFuncionarios()
        {
            try
            {
                Business.FuncionariosBusiness BSFuncionario = new Business.FuncionariosBusiness();
                List<Model.tb_funcionario> ModelFuncionarios = BSFuncionario.ConsultarFuncionarios();

                cboFuncionario.DisplayMember = nameof(Model.tb_funcionario.nm_funcionario);
                cboFuncionario.DataSource = ModelFuncionarios;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarModelos()
        {
            try
            {
                Business.CarrosBusiness BSCarro = new Business.CarrosBusiness();
                List<Model.tb_carro> ModelCarros = BSCarro.ConsultarCarros();

                cboCarro.DisplayMember = nameof(Model.tb_carro.nm_modelo);
                cboCarro.DataSource = ModelCarros;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Cadastrar()
        {
            try
            {
                Model.tb_cliente ModelCliente = cboCliente.SelectedItem as Model.tb_cliente;
                Model.tb_funcionario ModelFuncionario = cboFuncionario.SelectedItem as Model.tb_funcionario;
                Model.tb_carro ModelCarro = cboCarro.SelectedItem as Model.tb_carro;

                Model.tb_prova ModelProva = new Model.tb_prova();
                ModelProva.id_cliente = ModelCliente.id_cliente;
                ModelProva.id_funcionario = ModelFuncionario.id_funcionario;
                ModelProva.id_carro = ModelCarro.id_carro;
                ModelProva.tm_inicial = TimeSpan.Parse(txtTmInicio.Text);
                ModelProva.tm_final = TimeSpan.Parse(txtTmFinal.Text);
                ModelProva.dt_prova = dtpData.Value;
                ModelProva.tp_prova = cboTpProva.Text;

                Business.ProvasBusiness BSProva = new Business.ProvasBusiness();
                BSProva.CadastrarProvas(ModelProva);

                MessageBox.Show("Prova cadastrada com sucesso!", "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
