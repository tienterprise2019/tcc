﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Provas
{
    public partial class frmRemover : Form
    {
        public frmRemover()
        {
            InitializeComponent();
            this.CarregarProvas();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRemover_Click(object sender, EventArgs e)
        {
            this.Remover();
        }

        public void CarregarProvas()
        {
            try
            {
                Business.ProvasBusiness BSProva = new Business.ProvasBusiness();
                List<Model.tb_prova> ModelProvas = BSProva.ConsultarProvas();

                dgvProvas.AutoGenerateColumns = false;
                dgvProvas.DataSource = ModelProvas;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Remover()
        {
            try
            {
                Model.tb_prova ModelProva = dgvProvas.CurrentRow.DataBoundItem as Model.tb_prova;

                Business.ProvasBusiness BSProva = new Business.ProvasBusiness();
                BSProva.RemoverProvas(ModelProva.id_prova);

                MessageBox.Show("Prova removido com sucesso!", "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.CarregarProvas();

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
