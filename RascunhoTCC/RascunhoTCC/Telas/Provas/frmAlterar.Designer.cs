﻿namespace RascunhoTCC.Telas.Provas
{
    partial class frmAlterar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAlterar));
            this.dgvProvas = new System.Windows.Forms.DataGridView();
            this.id_prova = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_funcionario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_carro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tm_inicial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tm_final = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dt_prova = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tp_prova = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cboTpProva = new System.Windows.Forms.ComboBox();
            this.dtpData = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTmFinal = new System.Windows.Forms.MaskedTextBox();
            this.txtTmInicio = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel9 = new System.Windows.Forms.Panel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.lblRelogio1 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.lblFechar = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel33 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.button13 = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProvas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.panel11.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel32.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvProvas
            // 
            this.dgvProvas.AllowUserToAddRows = false;
            this.dgvProvas.AllowUserToDeleteRows = false;
            this.dgvProvas.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.dgvProvas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvProvas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProvas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_prova,
            this.id_cliente,
            this.id_funcionario,
            this.id_carro,
            this.tm_inicial,
            this.tm_final,
            this.dt_prova,
            this.tp_prova});
            this.dgvProvas.Location = new System.Drawing.Point(406, 229);
            this.dgvProvas.Name = "dgvProvas";
            this.dgvProvas.ReadOnly = true;
            this.dgvProvas.Size = new System.Drawing.Size(843, 393);
            this.dgvProvas.TabIndex = 1;
            this.dgvProvas.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProvas_CellContentClick);
            // 
            // id_prova
            // 
            this.id_prova.DataPropertyName = "id_prova";
            this.id_prova.HeaderText = "Id";
            this.id_prova.Name = "id_prova";
            this.id_prova.ReadOnly = true;
            // 
            // id_cliente
            // 
            this.id_cliente.DataPropertyName = "id_cliente";
            this.id_cliente.HeaderText = "Id. Cliente";
            this.id_cliente.Name = "id_cliente";
            this.id_cliente.ReadOnly = true;
            // 
            // id_funcionario
            // 
            this.id_funcionario.DataPropertyName = "id_funcionario";
            this.id_funcionario.HeaderText = "Id. Funcionário";
            this.id_funcionario.Name = "id_funcionario";
            this.id_funcionario.ReadOnly = true;
            // 
            // id_carro
            // 
            this.id_carro.DataPropertyName = "id_carro";
            this.id_carro.HeaderText = "Id. Carro";
            this.id_carro.Name = "id_carro";
            this.id_carro.ReadOnly = true;
            // 
            // tm_inicial
            // 
            this.tm_inicial.DataPropertyName = "tm_inicial";
            this.tm_inicial.HeaderText = "Hr. Inicial";
            this.tm_inicial.Name = "tm_inicial";
            this.tm_inicial.ReadOnly = true;
            // 
            // tm_final
            // 
            this.tm_final.DataPropertyName = "tm_final";
            this.tm_final.HeaderText = "Hr. Final";
            this.tm_final.Name = "tm_final";
            this.tm_final.ReadOnly = true;
            // 
            // dt_prova
            // 
            this.dt_prova.DataPropertyName = "dt_prova";
            this.dt_prova.HeaderText = "Dt. Prova";
            this.dt_prova.Name = "dt_prova";
            this.dt_prova.ReadOnly = true;
            // 
            // tp_prova
            // 
            this.tp_prova.DataPropertyName = "tp_prova";
            this.tp_prova.HeaderText = "Prova";
            this.tp_prova.Name = "tp_prova";
            this.tp_prova.ReadOnly = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(46, 382);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 21);
            this.label7.TabIndex = 28;
            this.label7.Text = "Prova:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(50, 299);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 21);
            this.label5.TabIndex = 27;
            this.label5.Text = "Data:";
            // 
            // cboTpProva
            // 
            this.cboTpProva.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTpProva.FormattingEnabled = true;
            this.cboTpProva.Items.AddRange(new object[] {
            "Teórica",
            "Prática"});
            this.cboTpProva.Location = new System.Drawing.Point(120, 379);
            this.cboTpProva.Name = "cboTpProva";
            this.cboTpProva.Size = new System.Drawing.Size(224, 29);
            this.cboTpProva.TabIndex = 5;
            // 
            // dtpData
            // 
            this.dtpData.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpData.Location = new System.Drawing.Point(120, 293);
            this.dtpData.Name = "dtpData";
            this.dtpData.Size = new System.Drawing.Size(224, 29);
            this.dtpData.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(49, 213);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 21);
            this.label4.TabIndex = 26;
            this.label4.Text = "Final:";
            // 
            // txtTmFinal
            // 
            this.txtTmFinal.Location = new System.Drawing.Point(120, 210);
            this.txtTmFinal.Mask = "00:00";
            this.txtTmFinal.Name = "txtTmFinal";
            this.txtTmFinal.Size = new System.Drawing.Size(224, 29);
            this.txtTmFinal.TabIndex = 3;
            // 
            // txtTmInicio
            // 
            this.txtTmInicio.Location = new System.Drawing.Point(120, 123);
            this.txtTmInicio.Mask = "00:00";
            this.txtTmInicio.Name = "txtTmInicio";
            this.txtTmInicio.Size = new System.Drawing.Size(224, 29);
            this.txtTmInicio.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(47, 126);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 21);
            this.label6.TabIndex = 25;
            this.label6.Text = "Início:";
            // 
            // btnAlterar
            // 
            this.btnAlterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar.ForeColor = System.Drawing.Color.White;
            this.btnAlterar.Location = new System.Drawing.Point(34, 571);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(302, 70);
            this.btnAlterar.TabIndex = 6;
            this.btnAlterar.Text = "Alterar";
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.White;
            this.panel9.ForeColor = System.Drawing.Color.White;
            this.panel9.Location = new System.Drawing.Point(1, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(905, 1);
            this.panel9.TabIndex = 71;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox6.BackgroundImage")));
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.Location = new System.Drawing.Point(12, 10);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(27, 25);
            this.pictureBox6.TabIndex = 31;
            this.pictureBox6.TabStop = false;
            // 
            // lblRelogio1
            // 
            this.lblRelogio1.AutoSize = true;
            this.lblRelogio1.BackColor = System.Drawing.Color.Transparent;
            this.lblRelogio1.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRelogio1.ForeColor = System.Drawing.Color.White;
            this.lblRelogio1.Location = new System.Drawing.Point(45, 10);
            this.lblRelogio1.Name = "lblRelogio1";
            this.lblRelogio1.Size = new System.Drawing.Size(88, 25);
            this.lblRelogio1.TabIndex = 2;
            this.lblRelogio1.Text = "00:00:00";
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel11.Controls.Add(this.label14);
            this.panel11.Controls.Add(this.pictureBox6);
            this.panel11.Controls.Add(this.lblFechar);
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Controls.Add(this.lblRelogio1);
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1280, 47);
            this.panel11.TabIndex = 73;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(554, 9);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(146, 25);
            this.label14.TabIndex = 51;
            this.label14.Text = "Provas - Alterar";
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.BackColor = System.Drawing.Color.Transparent;
            this.lblFechar.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechar.ForeColor = System.Drawing.Color.Transparent;
            this.lblFechar.Location = new System.Drawing.Point(1244, 10);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(24, 25);
            this.lblFechar.TabIndex = 7;
            this.lblFechar.Text = "X";
            this.lblFechar.Click += new System.EventHandler(this.lblFechar_Click);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.White;
            this.panel12.Location = new System.Drawing.Point(0, 46);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(1280, 1);
            this.panel12.TabIndex = 22;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.White;
            this.panel7.ForeColor = System.Drawing.Color.White;
            this.panel7.Location = new System.Drawing.Point(538, 32);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(550, 1);
            this.panel7.TabIndex = 72;
            // 
            // panel33
            // 
            this.panel33.BackColor = System.Drawing.Color.White;
            this.panel33.Location = new System.Drawing.Point(375, 46);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(1, 674);
            this.panel33.TabIndex = 76;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel13);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.cboTpProva);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.btnAlterar);
            this.panel1.Controls.Add(this.dtpData);
            this.panel1.Controls.Add(this.txtTmInicio);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtTmFinal);
            this.panel1.Location = new System.Drawing.Point(-1, 46);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(377, 675);
            this.panel1.TabIndex = 71;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.Location = new System.Drawing.Point(27, 414);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(330, 1);
            this.panel6.TabIndex = 78;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.Location = new System.Drawing.Point(27, 328);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(330, 1);
            this.panel5.TabIndex = 77;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.White;
            this.panel13.Location = new System.Drawing.Point(27, 245);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(330, 1);
            this.panel13.TabIndex = 76;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Location = new System.Drawing.Point(27, 158);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(330, 1);
            this.panel4.TabIndex = 75;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(87, 40);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 1);
            this.panel2.TabIndex = 74;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(139, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(96, 21);
            this.label13.TabIndex = 73;
            this.label13.Text = "Informações";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.ForeColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(0, 37);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(905, 1);
            this.panel3.TabIndex = 73;
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.panel30.Controls.Add(this.panel3);
            this.panel30.Controls.Add(this.panel9);
            this.panel30.Controls.Add(this.panel32);
            this.panel30.Location = new System.Drawing.Point(376, 682);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(905, 39);
            this.panel30.TabIndex = 74;
            // 
            // panel32
            // 
            this.panel32.BackColor = System.Drawing.Color.Red;
            this.panel32.Controls.Add(this.button13);
            this.panel32.Location = new System.Drawing.Point(1243, 12);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(25, 25);
            this.panel32.TabIndex = 0;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.Red;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Segoe UI Symbol", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.ForeColor = System.Drawing.Color.White;
            this.button13.Location = new System.Drawing.Point(-6, -7);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(38, 34);
            this.button13.TabIndex = 0;
            this.button13.Text = "X";
            this.button13.UseVisualStyleBackColor = false;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.White;
            this.panel8.ForeColor = System.Drawing.Color.White;
            this.panel8.Location = new System.Drawing.Point(376, 699);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(905, 1);
            this.panel8.TabIndex = 75;
            // 
            // frmAlterar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1280, 720);
            this.Controls.Add(this.dgvProvas);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel33);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel30);
            this.Controls.Add(this.panel8);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmAlterar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alterar | Aulas";
            ((System.ComponentModel.ISupportInitialize)(this.dgvProvas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel30.ResumeLayout(false);
            this.panel32.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cboTpProva;
        private System.Windows.Forms.DateTimePicker dtpData;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox txtTmFinal;
        private System.Windows.Forms.MaskedTextBox txtTmInicio;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dgvProvas;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_prova;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_cliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_funcionario;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_carro;
        private System.Windows.Forms.DataGridViewTextBoxColumn tm_inicial;
        private System.Windows.Forms.DataGridViewTextBoxColumn tm_final;
        private System.Windows.Forms.DataGridViewTextBoxColumn dt_prova;
        private System.Windows.Forms.DataGridViewTextBoxColumn tp_prova;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label lblRelogio1;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblFechar;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Panel panel8;
    }
}