﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Fornecedores
{
    public partial class frmCadastrar : Form
    {
        public frmCadastrar()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtCep_Leave(object sender, EventArgs e)
        {
            this.Buscar(txtCep.Text);
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            this.Cadastrar();
        }

        public void Cadastrar()
        {
            try
            {
                Model.tb_fornecedor ModelFornecedor = new Model.tb_fornecedor();
                ModelFornecedor.nm_fornecedor = txtNome.Text;
                ModelFornecedor.ds_telefone = txtTelefone.Text;
                ModelFornecedor.ds_email = txtEmail.Text;
                ModelFornecedor.ds_cnpj = txtCnpj.Text;
                ModelFornecedor.ds_cep = txtCep.Text;
                ModelFornecedor.nm_rua = txtRua.Text;
                ModelFornecedor.nr_numero = Convert.ToInt32(txtNumero.Text);
                ModelFornecedor.nm_bairro = txtBairro.Text;
                ModelFornecedor.nm_cidade = txtCidade.Text;

                Business.FornecedoresBusiness BSFornecedor = new Business.FornecedoresBusiness();
                BSFornecedor.CadastrarFornecedores(ModelFornecedor);

                MessageBox.Show("Fornecedor cadastrado com sucesso!", "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Buscar(string cep)
        {
            try
            {
                // Cria objeto responsável por conversar com uma API
                System.Net.WebClient rest = new System.Net.WebClient();
                rest.Encoding = Encoding.UTF8;

                // Chama API do correio, concatenando o cep
                string resposta = rest.DownloadString("https://viacep.com.br/ws/" + cep + "/json");

                // Transforma a resposta do correio em DTO
                dynamic r = Newtonsoft.Json.JsonConvert.DeserializeObject(resposta);

                txtRua.Text = r.logradouro;
                txtBairro.Text = r.bairro;
                txtCidade.Text = r.localidade;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
