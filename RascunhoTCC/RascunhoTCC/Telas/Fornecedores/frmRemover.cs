﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Fornecedores
{
    public partial class frmRemover : Form
    {
        public frmRemover()
        {
            InitializeComponent();
            this.CarregarFornecedores();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRemover_Click(object sender, EventArgs e)
        {
            this.Remover();
        }

        public void CarregarFornecedores()
        {
            try
            {
                Business.FornecedoresBusiness BSFornecedor = new Business.FornecedoresBusiness();
                List<Model.tb_fornecedor> ModelFornecedores = BSFornecedor.ConsultarFornecedores();

                dgvFornecedores.AutoGenerateColumns = false;
                dgvFornecedores.DataSource = ModelFornecedores;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Remover()
        {
            try
            {
                Model.tb_fornecedor ModelFornecedor = dgvFornecedores.CurrentRow.DataBoundItem as Model.tb_fornecedor;

                Business.FornecedoresBusiness BSFornecedor = new Business.FornecedoresBusiness();
                BSFornecedor.RemoverFornecedores(ModelFornecedor.id_fornecedor);

                MessageBox.Show("Fornecedores removido com sucesso!", "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.CarregarFornecedores();

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
