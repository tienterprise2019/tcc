﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Fornecedores
{
    public partial class frmAlterar : Form
    {
        public frmAlterar()
        {
            InitializeComponent();
            this.CarregarFornecedores();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cboFornecedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.CarregarFornecedor();
        }

        private void txtCep_Leave(object sender, EventArgs e)
        {
            this.Buscar(txtCep.Text);
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            this.Alterar();
        }

        public void CarregarFornecedores()
        {
            try
            {
                Business.FornecedoresBusiness BSFornecedores = new Business.FornecedoresBusiness();
                List<Model.tb_fornecedor> ModelFornecedores = BSFornecedores.ConsultarFornecedores();

                cboFornecedor.DisplayMember = nameof(Model.tb_fornecedor.nm_fornecedor);
                cboFornecedor.DataSource = ModelFornecedores;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void CarregarFornecedor()
        {
            try
            {
                Model.tb_fornecedor ModelFornecedor = cboFornecedor.SelectedItem as Model.tb_fornecedor;

                txtTelefone.Text = ModelFornecedor.ds_telefone;
                txtEmail.Text = ModelFornecedor.ds_email;
                txtCnpj.Text = ModelFornecedor.ds_cnpj;
                txtCep.Text = ModelFornecedor.ds_cep;
                txtNumero.Text = Convert.ToString(ModelFornecedor.nr_numero);
                txtRua.Text = ModelFornecedor.nm_rua;
                txtBairro.Text = ModelFornecedor.nm_bairro;
                txtCidade.Text = ModelFornecedor.nm_cidade;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void Alterar()
        {
            try
            {
                Model.tb_fornecedor ModelFornecedor = cboFornecedor.SelectedItem as Model.tb_fornecedor;
                ModelFornecedor.ds_telefone = txtTelefone.Text;
                ModelFornecedor.ds_email = txtEmail.Text;
                ModelFornecedor.ds_cnpj = txtCnpj.Text;
                ModelFornecedor.ds_cep = txtCep.Text;
                ModelFornecedor.nm_rua = txtRua.Text;
                ModelFornecedor.nr_numero = Convert.ToInt32(txtNumero.Text);
                ModelFornecedor.nm_bairro = txtBairro.Text;
                ModelFornecedor.nm_cidade = txtCidade.Text;

                Business.FornecedoresBusiness BSFornecedores = new Business.FornecedoresBusiness();
                BSFornecedores.AlterarFornecedores(ModelFornecedor);

                MessageBox.Show("Fornecedores alterado com sucesso!", "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Buscar(string cep)
        {
            try
            {
                // Cria objeto responsável por conversar com uma API
                System.Net.WebClient rest = new System.Net.WebClient();
                rest.Encoding = Encoding.UTF8;

                // Chama API do correio, concatenando o cep
                string resposta = rest.DownloadString("https://viacep.com.br/ws/" + cep + "/json");

                // Transforma a resposta do correio em DTO
                dynamic r = Newtonsoft.Json.JsonConvert.DeserializeObject(resposta);

                txtRua.Text = r.logradouro;
                txtBairro.Text = r.bairro;
                txtCidade.Text = r.localidade;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
