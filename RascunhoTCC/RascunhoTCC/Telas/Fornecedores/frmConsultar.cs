﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Fornecedores
{
    public partial class frmConsultar : Form
    {
        public frmConsultar()
        {
            InitializeComponent();
            this.CarregarFornecedores();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtFornecedor_TextChanged(object sender, EventArgs e)
        {
            this.CarregarPorNomeFornecedores();
        }

        public void CarregarFornecedores()
        {
            try
            {
                Business.FornecedoresBusiness BSFornecedor = new Business.FornecedoresBusiness();
                List<Model.tb_fornecedor> ModelFornecedores = BSFornecedor.ConsultarFornecedores();

                dgvFornecedores.AutoGenerateColumns = false;
                dgvFornecedores.DataSource = ModelFornecedores;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarPorNomeFornecedores()
        {
            try
            {
                string fornecedor = txtFornecedor.Text;

                Business.FornecedoresBusiness BSFornecedor = new Business.FornecedoresBusiness();
                List<Model.tb_fornecedor> ModelFornecedores = BSFornecedor.ConsultarPorNomeFornecedores(fornecedor);

                dgvFornecedores.AutoGenerateColumns = false;
                dgvFornecedores.DataSource = ModelFornecedores;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
