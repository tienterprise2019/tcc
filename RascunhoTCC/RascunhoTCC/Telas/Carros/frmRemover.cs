﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Carros
{
    public partial class frmRemover : Form
    {
        public frmRemover()
        {
            InitializeComponent();
            this.CarregarModelos();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRemover_Click(object sender, EventArgs e)
        {
            this.Remover();
        }

        public void CarregarModelos()
        {
            try
            {
                Business.CarrosBusiness BSCarro = new Business.CarrosBusiness();
                List<Model.tb_carro> ModelCarros = BSCarro.ConsultarCarros();

                dgvCarros.AutoGenerateColumns = false;
                dgvCarros.DataSource = ModelCarros;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Remover()
        {
            try
            {
                Model.tb_carro ModelCarro = dgvCarros.CurrentRow.DataBoundItem as Model.tb_carro;

                Business.CarrosBusiness BSCarro = new Business.CarrosBusiness();
                BSCarro.RemoverCarros(ModelCarro.id_carro);

                MessageBox.Show("Carro removido com sucesso!", "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.CarregarModelos();

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
