﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Carros
{
    public partial class frmCadastrar : Form
    {
        public frmCadastrar()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            this.Cadastrar();
        }

        public void Cadastrar()
        {
            try
            {
                Model.tb_carro ModelCarro = new Model.tb_carro();
                ModelCarro.nm_modelo = txtModelo.Text;
                ModelCarro.ds_placa = txtPlaca.Text;
                ModelCarro.ds_ano = dtpAno.Value;
                ModelCarro.km_inicial = Convert.ToInt32(nudKmInicial.Value);

                if (rdnNao1.Checked)
                {
                    ModelCarro.vc_adaptado = false;
                }
                else if (rdnSim1.Checked)
                {
                    ModelCarro.vc_adaptado = true;
                }

                if (rdnNao2.Checked)
                {
                    ModelCarro.tp_disponivel = false;
                }
                else if (rdnSim2.Checked)
                {
                    ModelCarro.tp_disponivel = true;
                }

                Business.CarrosBusiness BSCarro = new Business.CarrosBusiness();
                BSCarro.CadastrarCarros(ModelCarro);

                MessageBox.Show("Carro cadastrado com sucesso!", "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
