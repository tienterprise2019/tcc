﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Carros
{
    public partial class frmAlterar : Form
    {
        public frmAlterar()
        {
            InitializeComponent();
            this.CarregarModelos();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cboModelo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.CarregarModelo();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            this.Alterar();
        }

        public void CarregarModelos()
        {
            try
            {
                Business.CarrosBusiness BSCarros = new Business.CarrosBusiness();
                List<Model.tb_carro> ModelCarros = BSCarros.ConsultarCarros();

                cboModelo.DisplayMember = nameof(Model.tb_carro.nm_modelo);
                cboModelo.DataSource = ModelCarros;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarModelo()
        {
            try
            {
                Model.tb_carro ModelCarro = cboModelo.SelectedItem as Model.tb_carro;

                dtpAno.Value = ModelCarro.ds_ano;
                txtPlaca.Text = ModelCarro.ds_placa;
                nudKmInicial.Value = ModelCarro.km_inicial;
                nudKmFinal.Value = ModelCarro.km_final;

                if (ModelCarro.tp_disponivel == false)
                {
                    rdnNao.Checked = true;
                }
                else if (ModelCarro.tp_disponivel == true)
                {
                    rdnSim.Checked = true;
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Alterar()
        {
            try
            {
                Model.tb_carro ModelCarro = cboModelo.SelectedItem as Model.tb_carro;
                ModelCarro.ds_ano = dtpAno.Value;
                ModelCarro.ds_placa = txtPlaca.Text;
                ModelCarro.km_inicial = Convert.ToInt32(nudKmInicial.Value);
                ModelCarro.km_final = Convert.ToInt32(nudKmFinal.Value);

                if (rdnNao.Checked)
                {
                    ModelCarro.tp_disponivel = false;
                }
                else if (rdnSim.Checked)
                {
                    ModelCarro.tp_disponivel = true;
                }

                Business.CarrosBusiness BSCarros = new Business.CarrosBusiness();
                BSCarros.AlterarCarros(ModelCarro);

                MessageBox.Show("Carro alterado com sucesso!", "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Carros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
