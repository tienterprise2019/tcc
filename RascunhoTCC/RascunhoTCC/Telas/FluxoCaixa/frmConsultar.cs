﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.FluxoCaixa
{
    public partial class frmConsultar : Form
    {
        public frmConsultar()
        {
            InitializeComponent();
            this.CarregarFluxosCaixas();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Fluxo de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Fluxo de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtpData_ValueChanged(object sender, EventArgs e)
        {
            this.CarregarFluxoCaixa();
        }

        public void CarregarFluxosCaixas()
        {
            try
            {
                Business.FluxoCaixaBusiness BSFluxosCaixas = new Business.FluxoCaixaBusiness();
                List<Model.vw_fluxo_caixa> ModelFluxosCaixas = BSFluxosCaixas.ConsultarFluxosCaixas();

                dgvFluxoCaixa.AutoGenerateColumns = false;
                dgvFluxoCaixa.DataSource = ModelFluxosCaixas;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Fluxo de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Fluxo de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarFluxoCaixa()
        {
            try
            {
                DateTime data = dtpData.Value;

                Business.FluxoCaixaBusiness BSFluxosCaixas = new Business.FluxoCaixaBusiness();
                List<Model.vw_fluxo_caixa> ModelFluxosCaixas = BSFluxosCaixas.ConsultarPorData(data);

                dgvFluxoCaixa.AutoGenerateColumns = false;
                dgvFluxoCaixa.DataSource = ModelFluxosCaixas;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Fluxo de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Fluxo de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Fluxo de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Fluxo de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
