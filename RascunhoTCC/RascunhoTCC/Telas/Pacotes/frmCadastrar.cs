﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Pacotes
{
    public partial class frmCadastrar : Form
    {
        public frmCadastrar()
        {
            InitializeComponent();
            this.CarregarProvas();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            this.Cadastrar();
        }

        public void CarregarProvas()
        {
            try
            {
                Business.ProvasBusiness BSProva = new Business.ProvasBusiness();
                List<Model.tb_prova> ModelProvas = BSProva.ConsultarProvas();

                dgvProvas.AutoGenerateColumns = false;
                dgvProvas.DataSource = ModelProvas;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Provas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Cadastrar()
        {
            try
            {
                Model.tb_prova ModelProva = dgvProvas.CurrentRow.DataBoundItem as Model.tb_prova;
                Model.tb_pacote ModelPacote = new Model.tb_pacote();
                ModelPacote.id_cliente = ModelProva.id_cliente;
                ModelPacote.id_prova = ModelProva.id_prova;
                ModelPacote.qt_teorica = Convert.ToInt32(nudQtTeorica.Value);
                ModelPacote.qt_pratica = Convert.ToInt32(nudQtPratica.Value);
                ModelPacote.vl_preco = nudPreco.Value;
                ModelPacote.dt_venda = dtpData.Value;

                if(rdnA.Checked)
                {
                    ModelPacote.tp_carteira = "A";
                }
                else if (rdnB.Checked)
                {
                    ModelPacote.tp_carteira = "B";
                }
                else if (rdnD.Checked)
                {
                    ModelPacote.tp_carteira = "D";
                }
                else if (rdnAB.Checked)
                {
                    ModelPacote.tp_carteira = "AB";
                }

                Business.PacotesBusiness BSPacote = new Business.PacotesBusiness();
                BSPacote.CadastrarPacotes(ModelPacote);

                MessageBox.Show("Pacote cadastrado com sucesso!", "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
