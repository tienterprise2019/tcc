﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Pacotes
{
    public partial class frmConsultar : Form
    {
        public frmConsultar()
        {
            InitializeComponent();
            this.CarregarPacotes();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtModelo_TextChanged(object sender, EventArgs e)
        {
            this.CarregarPorNomeAlunos();
        }

        public void CarregarPacotes()
        {
            try
            {
                Business.PacotesBusiness BSPacote = new Business.PacotesBusiness();
                List<Model.tb_pacote> ModelPacotes = BSPacote.ConsultarPacotes();

                dgvPacotes.AutoGenerateColumns = false;
                dgvPacotes.DataSource = ModelPacotes;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarPorNomeAlunos()
        {
            try
            {
                string aluno = txtAluno.Text;

                Business.PacotesBusiness BSPacote = new Business.PacotesBusiness();
                List<Model.tb_pacote> ModelPacotes = BSPacote.ConsultarPorNomeAlunos(aluno);

                dgvPacotes.AutoGenerateColumns = false;
                dgvPacotes.DataSource = ModelPacotes;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
