﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas.Pacotes
{
    public partial class frmAlterar : Form
    {
        public frmAlterar()
        {
            InitializeComponent();
            this.CarregarPacotes();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvPacotes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            this.CarregarPacote();
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            this.Alterar();
        }

        public void CarregarPacotes()
        {
            try
            {
                Business.PacotesBusiness BSPacotes = new Business.PacotesBusiness();
                List<Model.tb_pacote> ModelPacotes = BSPacotes.ConsultarPacotes();

                dgvPacotes.DataSource = ModelPacotes;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarPacote()
        {
            try
            {
                Model.tb_pacote ModelPacote = dgvPacotes.CurrentRow.DataBoundItem as Model.tb_pacote;

                nudQtTeorica.Value = ModelPacote.qt_teorica;
                nudQtPratica.Value = ModelPacote.qt_pratica;
                nudPreco.Value = ModelPacote.vl_preco;
                dtpData.Value = ModelPacote.dt_venda;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Alterar()
        {
            try
            {
                Model.tb_pacote ModelPacote = dgvPacotes.CurrentRow.DataBoundItem as Model.tb_pacote;
                ModelPacote.qt_teorica = Convert.ToInt32(nudQtTeorica.Value);
                ModelPacote.qt_pratica = Convert.ToInt32(nudQtPratica.Value);
                ModelPacote.vl_preco = nudPreco.Value;
                ModelPacote.dt_venda = dtpData.Value;

                Business.PacotesBusiness BSPacote = new Business.PacotesBusiness();
                BSPacote.AlterarPacotes(ModelPacote);

                MessageBox.Show("Pacote alterado com sucesso!", "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Pacotes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
