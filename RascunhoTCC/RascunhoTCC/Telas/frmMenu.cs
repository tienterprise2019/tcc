﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void movepanel(Control btn)
        {
            try
            {
                panel38.Location = btn.Location;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAbast_Click(object sender, EventArgs e)
        {
            try
            {
                panel38.Visible = true;
                movepanel(btnAbast);

                pnlAbast.Visible = true;
                pnlAula.Visible = false;
                pnlCarro.Visible = false;
                pnlProva.Visible = false;
                pnlProd.Visible = false;
                pnlPact.Visible = false;
                pnlJornada.Visible = false;
                pnlHoler.Visible = false;
                pnlFunc.Visible = false;
                pnlCliente.Visible = false;
                pnlForn.Visible = false;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAula_Click(object sender, EventArgs e)
        {
            try
            {
                panel38.Visible = true;
                movepanel(btnAula);

                pnlAbast.Visible = false;
                pnlAula.Visible = true;
                pnlCarro.Visible = false;
                pnlProva.Visible = false;
                pnlProd.Visible = false;
                pnlPact.Visible = false;
                pnlJornada.Visible = false;
                pnlHoler.Visible = false;
                pnlFunc.Visible = false;
                pnlCliente.Visible = false;
                pnlForn.Visible = false;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCarro_Click(object sender, EventArgs e)
        {
            try
            {
                panel38.Visible = true;
                movepanel(btnCarro);

                pnlAbast.Visible = false;
                pnlAula.Visible = false;
                pnlCarro.Visible = true;
                pnlProva.Visible = false;
                pnlProd.Visible = false;
                pnlPact.Visible = false;
                pnlJornada.Visible = false;
                pnlHoler.Visible = false;
                pnlFunc.Visible = false;
                pnlCliente.Visible = false;
                pnlForn.Visible = false;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            try
            {
                panel38.Visible = true;
                movepanel(btnCliente);

                pnlAbast.Visible = false;
                pnlAula.Visible = false;
                pnlCarro.Visible = false;
                pnlProva.Visible = false;
                pnlProd.Visible = false;
                pnlPact.Visible = false;
                pnlJornada.Visible = false;
                pnlHoler.Visible = false;
                pnlFunc.Visible = false;
                pnlCliente.Visible = true;
                pnlForn.Visible = false;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnForn_Click(object sender, EventArgs e)
        {
            try
            {
                panel38.Visible = true;
                movepanel(btnForn);

                pnlAbast.Visible = false;
                pnlAula.Visible = false;
                pnlCarro.Visible = false;
                pnlProva.Visible = false;
                pnlProd.Visible = false;
                pnlPact.Visible = false;
                pnlJornada.Visible = false;
                pnlHoler.Visible = false;
                pnlFunc.Visible = false;
                pnlCliente.Visible = false;
                pnlForn.Visible = true;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFunc_Click(object sender, EventArgs e)
        {
            try
            {
                panel38.Visible = true;
                movepanel(btnFunc);

                pnlAbast.Visible = false;
                pnlAula.Visible = false;
                pnlCarro.Visible = false;
                pnlProva.Visible = false;
                pnlProd.Visible = false;
                pnlPact.Visible = false;
                pnlJornada.Visible = false;
                pnlHoler.Visible = false;
                pnlFunc.Visible = true;
                pnlCliente.Visible = false;
                pnlForn.Visible = false;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnHolerite_Click(object sender, EventArgs e)
        {
            try
            {
                panel38.Visible = true;
                movepanel(btnHolerite);

                pnlAbast.Visible = false;
                pnlAula.Visible = false;
                pnlCarro.Visible = false;
                pnlProva.Visible = false;
                pnlProd.Visible = false;
                pnlPact.Visible = false;
                pnlJornada.Visible = false;
                pnlHoler.Visible = true;
                pnlFunc.Visible = false;
                pnlCliente.Visible = false;
                pnlForn.Visible = false;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnJornada_Click(object sender, EventArgs e)
        {
            try
            {
                panel38.Visible = true;
                movepanel(btnJornada);

                pnlAbast.Visible = false;
                pnlAula.Visible = false;
                pnlCarro.Visible = false;
                pnlProva.Visible = false;
                pnlProd.Visible = false;
                pnlPact.Visible = false;
                pnlJornada.Visible = true;
                pnlHoler.Visible = false;
                pnlFunc.Visible = false;
                pnlCliente.Visible = false;
                pnlForn.Visible = false;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPacotes_Click(object sender, EventArgs e)
        {
            try
            {
                panel38.Visible = true;
                movepanel(btnPacotes);

                pnlAbast.Visible = false;
                pnlAula.Visible = false;
                pnlCarro.Visible = false;
                pnlProva.Visible = false;
                pnlProd.Visible = false;
                pnlPact.Visible = true;
                pnlJornada.Visible = false;
                pnlHoler.Visible = false;
                pnlFunc.Visible = false;
                pnlCliente.Visible = false;
                pnlForn.Visible = false;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnProdt_Click(object sender, EventArgs e)
        {
            try
            {
                panel38.Visible = true;
                movepanel(btnProdt);

                pnlAbast.Visible = false;
                pnlAula.Visible = false;
                pnlCarro.Visible = false;
                pnlProva.Visible = false;
                pnlProd.Visible = true;
                pnlPact.Visible = false;
                pnlJornada.Visible = false;
                pnlHoler.Visible = false;
                pnlFunc.Visible = false;
                pnlCliente.Visible = false;
                pnlForn.Visible = false;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnProv_Click(object sender, EventArgs e)
        {
            try
            {
                panel38.Visible = true;
                movepanel(btnProv);

                pnlAbast.Visible = false;
                pnlAula.Visible = false;
                pnlCarro.Visible = false;
                pnlProva.Visible = true;
                pnlProd.Visible = false;
                pnlPact.Visible = false;
                pnlJornada.Visible = false;
                pnlHoler.Visible = false;
                pnlFunc.Visible = false;
                pnlCliente.Visible = false;
                pnlForn.Visible = false;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void panel2_Click(object sender, EventArgs e)
        {
            try
            {
                pnlAbast.Visible = false;
                pnlAula.Visible = false;
                pnlCarro.Visible = false;
                pnlProva.Visible = false;
                pnlProd.Visible = false;
                pnlPact.Visible = false;
                pnlJornada.Visible = false;
                pnlHoler.Visible = false;
                pnlFunc.Visible = false;
                pnlCliente.Visible = false;
                pnlForn.Visible = false;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Menu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCadastrar1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Abastecimentos.frmCadastrar tela = new Abastecimentos.frmCadastrar();
            tela.Show();
        }

        private void btnConsultar1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Abastecimentos.frmConsultar tela = new Abastecimentos.frmConsultar();
            tela.Show();
        }

        private void btnAlterar1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Abastecimentos.frmAlterar tela = new Abastecimentos.frmAlterar();
            tela.Show();
        }

        private void btnRemover1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Abastecimentos.frmRemover tela = new Abastecimentos.frmRemover();
            tela.Show();
        }

        private void btnCadastrar2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Aulas.frmCadastrar tela = new Aulas.frmCadastrar();
            tela.Show();
        }

        private void btnConsultar2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Aulas.frmConsultar tela = new Aulas.frmConsultar();
            tela.Show();
        }

        private void btnAlterar2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Aulas.frmAlterar tela = new Aulas.frmAlterar();
            tela.Show();
        }

        private void btnRemover2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Aulas.frmRemover tela = new Aulas.frmRemover();
            tela.Show();
        }

        private void btnCadastrar3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Carros.frmCadastrar tela = new Carros.frmCadastrar();
            tela.Show();
        }

        private void btnConsultar3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Carros.frmConsultar tela = new Carros.frmConsultar();
            tela.Show();
        }

        private void btnAlterar3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Carros.frmAlterar tela = new Carros.frmAlterar();
            tela.Show();
        }

        private void btnRemover3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Carros.frmRemover tela = new Carros.frmRemover();
            tela.Show();
        }

        private void btnCadastrar4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Clientes.frmCadastrar tela = new Clientes.frmCadastrar();
            tela.Show();
        }

        private void btnConsultar4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Clientes.frmConsultar tela = new Clientes.frmConsultar();
            tela.Show();
        }

        private void btnAlterar4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Clientes.frmAlterar tela = new Clientes.frmAlterar();
            tela.Show();
        }

        private void btnRemover4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Clientes.frmRemover tela = new Clientes.frmRemover();
            tela.Show();
        }

        private void btnCadastrar5_Click(object sender, EventArgs e)
        {
            this.Hide();
            Fornecedores.frmCadastrar tela = new Fornecedores.frmCadastrar();
            tela.Show();
        }

        private void btnConsultar5_Click(object sender, EventArgs e)
        {
            this.Hide();
            Fornecedores.frmConsultar tela = new Fornecedores.frmConsultar();
            tela.Show();
        }

        private void btnAlterar5_Click(object sender, EventArgs e)
        {
            this.Hide();
            Fornecedores.frmAlterar tela = new Fornecedores.frmAlterar();
            tela.Show();
        }

        private void btnRemover5_Click(object sender, EventArgs e)
        {
            this.Hide();
            Fornecedores.frmRemover tela = new Fornecedores.frmRemover();
            tela.Show();
        }

        private void btnCadastrar6_Click(object sender, EventArgs e)
        {
            this.Hide();
            Funcionarios.frmCadastrar tela = new Funcionarios.frmCadastrar();
            tela.Show();
        }

        private void btnConsultar6_Click(object sender, EventArgs e)
        {
            this.Hide();
            Funcionarios.frmConsultar tela = new Funcionarios.frmConsultar();
            tela.Show();
        }

        private void btnAlterar6_Click(object sender, EventArgs e)
        {
            this.Hide();
            Funcionarios.frmAlterar tela = new Funcionarios.frmAlterar();
            tela.Show();
        }

        private void btnRemover6_Click(object sender, EventArgs e)
        {
            this.Hide();
            Funcionarios.frmRemover tela = new Funcionarios.frmRemover();
            tela.Show();
        }

        private void btnCadastrar7_Click(object sender, EventArgs e)
        {
            this.Hide();
            Holerites.frmCadastrar tela = new Holerites.frmCadastrar();
            tela.Show();
        }

        private void btnConsultar7_Click(object sender, EventArgs e)
        {
            this.Hide();
            Holerites.frmConsultar tela = new Holerites.frmConsultar();
            tela.Show();
        }

        private void btnAlterar7_Click(object sender, EventArgs e)
        {
            this.Hide();
            Holerites.frmAlterar tela = new Holerites.frmAlterar();
            tela.Show();
        }

        private void btnRemover7_Click(object sender, EventArgs e)
        {
            this.Hide();
            Holerites.frmRemover tela = new Holerites.frmRemover();
            tela.Show();
        }

        private void btnCadastrar8_Click(object sender, EventArgs e)
        {
            this.Hide();
            JornadasTrabalhos.frmCadastrar tela = new JornadasTrabalhos.frmCadastrar();
            tela.Show();
        }

        private void btnConsultar8_Click(object sender, EventArgs e)
        {
            this.Hide();
            JornadasTrabalhos.frmConsultar tela = new JornadasTrabalhos.frmConsultar();
            tela.Show();
        }

        private void btnAlterar8_Click(object sender, EventArgs e)
        {
            this.Hide();
            JornadasTrabalhos.frmAlterar tela = new JornadasTrabalhos.frmAlterar();
            tela.Show();
        }

        private void btnRemover8_Click(object sender, EventArgs e)
        {
            this.Hide();
            JornadasTrabalhos.frmRemover tela = new JornadasTrabalhos.frmRemover();
            tela.Show();
        }

        private void btnCadastrar9_Click(object sender, EventArgs e)
        {
            this.Hide();
            Pacotes.frmCadastrar tela = new Pacotes.frmCadastrar();
            tela.Show();
        }

        private void btnConsultar9_Click(object sender, EventArgs e)
        {
            this.Hide();
            Pacotes.frmConsultar tela = new Pacotes.frmConsultar();
            tela.Show();
        }

        private void btnAlterar9_Click(object sender, EventArgs e)
        {
            this.Hide();
            Pacotes.frmAlterar tela = new Pacotes.frmAlterar();
            tela.Show();
        }

        private void btnRemover9_Click(object sender, EventArgs e)
        {
            this.Hide();
            Pacotes.frmRemover tela = new Pacotes.frmRemover();
            tela.Show();
        }

        private void btnCadastrar10_Click(object sender, EventArgs e)
        {
            this.Hide();
            Produtos.frmCadastrar tela = new Produtos.frmCadastrar();
            tela.Show();
        }

        private void btnConsultar10_Click(object sender, EventArgs e)
        {
            this.Hide();
            Produtos.frmConsultar tela = new Produtos.frmConsultar();
            tela.Show();
        }

        private void btnAlterar10_Click(object sender, EventArgs e)
        {
            this.Hide();
            Produtos.frmAlterar tela = new Produtos.frmAlterar();
            tela.Show();
        }

        private void btnRemover10_Click(object sender, EventArgs e)
        {
            this.Hide();
            Produtos.frmRemover tela = new Produtos.frmRemover();
            tela.Show();
        }

        private void btnCadastrar11_Click(object sender, EventArgs e)
        {
            this.Hide();
            Provas.frmCadastrar tela = new Provas.frmCadastrar();
            tela.Show();
        }

        private void btnConsultar11_Click(object sender, EventArgs e)
        {
            this.Hide();
            Provas.frmConsultar tela = new Provas.frmConsultar();
            tela.Show();
        }

        private void btnAlterar11_Click(object sender, EventArgs e)
        {
            this.Hide();
            Provas.frmAlterar tela = new Provas.frmAlterar();
            tela.Show();
        }

        private void btnRemover11_Click(object sender, EventArgs e)
        {
            this.Hide();
            Provas.frmRemover tela = new Provas.frmRemover();
            tela.Show();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }
    }
}
