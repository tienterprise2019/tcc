﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            this.Login();
        }

        private void txtEmail_Enter(object sender, EventArgs e)
        {
            try
            {
                if (txtEmail.Text == "exemplo@email.com")
                {
                    txtEmail.Text = "";
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Login", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Login", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtSenha_Enter(object sender, EventArgs e)
        {
            try
            {
                if (txtSenha.Text == "000000000")
                {
                    txtSenha.Text = "";
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Login", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Login", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Login()
        {
            try
            {
                Model.tb_login LoginModel = new Model.tb_login();
                LoginModel.ds_email = txtEmail.Text;
                LoginModel.ds_senha = txtSenha.Text;

                Business.LoginBusiness LB = new Business.LoginBusiness();
                Model.tb_login loginMD = LB.Login(LoginModel);

                if (loginMD != null)
                {
                    Model.UsuarioLogado.ID = loginMD.id_login;
                    Model.UsuarioLogado.Email = loginMD.ds_email;

                    Telas.frmMenu tela = new Telas.frmMenu();
                    tela.Show();

                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Credenciais inválidas.", "Future Drivers Auto Escola | Login", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Login", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Login", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                Application.Exit();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Login", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Login", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
