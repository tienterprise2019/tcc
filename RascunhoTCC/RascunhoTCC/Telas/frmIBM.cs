﻿using IBM.Cloud.SDK.Core.Authentication.Iam;
using IBM.Watson.SpeechToText.v1;
using IBM.Watson.TextToSpeech.v1;
using Stannieman.AudioPlayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RascunhoTCC.Telas
{
    public partial class frmIBM : Form
    {
        public frmIBM()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblRelogio1.Text = DateTime.Now.ToString("HH:mm:ss");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | IBM", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | IBM", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnOuvir_Click(object sender, EventArgs e)
        {
            string texto = txtFrase.Text;

            this.FalarTextoDigitado(texto);
        }

        private async void FalarTextoDigitado(string texto)
        {
            try
            {
                IamAuthenticator authenticator = new IamAuthenticator(
                    apikey: "RZhe_kdTdv66WEmKFaq1UeaD8gc_6nvk9ltssTpBqDkc");

                TextToSpeechService service = new TextToSpeechService(authenticator);
                service.SetServiceUrl("https://stream.watsonplatform.net/text-to-speech/api");

                var result = service.Synthesize(
                    text: texto,
                    accept: "audio/wav",
                    voice: "pt-BR_IsabelaVoice"
                    );

                using (FileStream fs = File.Create("futuredrivers_texto_voz.wav"))
                {
                    result.Result.WriteTo(fs);
                    fs.Close();
                    result.Result.Close();
                }

                AudioPlayer player = new AudioPlayer();
                await player.SetFileAsync("futuredrivers_texto_voz.wav", "futuredrivers_texto_voz.wav");
                await player.PlayAsync();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Texto para Voz", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Texto para Voz", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                mciSendString("open new Type waveaudio alias meuaudio", null, 0, IntPtr.Zero);
                mciSendString("record meuaudio", null, 0, IntPtr.Zero);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Voz para Texto", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Voz para Texto", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnParar_Click(object sender, EventArgs e)
        {
            try
            {
                mciSendString("save meuaudio futuredrivers_voz_texto.wav", null, 0, IntPtr.Zero);
                mciSendString("close meuaudio", null, 0, IntPtr.Zero);

                IamAuthenticator authenticator = new IamAuthenticator(
                    apikey: "_xcfZrYOBBybvBN92EPKZufW3Z6aZFdp2FEDDU7UUWVl");

                SpeechToTextService service = new SpeechToTextService(authenticator);
                service.SetServiceUrl("https://stream.watsonplatform.net/speech-to-text/api");

                var result = service.Recognize(
                    audio: File.ReadAllBytes("futuredrivers_voz_texto.wav"),
                    contentType: "audio/wav",
                    model: "pt-BR_NarrowbandModel"
                    );

                string textoIbm = result.Result.Results.First().Alternatives.First().Transcript;

                lblTexto.Text = textoIbm;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | Voz para Texto", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | Voz para Texto", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        [System.Runtime.InteropServices.DllImport("winmm.dll")]
        private static extern long mciSendString(string comando, StringBuilder sb, int length, IntPtr cb);

        private void lblFechar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Future Drivers Auto Escola | IBM", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde!", "Future Drivers Auto Escola | IBM", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
