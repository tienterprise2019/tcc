﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Business
{
    class ClientesBusiness
    {
        public void CadastrarClientes(Model.tb_cliente ModelCliente)
        {
            if (ModelCliente.nm_cliente == string.Empty)
            {
                throw new ArgumentException("Nome do cliente não pode ser vazio");
            }

            if (ModelCliente.ds_cpf == string.Empty)
            {
                throw new ArgumentException("CPF não pode ser vazio");
            }

            if (ModelCliente.ds_telefone == string.Empty)
            {
                throw new ArgumentException("Telefone não pode ser vazio");
            }

            if (ModelCliente.ds_celular == string.Empty)
            {
                throw new ArgumentException("Celular não pode ser vazio");
            }

            if (ModelCliente.ds_email == string.Empty)
            {
                throw new ArgumentException("Email não pode ser vazio");
            }

            if (ModelCliente.ds_cep == string.Empty)
            {
                throw new ArgumentException("CEP não pode ser vazio");
            }

            if (ModelCliente.nr_numero == 0)
            {
                throw new ArgumentException("Número não pode ser zero");
            }

            if (ModelCliente.nm_rua == string.Empty)
            {
                throw new ArgumentException("Rua não pode ser vazio");
            }

            if (ModelCliente.nm_bairro == string.Empty)
            {
                throw new ArgumentException("Bairro não pode ser vazio");
            }

            if (ModelCliente.nm_cidade == string.Empty)
            {
                throw new ArgumentException("Cidade não pode ser vazio");
            }
            if (ModelCliente.tp_deficiente == null)
            {
                throw new ArgumentException("Deve maracar se tem deficiencia");
            }

            Database.ClientesDatabase DBCliente = new Database.ClientesDatabase();
            DBCliente.CadastrarClientes(ModelCliente);
        }

        public List<Model.tb_cliente> ConsultarClientes()
        {
            Database.ClientesDatabase DBCliente = new Database.ClientesDatabase();
            List<Model.tb_cliente> ModelClientes = DBCliente.ConsultarClientes();

            return ModelClientes;
        }


        public void AlterarClientes(Model.tb_cliente ModelCliente)
        {
            if (ModelCliente.id_cliente == 0)
            {
                throw new ArgumentException("Nome do cliente não pode ser vazio");
            }

            if (ModelCliente.ds_cpf == string.Empty)
            {
                throw new ArgumentException("CPF não pode ser vazio");
            }

            if (ModelCliente.ds_telefone == string.Empty)
            {
                throw new ArgumentException("Telefone não pode ser vazio");
            }

            if (ModelCliente.ds_celular == string.Empty)
            {
                throw new ArgumentException("Celular não pode ser vazio");
            }

            if (ModelCliente.ds_email == string.Empty)
            {
                throw new ArgumentException("Email não pode ser vazio");
            }

            if (ModelCliente.ds_cep == string.Empty)
            {
                throw new ArgumentException("CEP não pode ser vazio");
            }

            if (ModelCliente.nr_numero == 0)
            {
                throw new ArgumentException("Número não pode ser zero");
            }

            if (ModelCliente.nm_rua == string.Empty)
            {
                throw new ArgumentException("Rua não pode ser vazio");
            }

            if (ModelCliente.nm_bairro == string.Empty)
            {
                throw new ArgumentException("Bairro não pode ser vazio");
            }

            if (ModelCliente.nm_cidade == string.Empty)
            {
                throw new ArgumentException("Cidade não pode ser vazio");
            }
            if (ModelCliente.tp_deficiente == null)
            {
                throw new ArgumentException("Deve maracar se tem deficiencia");
            }

            Database.ClientesDatabase DBCliente = new Database.ClientesDatabase();
            DBCliente.AlterarClientes(ModelCliente);
        }

        public void RemoverClientes(int id)
        {
            Database.ClientesDatabase DBCliente = new Database.ClientesDatabase();
            DBCliente.RemoverClientes(id);
        }

        public List<Model.tb_cliente> ConsultarPorNomeClientes(string cliente)
        {
            Database.ClientesDatabase DBCliente = new Database.ClientesDatabase();
            List<Model.tb_cliente> ModelClientes = DBCliente.ConsultarPorNomeClientes(cliente);

            return ModelClientes;
        }
    }
}
