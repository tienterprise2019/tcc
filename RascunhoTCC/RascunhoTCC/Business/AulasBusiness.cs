﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Business
{
    class AulasBusiness
    {
        public void CadastrarAulas(Model.tb_aula ModelAula)
        {
            if(ModelAula.tp_aula == null)
            {
                throw new ArgumentException("Selecione o tipo de aula");
            }

            if (ModelAula.tp_carteira == null)
            {
                throw new ArgumentException("Selecione o tipo de carteira");
            }

            if (ModelAula.id_cliente == 0)
            {
                throw new ArgumentException("Selecione o cliente");
            }

            if (ModelAula.id_instrutor == 0)
            {
                throw new ArgumentException("Selecione o instrutor");
            }

            if (ModelAula.vl_preco == 0)
            {
                throw new ArgumentException("Valor naõ pode ser zero");
            }

            if (ModelAula.hr_saida == TimeSpan.Zero)
            {
                throw new ArgumentException("Saída não pode estar vazio");
            }

            Database.AulasDatabase DBAula = new Database.AulasDatabase();
            DBAula.CadastrarAulas(ModelAula);
        }

        public List<Model.tb_aula> ConsultarAulas()
        {
            Database.AulasDatabase DBAula = new Database.AulasDatabase();
            List<Model.tb_aula> ModelAulas = DBAula.ConsultarAulas();

            return ModelAulas;
        }


        public void AlterarAulas(Model.tb_aula ModelAula)
        {
            if (ModelAula.tp_aula == null)
            {
                throw new ArgumentException("Selecione o tipo de aula");
            }

            if (ModelAula.tp_carteira == null)
            {
                throw new ArgumentException("Selecione o tipo de carteira");
            }

            if (ModelAula.id_cliente == 0)
            {
                throw new ArgumentException("Selecione o cliente");
            }

            if (ModelAula.id_instrutor == 0)
            {
                throw new ArgumentException("Selecione o instrutor");
            }

            if (ModelAula.vl_preco == 0)
            {
                throw new ArgumentException("Valor naõ pode ser zero");
            }

            if (ModelAula.hr_saida == TimeSpan.Zero)
            {
                throw new ArgumentException("Saída nao pode estar vazio");
            }

            if (ModelAula.id_carro == 0)
            {
                throw new ArgumentException("Selecione o carro");
            }

            if (ModelAula.km_final == 0)
            {
                throw new ArgumentException("KM final não pode ser zero");
            }


            Database.AulasDatabase DBAula = new Database.AulasDatabase();
            DBAula.AlterarAulas(ModelAula);
        }

        public void RemoverAulas(int id)
        {
            Database.AulasDatabase DBAula = new Database.AulasDatabase();
            DBAula.RemoverAulas(id);
        }

        public List<Model.tb_aula> ConsultarPorNomeClientes(string cliente)
        {
            Database.AulasDatabase DBAula = new Database.AulasDatabase();
            List<Model.tb_aula> ModelAulas = DBAula.ConsultarPorNomeClientes(cliente);

            return ModelAulas;
        }
    }
}
