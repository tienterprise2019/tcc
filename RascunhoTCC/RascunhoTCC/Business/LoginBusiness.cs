﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Business
{
    class LoginBusiness
    {
        public Model.tb_login Login(Model.tb_login LoginModel)
        {
            Database.LoginDatabase loginDB = new Database.LoginDatabase();
            Model.tb_login loginMD = loginDB.Login(LoginModel);

            return loginMD;
        }

        public void CadastroLogin(Model.tb_login LoginModel)
        {
            if (LoginModel.ds_email == string.Empty)
            {
                throw new ArgumentException("E-mail é obrigatório");
            }

            if (LoginModel.ds_senha == string.Empty)
            {
                throw new ArgumentException("Senha é obrigatório");
            }

            Database.LoginDatabase loginDatabase = new Database.LoginDatabase();
            loginDatabase.CadastroLogin(LoginModel);
        }
    }
}
