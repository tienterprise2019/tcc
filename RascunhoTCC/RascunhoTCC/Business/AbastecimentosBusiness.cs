﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Business
{
    class AbastecimentosBusiness
    {
        
        public void CadastrarAbastecimentos(Model.tb_abastecimento ModelAbastecimento)
        {
            if (ModelAbastecimento.tp_combustivel == string.Empty)
            {
                throw new ArgumentException("Combustível não pode estar vazio");
            }

            if (ModelAbastecimento.vl_preco == 0)
            {
                throw new ArgumentException("Preço não pode ser zero");
            }

            if (ModelAbastecimento.nr_litros == 0)
            {
                throw new ArgumentException("Litros não pode ser zero");
            }

            if (ModelAbastecimento.nm_local == string.Empty)
            {
                throw new ArgumentException("Local não pode estar vazio");
            }


            Database.AbastecimentosDatabase DBAbastecimento = new Database.AbastecimentosDatabase();
            DBAbastecimento.CadastrarAbastecimentos(ModelAbastecimento);

        }

        public List<Model.tb_abastecimento> ConsultarAbastecimentos()
        {
            Database.AbastecimentosDatabase DBAbastecimento = new Database.AbastecimentosDatabase();
            List<Model.tb_abastecimento> ModelAbastecimentos = DBAbastecimento.ConsultarAbastecimentos();

            return ModelAbastecimentos;
        }


        public void AlterarAbastecimentos(Model.tb_abastecimento ModelAbastecimento)
        {
            if (ModelAbastecimento.tp_combustivel == string.Empty)
            {
                throw new ArgumentException("Combustível não pode estar vazio");
            }

            if (ModelAbastecimento.vl_preco == 0)
            {
                throw new ArgumentException("Preço não pode ser zero");
            }

            if (ModelAbastecimento.nr_litros == 0)
            {
                throw new ArgumentException("Litros não pode ser zero");
            }

            if (ModelAbastecimento.nm_local == string.Empty)
            {
                throw new ArgumentException("Local não pode estar vazio");
            }

            Database.AbastecimentosDatabase DBAbastecimento = new Database.AbastecimentosDatabase();
            DBAbastecimento.AlterarAbastecimentos(ModelAbastecimento);
        }

        public void RemoverAbastecimentos(int id)
        {
            Database.AbastecimentosDatabase DBAbastecimento = new Database.AbastecimentosDatabase();
            DBAbastecimento.RemoverAbastecimentos(id);
        }

        public List<Model.tb_abastecimento> ConsultarPorModelos(string modelo)
        {
            Database.AbastecimentosDatabase DBAbastecimento = new Database.AbastecimentosDatabase();
            List<Model.tb_abastecimento> ModelAbastecimentos = DBAbastecimento.ConsultarPorModelos(modelo);

            return ModelAbastecimentos;
        }

    }
}
