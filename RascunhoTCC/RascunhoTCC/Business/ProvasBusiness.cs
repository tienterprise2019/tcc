﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Business
{
    class ProvasBusiness
    {
        public void CadastrarProvas(Model.tb_prova ModelProva)
        {
            if (ModelProva.id_cliente == 0)
            {
                throw new ArgumentException("Cliente não pode ser vazio");
            }

            if (ModelProva.id_funcionario == 0)
            {
                throw new ArgumentException("Funcionário não pode ser vazio");
            }

            if (ModelProva.id_carro == 0)
            {
                throw new ArgumentException("Cliente não pode ser vazio");
            }

            if (ModelProva.tm_inicial == TimeSpan.Zero)
            {
                throw new ArgumentException("Horário de início não pode ser vazio");
            }

            if (ModelProva.tm_final == TimeSpan.Zero)
            {
                throw new ArgumentException("Horário de término não pode ser vazio");
            }

            if (ModelProva.tp_prova == string.Empty)
            {
                throw new ArgumentException("Tipo de prova não pode ser vazio");
            }

            Database.ProvasDatabase DBProva = new Database.ProvasDatabase();
            DBProva.CadastrarProvas(ModelProva);
        }

        public List<Model.tb_prova> ConsultarProvas()
        {
            Database.ProvasDatabase DBProva = new Database.ProvasDatabase();
            List<Model.tb_prova> ModelProvas = DBProva.ConsultarProvas();

            return ModelProvas;
        }


        public void AlterarProvas(Model.tb_prova ModelProva)
        {
            if (ModelProva.tm_inicial == TimeSpan.Zero)
            {
                throw new ArgumentException("Horário de início não pode ser vazio");
            }

            if (ModelProva.tm_final == TimeSpan.Zero)
            {
                throw new ArgumentException("Horário de término não pode ser vazio");
            }

            if (ModelProva.tp_prova == string.Empty)
            {
                throw new ArgumentException("Tipo de prova não pode ser vazio");
            }

            Database.ProvasDatabase DBProva = new Database.ProvasDatabase();
            DBProva.AlterarProvas(ModelProva);
        }

        public void RemoverProvas(int id)
        {
            Database.ProvasDatabase DBProva = new Database.ProvasDatabase();
            DBProva.RemoverProvas(id);
        }

        public List<Model.tb_prova> ConsultarPorNomeAlunos(string aluno)
        {
            Database.ProvasDatabase DBProva = new Database.ProvasDatabase();
            List<Model.tb_prova> ModelProvas = DBProva.ConsultarPorNomeAlunos(aluno);

            return ModelProvas;
        }
    }
}
