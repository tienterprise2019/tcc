﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Business
{
    class FluxoCaixaBusiness
    {
        public List<Model.vw_fluxo_caixa> ConsultarFluxosCaixas()
        {
            Database.FluxoCaixaDatabase DBFluxosCaixas = new Database.FluxoCaixaDatabase();
            List<Model.vw_fluxo_caixa> ModelFluxosCaixas = DBFluxosCaixas.ConsultarFluxosCaixas();

            return ModelFluxosCaixas;
        }

        public List<Model.vw_fluxo_caixa> ConsultarPorData(DateTime data)
        {
            Database.FluxoCaixaDatabase DBFluxosCaixas = new Database.FluxoCaixaDatabase();
            List<Model.vw_fluxo_caixa> ModelFluxosCaixas = DBFluxosCaixas.ConsultarPorData(data);

            return ModelFluxosCaixas;
        }
    }
}
