﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Business
{
    class JornadasTrabalhosBusiness
    {
        public void CadastrarJornadasTrabalhos(Model.tb_jornada_trabalho ModelJornadasTrabalhos)
        {
           

            if ( ModelJornadasTrabalhos.hr_entrada  == TimeSpan.Zero)
            {
                throw new ArgumentException("Horário de entrada não pode estar vazio");
            }

            if (ModelJornadasTrabalhos.hr_saida_interv == TimeSpan.Zero)
            {
                throw new ArgumentException("Horário da saída do intervalo não pode estar vazio");
            }

            if (ModelJornadasTrabalhos.hr_retorno_intrv == TimeSpan.Zero)
            {
                throw new ArgumentException("Horário do retorno do intervalo não pode estar vazio");
            }


            if (ModelJornadasTrabalhos.hr_saida == TimeSpan.Zero)
            {
                throw new ArgumentException("Horário de saída não pode estar vazio");
            }

            Database.JornadasTrabalhosDatabase DBJornadaTrabalho = new Database.JornadasTrabalhosDatabase();
            DBJornadaTrabalho.CadastrarJornadasTrabalhos(ModelJornadasTrabalhos);
        }

        public List<Model.tb_jornada_trabalho> ConsultarJornadasTrabalhos()
        {
            Database.JornadasTrabalhosDatabase DBJornadaTrabalho = new Database.JornadasTrabalhosDatabase();
            List<Model.tb_jornada_trabalho> ModelJornadasTrabalhos = DBJornadaTrabalho.ConsultarJornadasTrabalhos();

            return ModelJornadasTrabalhos;
        }


        public void AlterarJornadasTrabalhos(Model.tb_jornada_trabalho ModelJornadaTrabalho)
        {
            if (ModelJornadaTrabalho.id_funcionario == 0)
            {
                throw new ArgumentException("Funcionário não pode estar vazio");
            }

            if (ModelJornadaTrabalho.hr_entrada == TimeSpan.Zero)
            {
                throw new ArgumentException("Horário de entrada não pode estar vazio");
            }

            if (ModelJornadaTrabalho.hr_saida_interv == TimeSpan.Zero)
            {
                throw new ArgumentException("Horário da saída do intervalo não pode estar vazio");
            }

            if (ModelJornadaTrabalho.hr_retorno_intrv == TimeSpan.Zero)
            {
                throw new ArgumentException("Horário do retorno do intervalo não pode estar vazio");
            }


            if (ModelJornadaTrabalho.hr_saida == TimeSpan.Zero)
            {
                throw new ArgumentException("Horário de saída não pode estar vazio");
            }

            Database.JornadasTrabalhosDatabase DBJornadaTrabalho = new Database.JornadasTrabalhosDatabase();
            DBJornadaTrabalho.AlterarJornadasTrabalhos(ModelJornadaTrabalho);
        }

        public void RemoverJornadasTrabalhos(int id)
        {
            Database.JornadasTrabalhosDatabase DBJornadaTrabalho = new Database.JornadasTrabalhosDatabase();
            DBJornadaTrabalho.RemoverJornadasTrabalhos(id);
        }

        public List<Model.tb_jornada_trabalho> ConsultarPorNomeFuncionario(string funcionario)
        {
            Database.JornadasTrabalhosDatabase DBJornadaTrabalho = new Database.JornadasTrabalhosDatabase();
            List<Model.tb_jornada_trabalho> ModelJornadasTrabalhos = DBJornadaTrabalho.ConsultarPorNomeFuncionario(funcionario);

            return ModelJornadasTrabalhos;
        }
    }
}
