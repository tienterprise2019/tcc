﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Business
{
    class HoleritesBusiness
    {
        public void CadastrarHolerites(Model.tb_holerite ModelHolerite)
        {
            if (ModelHolerite.vl_bruto == 0)
            {
                throw new ArgumentException("Salário bruto não pode ser zero");
            }

            if (ModelHolerite.vl_fgts == 0)
            {
                throw new ArgumentException("FGTS não pode ser zero");
            }

            if (ModelHolerite.vl_irrf == 0)
            {
                throw new ArgumentException("IRRF não pode ser zero");
            }

            if (ModelHolerite.sl_liquido == 0)
            {
                throw new ArgumentException("Salário líquido não pode ser zero");
            }

            if (ModelHolerite.vl_decimo_terceiro == 0)
            {
                throw new ArgumentException("Décimo terceiro não pode ser zero");
            }

            if (ModelHolerite.tp_beneficio == null)
            {
                throw new ArgumentException("Marque os tipos de benefício");
            }

            if (ModelHolerite.sl_periculosidade == null)
            {
                throw new ArgumentException("Defina salário de periculosidade");
            }

            Database.HoleritesDatabase DBHolerite = new Database.HoleritesDatabase();
            DBHolerite.CadastrarHolerites(ModelHolerite);
        }

        public List<Model.tb_holerite> ConsultarHolerites()
        {
            Database.HoleritesDatabase DBHolerite = new Database.HoleritesDatabase();
            List<Model.tb_holerite> ModelHolerites = DBHolerite.ConsultarHolerites();

            return ModelHolerites;
        }


        public void AlterarHolerites(Model.tb_holerite ModelHolerite)
        {
            if (ModelHolerite.vl_bruto == 0)
            {
                throw new ArgumentException("Salário bruto não pode ser zero");
            }

            if (ModelHolerite.vl_fgts == 0)
            {
                throw new ArgumentException("FGTS não pode ser zero");
            }

            if (ModelHolerite.vl_irrf == 0)
            {
                throw new ArgumentException("IRRF não pode ser zero");
            }

            if (ModelHolerite.sl_liquido == 0)
            {
                throw new ArgumentException("Salário líquido não pode ser zero");
            }

            if (ModelHolerite.vl_decimo_terceiro == 0)
            {
                throw new ArgumentException("Décimo terceiro não pode ser zero");
            }

            if (ModelHolerite.sl_periculosidade == null)
            {
                throw new ArgumentException("Defina salário de periculosidade");
            }

            Database.HoleritesDatabase DBHolerite = new Database.HoleritesDatabase();
            DBHolerite.AlterarHolerites(ModelHolerite);
        }

        public void RemoverHolerites(int id)
        {
            Database.HoleritesDatabase DBHolerite = new Database.HoleritesDatabase();
            DBHolerite.RemoverHolerites(id);
        }

        public List<Model.tb_holerite> ConsultarPorNomeHolerites(string funcionario)
        {
            Database.HoleritesDatabase DBHolerite = new Database.HoleritesDatabase();
            List<Model.tb_holerite> ModelHolerites = DBHolerite.ConsultarPorNomeHolerites(funcionario);

            return ModelHolerites;
        }
    }
}
