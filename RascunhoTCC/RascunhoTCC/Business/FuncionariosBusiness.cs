﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Business
{
    class FuncionariosBusiness
    {
        public void CadastrarFuncionarios(Model.tb_funcionario ModelFuncionario)
        {
            if (ModelFuncionario.nm_funcionario == string.Empty)
            {
                throw new ArgumentException("Nome do funcionário não pode ser vazio");
            }

            if (ModelFuncionario.ds_cpf == string.Empty)
            {
                throw new ArgumentException("CPF não pode ser vazio");
            }

            if (ModelFuncionario.ds_telefone == string.Empty)
            {
                throw new ArgumentException("Telefone não pode ser vazio");
            }

            if (ModelFuncionario.ds_celular == string.Empty)
            {
                throw new ArgumentException("Celular não pode ser vazio");
            }

            if (ModelFuncionario.ds_email == string.Empty)
            {
                throw new ArgumentException("Email não pode ser vazio");
            }

            if (ModelFuncionario.nr_numero == 0)
            {
                throw new ArgumentException("Número não pode ser zero");
            }

            if (ModelFuncionario.nm_rua == string.Empty)
            {
                throw new ArgumentException("Rua não pode ser vazio");
            }

            if (ModelFuncionario.nm_bairro == string.Empty)
            {
                throw new ArgumentException("Bairro não pode ser vazio");
            }

            if (ModelFuncionario.nm_cidade == string.Empty)
            {
                throw new ArgumentException("Cidade não pode ser vazio");
            }

            if (ModelFuncionario.ds_cpf == string.Empty)
            {
                throw new ArgumentException("CPF não pode ser vazio");
            }

            if (ModelFuncionario.ds_cargo == string.Empty)
            {
                throw new ArgumentException("Cargo não pode ser vazio");
            }

            if (ModelFuncionario.hr_entrada == TimeSpan.Zero)
            {
                throw new ArgumentException("Hora de entrada não pode ser vazio");
            }

            if (ModelFuncionario.hr_saida == TimeSpan.Zero)
            {
                throw new ArgumentException("Hora de saída não pode ser vazio");
            }

            Database.FuncionariosDatabase DBFuncionario = new Database.FuncionariosDatabase();
            DBFuncionario.CadastrarFuncionarios(ModelFuncionario);
        }

        public List<Model.tb_funcionario> ConsultarFuncionarios()
        {
            Database.FuncionariosDatabase DBFuncionario = new Database.FuncionariosDatabase();
            List<Model.tb_funcionario> ModelFuncionarios = DBFuncionario.ConsultarFuncionarios();

            return ModelFuncionarios;
        }


        public void AlterarFuncionarios(Model.tb_funcionario ModelFuncionario)
        {
            if (ModelFuncionario.id_funcionario == 0)
            {
                throw new ArgumentException("Nome do funcionário não pode ser vazio");
            }

            if (ModelFuncionario.ds_cpf == string.Empty)
            {
                throw new ArgumentException("CPF não pode ser vazio");
            }

            if (ModelFuncionario.ds_telefone == string.Empty)
            {
                throw new ArgumentException("Telefone não pode ser vazio");
            }

            if (ModelFuncionario.ds_celular == string.Empty)
            {
                throw new ArgumentException("Celular não pode ser vazio");
            }

            if (ModelFuncionario.ds_email == string.Empty)
            {
                throw new ArgumentException("Email não pode ser vazio");
            }

            if (ModelFuncionario.nr_numero == 0)
            {
                throw new ArgumentException("Número não pode ser zero");
            }

            if (ModelFuncionario.nm_rua == string.Empty)
            {
                throw new ArgumentException("Rua não pode ser vazio");
            }

            if (ModelFuncionario.nm_bairro == string.Empty)
            {
                throw new ArgumentException("Bairro não pode ser vazio");
            }

            if (ModelFuncionario.nm_cidade == string.Empty)
            {
                throw new ArgumentException("Cidade não pode ser vazio");
            }

            if (ModelFuncionario.ds_cpf == string.Empty)
            {
                throw new ArgumentException("CPF não pode ser vazio");
            }

            if (ModelFuncionario.ds_cargo == string.Empty)
            {
                throw new ArgumentException("Cargo não pode ser vazio");
            }

            if (ModelFuncionario.hr_entrada == TimeSpan.Zero)
            {
                throw new ArgumentException("Hora de entrada não pode ser vazio");
            }

            if (ModelFuncionario.hr_saida == TimeSpan.Zero)
            {
                throw new ArgumentException("Hora de saída não pode ser vazio");
            }

            Database.FuncionariosDatabase DBFuncionario = new Database.FuncionariosDatabase();
            DBFuncionario.AlterarFuncionarios(ModelFuncionario);
        }

        public void RemoverFuncionarios(int id)
        {
            Database.FuncionariosDatabase DBFuncionario = new Database.FuncionariosDatabase();
            DBFuncionario.RemoverFuncionarios(id);
        }

        public List<Model.tb_funcionario> ConsultarPorNomeFuncionarios(string funcionario)
        {
            Database.FuncionariosDatabase DBFuncionario = new Database.FuncionariosDatabase();
            List<Model.tb_funcionario> ModelFuncionarios = DBFuncionario.ConsultarPorNomeFuncionarios(funcionario);

            return ModelFuncionarios;
        }
    }
}
