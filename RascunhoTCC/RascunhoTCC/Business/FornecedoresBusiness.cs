﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Business
{
    class FornecedoresBusiness
    {
        public void CadastrarFornecedores(Model.tb_fornecedor ModelFornecedor)
        {
            if (ModelFornecedor.nm_fornecedor == string.Empty)
            {
                throw new ArgumentException("Nome do fornecedor não pode ser vazio");
            }

            if (ModelFornecedor.ds_telefone == string.Empty)
            {
                throw new ArgumentException("Telefone não pode ser vazio");
            }

            if (ModelFornecedor.ds_email == string.Empty)
            {
                throw new ArgumentException("Email não pode ser vazio");
            }

            if (ModelFornecedor.ds_cnpj == string.Empty)
            {
                throw new ArgumentException("CNPJ não pode ser vazio");
            }

            if (ModelFornecedor.ds_telefone == string.Empty)
            {
                throw new ArgumentException("Telefone não pode ser vazio");
            }

            if (ModelFornecedor.ds_cep == string.Empty)
            {
                throw new ArgumentException("CEP não pode ser vazio");
            }

            if (ModelFornecedor.nr_numero == 0)
            {
                throw new ArgumentException("Número não pode ser zero");
            }

            if (ModelFornecedor.nm_rua == string.Empty)
            {
                throw new ArgumentException("Rua não pode ser vazio");
            }

            if (ModelFornecedor.nm_bairro == string.Empty)
            {
                throw new ArgumentException("Bairro não pode ser vazio");
            }

            if (ModelFornecedor.nm_cidade == string.Empty)
            {
                throw new ArgumentException("Cidade não pode ser vazio");
            }

            Database.FornecedoresDatabase DBFornecedor = new Database.FornecedoresDatabase();
            DBFornecedor.CadastrarFornecedores(ModelFornecedor);
        }

        public List<Model.tb_fornecedor> ConsultarFornecedores()
        {
            Database.FornecedoresDatabase DBFornecedor = new Database.FornecedoresDatabase();
            List<Model.tb_fornecedor> ModelFornecedores = DBFornecedor.ConsultarFornecedores();

            return ModelFornecedores;
        }


        public void AlterarFornecedores(Model.tb_fornecedor ModelFornecedor)
        {
            if (ModelFornecedor.id_fornecedor == 0)
            {
                throw new ArgumentException("Nome do fornecedor não pode ser vazio");
            }

            if (ModelFornecedor.ds_telefone == string.Empty)
            {
                throw new ArgumentException("Telefone não pode ser vazio");
            }

            if (ModelFornecedor.ds_email == string.Empty)
            {
                throw new ArgumentException("Email não pode ser vazio");
            }

            if (ModelFornecedor.ds_cnpj == string.Empty)
            {
                throw new ArgumentException("CNPJ não pode ser vazio");
            }

            if (ModelFornecedor.ds_telefone == string.Empty)
            {
                throw new ArgumentException("Telefone não pode ser vazio");
            }

            if (ModelFornecedor.ds_cep == string.Empty)
            {
                throw new ArgumentException("CEP não pode ser vazio");
            }

            if (ModelFornecedor.nr_numero == 0)
            {
                throw new ArgumentException("Número não pode ser zero");
            }

            if (ModelFornecedor.nm_rua == string.Empty)
            {
                throw new ArgumentException("Rua não pode ser vazio");
            }

            if (ModelFornecedor.nm_bairro == string.Empty)
            {
                throw new ArgumentException("Bairro não pode ser vazio");
            }

            if (ModelFornecedor.nm_cidade == string.Empty)
            {
                throw new ArgumentException("Cidade não pode ser vazio");
            }

            Database.FornecedoresDatabase DBFornecedor = new Database.FornecedoresDatabase();
            DBFornecedor.AlterarFornecedores(ModelFornecedor);
        }

        public void RemoverFornecedores(int id)
        {
            Database.FornecedoresDatabase DBFornecedor = new Database.FornecedoresDatabase();
            DBFornecedor.RemoverFornecedores(id);
        }

        public List<Model.tb_fornecedor> ConsultarPorNomeFornecedores(string fornecedor)
        {
            Database.FornecedoresDatabase DBFornecedor = new Database.FornecedoresDatabase();
            List<Model.tb_fornecedor> ModelFornecedores = DBFornecedor.ConsultarPorNomeFornecedores(fornecedor);

            return ModelFornecedores;
        }
    }
}
