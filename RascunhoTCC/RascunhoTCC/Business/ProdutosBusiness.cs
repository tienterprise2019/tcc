﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Business
{
    class ProdutosBusiness
    {
        public void CadastrarProdutos(Model.tb_produto ModelProduto)
        {
            if (ModelProduto.nm_produto == string.Empty)
            {
                throw new ArgumentException("Nome do produto não pode ser vazio");
            }

            if (ModelProduto.vl_preco == 0)
            {
                throw new ArgumentException("Preço não pode ser zero");
            }

            Database.ProdutosDatabase DBProduto = new Database.ProdutosDatabase();
            DBProduto.CadastrarProdutos(ModelProduto);
        }

        public List<Model.tb_produto> ConsultarProdutos()
        {
            Database.ProdutosDatabase DBProduto = new Database.ProdutosDatabase();
            List<Model.tb_produto> ModelProdutos = DBProduto.ConsultarProdutos();

            return ModelProdutos;
        }


        public void AlterarProdutos(Model.tb_produto ModelProduto)
        {
            if (ModelProduto.id_produto == 0)
            {
                throw new ArgumentException("Produto não pode ser vazio");
            }

            if (ModelProduto.vl_preco == 0)
            {
                throw new ArgumentException("Preço não pode ser zero");
            }

            Database.ProdutosDatabase DBProduto = new Database.ProdutosDatabase();
            DBProduto.AlterarProdutos(ModelProduto);
        }

        public void RemoverProdutos(int id)
        {
            Database.ProdutosDatabase DBProduto = new Database.ProdutosDatabase();
            DBProduto.RemoverProdutos(id);
        }

        public List<Model.tb_produto> ConsultarPorNomeProdutos(string produto)
        {
            Database.ProdutosDatabase DBProduto = new Database.ProdutosDatabase();
            List<Model.tb_produto> ModelProdutos = DBProduto.ConsultarPorNomeProdutos(produto);

            return ModelProdutos;
        }
    }
}
