﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Business
{
    class PacotesBusiness
    {
        public void CadastrarPacotes(Model.tb_pacote ModelPacote)
        {
            if (ModelPacote.qt_teorica == 0)
            {
                throw new ArgumentException("Quantidade de aulas teóricas não pode ser zero");
            }

            if (ModelPacote.qt_pratica == 0)
            {
                throw new ArgumentException("Quantidade de aulas práticas não pode ser zero");
            }

            if (ModelPacote.vl_preco == 0)
            {
                throw new ArgumentException("Preço não pode ser zero");
            }

            if (ModelPacote.tp_carteira == null)
            {
                throw new ArgumentException("Selecione o tipo de carteira");
            }

            Database.PacotesDatabase DBPacote = new Database.PacotesDatabase();
            DBPacote.CadastrarPacotes(ModelPacote);
        }

        public List<Model.tb_pacote> ConsultarPacotes()
        {
            Database.PacotesDatabase DBPacote = new Database.PacotesDatabase();
            List<Model.tb_pacote> ModelPacotes = DBPacote.ConsultarPacotes();

            return ModelPacotes;
        }


        public void AlterarPacotes(Model.tb_pacote ModelPacote)
        {
            if (ModelPacote.qt_teorica == 0)
            {
                throw new ArgumentException("Quantidade de aulas teóricas não pode ser zero");
            }

            if (ModelPacote.qt_pratica == 0)
            {
                throw new ArgumentException("Quantidade de aulas práticas não pode ser zero");
            }

            if (ModelPacote.vl_preco == 0)
            {
                throw new ArgumentException("Preço não pode ser zero");
            }

            Database.PacotesDatabase DBPacote = new Database.PacotesDatabase();
            DBPacote.AlterarPacotes(ModelPacote);
        }

        public void RemoverPacotes(int id)
        {
            Database.PacotesDatabase DBPacote = new Database.PacotesDatabase();
            DBPacote.RemoverPacotes(id);
        }

        public List<Model.tb_pacote> ConsultarPorNomeAlunos(string aluno)
        {
            Database.PacotesDatabase DBPacote = new Database.PacotesDatabase();
            List<Model.tb_pacote> ModelPacotes = DBPacote.ConsultarPorNomeAlunos(aluno);

            return ModelPacotes;
        }
    }
}
