﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RascunhoTCC.Business
{
    class CarrosBusiness
    {
        public void CadastrarCarros(Model.tb_carro ModelCarro)
        {
            if (ModelCarro.nm_modelo == string.Empty)
            {
                throw new ArgumentException("Modelo não pode ser vazio");
            }

            if (ModelCarro.ds_placa == string.Empty)
            {
                throw new ArgumentException("Placa não pode ser vazio");
            }

            if (ModelCarro.km_inicial == 0)
            {
                throw new ArgumentException("KM inicial não pode ser zero");
            }

            if (ModelCarro.vc_adaptado == null)
            {
                throw new ArgumentException("Defina o tipo de veículo");
            }

            if (ModelCarro.tp_disponivel == null)
            {
                throw new ArgumentException("Disponibilidade do veículo deve ser marcada");
            }

            Database.CarrosDatabase DBCarro = new Database.CarrosDatabase();
            DBCarro.CadastrarCarros(ModelCarro);
        }

        public List<Model.tb_carro> ConsultarCarros()
        {
            Database.CarrosDatabase DBCarro = new Database.CarrosDatabase();
            List<Model.tb_carro> ModelCarros = DBCarro.ConsultarCarros();

            return ModelCarros;
        }


        public void AlterarCarros(Model.tb_carro ModelCarro)
        {

            if (ModelCarro.nm_modelo == string.Empty)
            {
                throw new ArgumentException("Modelo não pode ser vazio");
            }

            if (ModelCarro.ds_placa == string.Empty)
            {
                throw new ArgumentException("Placa não pode ser vazio");
            }

            if (ModelCarro.km_inicial == 0)
            {
                throw new ArgumentException("KM final não pode ser zero");
            }

            if (ModelCarro.vc_adaptado == false)
            {
                throw new ArgumentException("Defina o tipo de veículo");
            }

            if (ModelCarro.tp_disponivel == false)
            {
                throw new ArgumentException("Disponibilidade do veículo deve ser marcada");
            }


            Database.CarrosDatabase DBCarro = new Database.CarrosDatabase();
            DBCarro.AlterarCarros(ModelCarro);
        }

        public void RemoverCarros(int id)
        {
            Database.CarrosDatabase DBCarro = new Database.CarrosDatabase();
            DBCarro.RemoverCarros(id);
        }

        public List<Model.tb_carro> ConsultarPorNomeModelos(string modelo)
        {
            Database.CarrosDatabase DBCarro = new Database.CarrosDatabase();
            List<Model.tb_carro> ModelCarros = DBCarro.ConsultarPorNomeModelos(modelo);

            return ModelCarros;
        }
    }
}
